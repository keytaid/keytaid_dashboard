<?php

Route::get('/',function ()
{
	return Redirect::to('login');
});
Route::get('/admin/dashboard/{id}','DashboardController@index')->where('id', '[0-9]+');

// Daily reward
Route::get('/admin/{id}/gamification/reward','GamificationController@indexReward')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/gamification/reward/{day}','GamificationController@detailReward')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::patch('/admin/{id}/gamification/reward/{reward_id}/edit','GamificationController@updateReward');

// Wheel of fortune
Route::get('/admin/{id}/gamification/wheel','GamificationController@indexWheel')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/gamification/wheel/{wheel_id}','GamificationController@detailWheel')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::patch('/admin/{id}/gamification/wheel/{wheel_id}/edit','GamificationController@updateWheel');

// History reward claimed
Route::get('/admin/{id}/gamification/history/{page}','GamificationController@indexHistory')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/gamification/history/filter/{page}','GamificationController@searchHistory')->where('id', '[0-9]+')->where('page', '[0-9]+');

// Markup
Route::get('/admin/{id}/markup/{page}','MarkupController@indexMarkup')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/markup/detail/{page}','MarkupController@detailMarkup')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/markup/detail/price/{page}','MarkupController@eachMarkup')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::put('/admin/{id}/markup/detail/price/{page}/edit','MarkupController@updateMarkup')->where('id', '[0-9]+')->where('page', '[0-9]+');

Route::get('/admin/{id}/keytasaldo/user-balance/{page}','KeytasaldoController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytasaldo');
Route::get('/admin/{id}/keytasaldo/history/{page}','KeytasaldoController@history')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytasaldo');

Route::get('/admin/{id}/keytapointnew/user-balance/{page}','KeytapointnewController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytapointnew');
Route::get('/admin/{id}/keytapointnew/history/{page}','KeytapointnewController@history')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytapointnew');

Route::get('/admin/{id}/keytapoint/user-balance/{page}','KeytapointController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytauser');
Route::get('/admin/{id}/keytapoint/user-balance/search/{page}','KeytapointController@index_search')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytauser');
Route::post('/admin/{id}/keytapoint/user-balance/export', 'KeytapointController@export')->name('export');
Route::get('/admin/{id}/keytapoint/history/{page}','KeytapointController@history')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/keytapoint/history/search/{page}','KeytapointController@history_search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/keytapoint/history/export','KeytapointController@history_export')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/keytapoint/export', 'KeytauserController@export')->name('export');
Route::get('/admin/keytapoint/autocomplete', 'KeytauserController@autocomplete')->name('autocomplete');
Route::post('/keytapoint/{id}/topup', 'KeytapointController@pointTopup')->where('id', '[0-9]+');
Route::post('/keytapoint/{id}/reward', 'KeytapointController@pointReward')->where('id', '[0-9]+');
Route::post('/keytapoint/{id}/reduce', 'KeytapointController@pointReduce')->where('id', '[0-9]+');

Route::get('/admin/{id}/app-management/assets/{page}','AssetController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/assets/search/{page}','AssetController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('assets_gallery');;
Route::get('/admin/{id}/assets/{asset_id}/delete','AssetController@delete')->where('id', '[0-9]+');
Route::post('/assets/{id}/upload','AssetController@upload')->where('id', '[0-9]+');

Route::get('/admin/{id}/app-management/faq','FaqsController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/app-management/faq/edit/{faq_id}','FaqsController@edit')->where('id', '[0-9]+')->where('faq_id', '[0-9]+');
Route::put('/admin/{id}/app-management/faq/update/{faq_id}','FaqsController@update')->where('id', '[0-9]+')->where('faq_id', '[0-9]+');
Route::get('/admin/{id}/app-management/faq/add','FaqsController@add')->where('id', '[0-9]+');
Route::post('/admin/{id}/app-management/faq/store','FaqsController@store')->where('id', '[0-9]+');

Route::get('/admin/{id}/voucher/list','VoucherController@list')->where('id', '[0-9]+')->name('voucher_list');
Route::get('/admin/{id}/voucher/list/{voucher_title}/{page}','VoucherController@list_by_title')->where('id', '[0-9]+');
Route::get('/admin/{id}/voucher/update/{voucher_title}','VoucherController@update')->where('id', '[0-9]+')->name('voucher_update');

Route::get('/admin/{id}/app-management/voucher','VoucherController@index')->where('id', '[0-9]+')->name('voucher_blast');
Route::get('/admin/{id}/app-management/voucher-shop','VoucherController@shop_index')->where('id', '[0-9]+')->name('voucher_shop');
Route::get('/admin/{id}/app-management/voucher-queue','VoucherController@queue_index')->where('id', '[0-9]+')->name('voucher_queue');
Route::post('/admin/{id}/app-management/voucher/blast', 'VoucherController@blast_voucher')->where('id', '[0-9]+');
Route::post('/admin/{id}/app-management/voucher/shop', 'VoucherController@shop_voucher')->where('id', '[0-9]+');
Route::post('/admin/{id}/app-management/voucher/queue', 'VoucherController@queue_voucher')->where('id', '[0-9]+');
Route::post('/admin/{id}/app-management/voucher/update/{voucher_title}', 'VoucherController@update_voucher')->where('id', '[0-9]+');


Route::get('/admin/{id}/keytauser/{page}','KeytauserController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytauser');
Route::get('/admin/{id}/keytauser/search/{page}','KeytauserController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/keytauser/export', 'KeytauserController@export')->name('export'); 

Route::get('/admin/{id}/keytauser/community/shop/{shop_id}/{page}','KeytauserCommunityController@details')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('keytauser');

Route::get('/admin/{id}/referral/{page}','ReferralController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('referral');

Route::get('/admin/{id}/notification/{page}','NotificationController@index')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('notification');
Route::get('/admin/{id}/notification/detail/{notif_id}','NotificationController@detail')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::put('/admin/{id}/notification/{notif_id}/update','NotificationController@update')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/notification/create','NotificationController@createPage')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::post('/admin/{id}/notification/create/new','NotificationController@create')->where('id', '[0-9]+')->where('page', '[0-9]+');

Route::get('/canvaser/{id}/keytauser/{page}','KeytauserController@canvaser_user_list')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/canvaser/{id}/keytauser/export','KeytauserController@canvaser_user_export')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/canvaser/{id}/orders/{page}','OrderController@canvaser_user_order_list')->where('id', '[0-9]+')->where('page', '[0-9]+');
 
Route::get('/admin/{id}/bankaccounts/{page}','BankaccountController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/bankaccounts/search/{page}','BankaccountController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/bankaccounts/export', 'BankaccountController@export')->name('export');

Route::get('/admin/{id}/expeditions/{page}','ExpeditionsController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/expeditions/search/{page}','ExpeditionsController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/expeditions/export', 'ExpeditionsController@export')->name('export');
Route::get('/admin/{id}/expeditions/list','ExpeditionsController@list')->where('id', '[0-9]+');
Route::get('/admin/{id}/expeditions/list/{expedition_id}/edit','ExpeditionsController@listEdit')->where('id', '[0-9]+')->where('expedition_id', '[0-9]+');
Route::put('/admin/{id}/expeditions/list/{expedition_id}/update','ExpeditionsController@listUpdate')->where('id', '[0-9]+')->where('expedition_id', '[0-9]+');

Route::get('/admin/{id}/transactions/{page}','TransactionController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/transactions/search/{page}','TransactionController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::post('/admin/{id}/transactions/export', 'TransactionController@export')->name('export');

Route::get('/admin/{id}/orders/{page}','OrderController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/orders/search/{page}','OrderController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::post('/admin/{id}/orders/export', 'OrderController@export')->name('export');
Route::get('/admin/{id}/orders/cancel/{order_id}','OrderController@cancelOrder')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/orders/detail/{order_id}','OrderController@get_order_detail')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/orders/tracking/{order_id}','OrderController@tracking_order')->where('id', '[0-9]+')->where('page', '[0-9]+');

Route::get('/admin/{id}/orders/difference/{page}','OrderController@indexDifference')->where('id', '[0-9]+')->where('page', '[0-9]+')->name('order_difference');
Route::post('/admin/{id}/orders/difference/create','OrderController@createDifference')->where('id', '[0-9]+')->where('page', '[0-9]+');

Route::get('/admin/{id}/orders-premium/{page}','OrderPremiumController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/orders-premium/search/{page}','OrderPremiumController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::post('/admin/{id}/orders-premium/export', 'OrderPremiumController@export')->name('export');
Route::get('/admin/{id}/pricing-premium','OrderPremiumController@priceIndex')->where('id', '[0-9]+');
Route::get('/admin/{id}/pricing-premium/{pricing_id}/edit','OrderPremiumController@priceEdit')->where('id', '[0-9]+')->where('pricing_id', '[0-9]+');
Route::put('/admin/{id}/pricing-premium/{pricing_id}/update','OrderPremiumController@priceUpdate')->where('id', '[0-9]+')->where('pricing_id', '[0-9]+');
Route::get('/admin/{id}/pricing-premium/{pricing_id}/delete','OrderPremiumController@priceDelete')->where('id', '[0-9]+')->where('pricing_id', '[0-9]+');
Route::get('/admin/{id}/pricing-premium/add','OrderPremiumController@priceAdd')->where('id', '[0-9]+');
Route::post('/admin/{id}/pricing-premium/store','OrderPremiumController@priceStore')->where('id', '[0-9]+');
Route::get('/admin/{id}/shops-premium/{page}','OrderPremiumController@premiumShopIndex')->where('id', '[0-9]+');
Route::get('/admin/{id}/shops-premium/search/{page}','OrderPremiumController@premiumShopSearch')->where('id', '[0-9]+');

Route::get('/admin/{id}/products/{page}','ProductController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/products/search/{page}','ProductController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/products/export', 'ProductController@export')->name('export')->where('page', '[0-9]+');

Route::get('/admin/{id}/reports/{page}','ReportController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/reports/search/{page}','ReportController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/reports/export', 'ReportController@export')->name('export');
Route::post('/admin/{id}/reports/resolve/{report_id}', 'ReportController@resolve')->where('id', '[0-9]+')->where('report_id', '[0-9]+');

Route::get('/admin/{id}/costs/{page}','CostController@index')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/costs/search/{page}','CostController@search')->where('id', '[0-9]+')->where('page', '[0-9]+');
Route::get('/admin/{id}/costs/export', 'CostController@export')->name('export');

Route::get('/admin/{id}','AdminController@index')->where('id', '[0-9]+')->name('admin');
Route::get('/admin/daftartenan/{id}','AdminController@tenantList')->where('id', '[0-9]+');
Route::get('/admin/daftarupload/{id}','AdminController@uploadList')->where('id', '[0-9]+');

Route::get('/register/{id}','RegisterController@getRegister')->where('id', '[0-9]+');
Route::post('/register/post/{id}','RegisterController@postRegister')->where('id', '[0-9]+');

Route::get('/superuser/{id}','SuperuserController@show')->where('id', '[0-9]+');
Route::get('/superuser/tambahadmin/{id}','SuperuserController@getAdminRegister')->where('id', '[0-9]+');
Route::post('/superuser/tambahadmin/{id}/post','SuperuserController@postAdmin')->where('id', '[0-9]+');

Route::get('/login','LoginController@getLogin')->name('login');
Route::post('/login/post','LoginController@postLogin');
Route::get('/logout',function()
{
	Auth::logout();
	Session::flush();
	return Redirect::to('login');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/cancel/test', function(){
    return Response::make("", 204);
    // return response()->noContent();
});


Route::get('/admin/{id}/disbursement/{page}','DisbursementController@index');
Route::get('/admin/{id}/paymentlink/{page}','PaymentLinkController@index');
Route::get('/admin/{id}/paymentlink_fee/{page}','PaymentLinkController@listfee');
Route::get('/admin/{id}/paymentlink/resolve/{trx_id}','PaymentLinkController@resolve');

Route::post('/admin/{id}/orders/exportnew', 'OrderController@export_new')->name('export');
Route::post('/admin/{id}/premium/exportnew', 'OrderPremiumController@export_new')->name('export');

Route::get('/admin/{id}/remote-config','RemoteConfigController@index')->name('remoteconfig');
Route::post('/admin/{id}/remote-config/create','RemoteConfigController@create');
Route::get('/admin/{id}/remote-config/name/{name}/{remote_id}','RemoteConfigController@getname');
Route::post('/admin/{id}/remote-config/update/{remote_id}','RemoteConfigController@update');