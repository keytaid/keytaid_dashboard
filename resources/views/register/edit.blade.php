@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">


  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('/tenant/resetpass/'.$user_id.'/update') }}" enctype="multipart/form-data">
      @method('PUT')
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="">
            <h3>{{$title}}</h2>
          </div>
          <br>
          <div class="form-group has-feedback">
            {{ Form::label('new_pass', 'Password Baru') }}
            <input type="password" class="form-control" placeholder="Password Baru" name="new_pass" value="">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('new_pass', 'Konfirmasi Password Baru') }}
            <input type="password" class="form-control" placeholder="Password Baru" name="new_pass_confirmation" value="">
          </div>
          

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Edit Slider</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection