@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>

                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Payment method</th>
                                        <th>Fee Xendit</th>
                                        <th>Fee Keyta</th>
                                        <th>Fee unit</th>
                                        <th>Discount</th>
                                        <th>Discount unit</th>
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 0;
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>
                            @foreach($paymentlinks as $paymentlink)
                                <tr>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $no++ }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['code'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['fee_xendit'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['fee'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['unit'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['discount'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['discount_type'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection