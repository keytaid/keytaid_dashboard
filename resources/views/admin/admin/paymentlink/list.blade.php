@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>

                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Keytasaldo total dari PL</span>
                                    <span class="info-box-number">{{ number_format(($paymentlinks['amount_disburseable'] + $paymentlinks['amount_onhold']), 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Amount received on xendit</span>
                                    <span class="info-box-number">{{ number_format($paymentlinks['amount_pl_on_xendit'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-warning"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Keyta Subsidy</span>
                                    <span class="info-box-number">{{ number_format(($paymentlinks['amount_pl_subsidy']), 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                <span class="info-box-text">Keytasaldo completed</span>
                                    <span class="info-box-number">{{ number_format($paymentlinks['amount_disburseable'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Fee xendit + vat</span>
                                        <span class="info-box-number">{{ number_format($paymentlinks['amount_pl_xendit_fee_vat'], 0, ',', '.') }}</span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Net saldo onhold</span>
                                    <span class="info-box-number">{{ $paymentlinks['amount_onhold'] }}</span>
                                </div>
                                </div>
                            </div> -->
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                <span class="info-box-text">Keytasaldo onhold</span>
                                    <span class="info-box-number">{{ number_format($paymentlinks['amount_onhold'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Net PL saldo on xendit</span>
                                        <span class="info-box-number">{{ number_format($paymentlinks['amount_pl_net_payment'], 0, ',', '.') }}</span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Net saldo onhold</span>
                                    <span class="info-box-number">{{ $paymentlinks['amount_onhold'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Net saldo onhold</span>
                                    <span class="info-box-number">{{ $paymentlinks['amount_onhold'] }}</span>
                                </div>
                                </div>
                            </div> -->
                        </div>

                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>                                  
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-4 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($status == null)
                                        Select Status
                                    @else
                                        {{$status}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?status=active') }}"       >active</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?status=in_progress') }}"  >in progress</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?status=expired') }}"      >expired</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?status=completed') }}"    >completed</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?status=dispute') }}"      >dispute</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3 text-center">
                                    <span></span>
                                </div>
                                <div class="col-12">
                                    <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/paymentlink/1') }}" enctype="multipart/form-data">
                                        <input type="hidden" name="status" value="{{$status}}">
                                        <input type="hidden" name="date_range" value="{{$date_range}}">
                                        <div class="md-form active-cyan active-cyan-2 mb-3">
                                            <input class="form-control" type="text" placeholder="shop name or shop id" aria-label="Search" name="keyword" value="{{$keyword}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Tanggal Generate</th>
                                        <th>Shop id</th>
                                        <th>Invoice id</th>
                                        <th>Invoice Number</th>
                                        <th>Nama toko</th>
                                        <th>PL Status</th>
                                        <th>Resolve</th>
                                        <th>Tanggal Expired</th>
                                        <th>Total harga produk</th>
                                        <th>Total harga pengiriman</th>
                                        <th>Total Invoice</th>
                                        <th>Fee before disc.</th>
                                        <th>Fee after disc.</th>
                                        <th>Fee By</th>
                                        <th>CC Fee before disc.</th>
                                        <th>CC Fee after disc.</th>
                                        <th>Fee CC By</th>
                                        <th>Total Pembayaran</th>

                                        <th>Fee Xendit</th>
                                        <th>VAT</th>
                                        <th>Net Payment</th>
                                        <th>Seller Amount</th>
                                        <th>Subsidy Amount</th>
                                        <th>PL ID</th>
                                        <th>Payment status</th>
                                        <th>Payment method</th>
                                        <th>Reference id</th>
                                        <th>Transaction status</th>
                                        <th>Keyta saldo</th>
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 1 + (($paymentlinks['current_page']-1)*$paymentlinks['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>
                            @foreach($paymentlinks['results'] as $paymentlink)
                                <tr>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $no++ }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ str_replace("T"," ",substr($paymentlink['created_at'], 0, 19)) }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['trx']['shop_id'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['trx']['id'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['trx']['number'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['shop']['name'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center
                                    @if($paymentlink['status'] == 'completed')
                                        {{'success'}}
                                    @elseif($paymentlink['status'] == 'expired')   
                                        {{'light'}}
                                    @elseif($paymentlink['status'] == 'dispute')   
                                        {{'error'}}
                                    @elseif($paymentlink['status'] == 'active')
                                        {{'warning'}}
                                    @elseif($paymentlink['status'] == 'in_progress')
                                        {{'info'}}
                                    @else
                                    @endif
                                    ">{{ $paymentlink['status'] }}</td>

                                    @if($paymentlink['trx']['is_dispute'])
                                        <td class="text-center">
                                            <a href="{{ URL::to('/admin/'.$user_id.'/paymentlink/resolve/'.$paymentlink['trx']['id'] ) }}">
                                                <i class="btn btn-info">resolve</i>
                                            </a>
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ null }}</td>
                                    @endif
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ str_replace("T"," ",substr($paymentlink['expired_date'], 0, 19)) }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($paymentlink['trx']['total_price'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($paymentlink['trx']['shipping_price'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($paymentlink['trx']['total_payment'], 0, ',', '.') }}</td>
                                    
                                    <!-- Fee Before discount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['xendit_fee_before_disc'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Fee After discount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['service_fee'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Fee handle by -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                @if($orderdetail['fee_by_shop'])
                                                    <li>shop</li>
                                                @else
                                                    <li>customer</li>
                                                @endif
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Fee CC Before Discount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['xendit_fee_cc_before_disc'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Fee CC After Discount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['xendit_fee_cc_before_disc'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Fee CC handle by -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                @if($orderdetail['credit_card_fee_by_shop'])
                                                    <li>shop</li>
                                                @else
                                                    <li>customer</li>
                                                @endif
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Total Pembayaran -->
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($paymentlink['total'], 0, ',', '.') }}</td>

                                    <!-- Xendit Fee -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['xendit_fee'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- VAT -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['vat'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Amount inside Xendit -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['net_income'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Seller Amount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['seller_amount'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <!-- Subsidy Amount -->
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ number_format($orderdetail['subsidy'], 0, ',', '.') }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['id'] }}</td>

                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ $orderdetail['status'] }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif


                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                <li>{{ $orderdetail['payment_method'] }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                @foreach($orderdetail['payments'] as $orderdetail_payment)
                                                    <li>{{ $orderdetail_payment['external_id'] }}</li>
                                                @endforeach
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif
                                    
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $paymentlink['trx']['transaction_status'] }}</td>
                                    @if(count($paymentlink['order']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($paymentlink['order'] as $orderdetail)
                                                @foreach($orderdetail['payments'] as $orderdetail_payment)
                                                    @if($orderdetail_payment['event'] == 'pt')
                                                        <li>{{ number_format($orderdetail_payment['amount'], 0, ',', '.') }}</li>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $paymentlinks['current_page'] + 2; 
                                    $start_page = $paymentlinks['current_page'] - 2;
                                ?>
                                @if($access_level['role_id']==2)
                                    @if ($paymentlinks['current_page'] > 1)
                                        <?php $before_page = $paymentlinks['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/1?date_range='.$date_range.'&status='.$status) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/'.$before_page.'?date_range='.$date_range.'&status='.$status) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $paymentlinks['total_pages'])
                                            <li class="page-item @if($paymentlinks['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/'.$i.'?date_range='.$date_range.'&status='.$status) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($paymentlinks['current_page'] < $paymentlinks['total_pages'])
                                        <?php $next_page = $paymentlinks['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/'.$next_page.'?date_range='.$date_range.'&status='.$status) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/paymentlink/'.$paymentlinks['total_pages'].'?date_range='.$date_range.'&status='.$status) }}">Last</a></li>
                                    @endif
                                @endif
                            </ul>
                        </nav>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection