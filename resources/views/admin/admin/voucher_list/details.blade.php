@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$voucher_title}}</h3>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="fa fa-barcode"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total voucher</span>
                                        <span class="info-box-number">{{ $vouchers['total'] }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-warning"><i class="fa fa-barcode"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total pending</span>
                                        <span class="info-box-number">{{ $vouchers['pending'] }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-success"><i class="fa fa-barcode"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Total completed</span>
                                        <span class="info-box-number">{{ $vouchers['completed'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Total users</span>
                                        <span class="info-box-number">{{ $vouchers['user_claimed'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="row">
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($filter == null)
                                    Select filter
                                @else
                                    {{$filter}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/1') }}">Select filter</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/1?filter=pending') }}">Pending</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/1?filter=completed') }}">Completed</a>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Voucher Id</th>
                                    <th>Shop Id</th>
                                    <th>Shop Name</th>
                                    <th>Status</th>
                                    <th>Completed at</th>
                                    <th>Title</th>
                                    <th>Code</th>
                                    <th>Value</th>
                                    <th>Unit</th>
                                    <th>Max Value</th>
                                    <th>Type</th>
                                    <th>Min Shipping</th>
                                    <th>Max Shipping</th>
                                    <th>Start date</th>
                                    <th>Expired date</th>
                                    <th>Expedition IDS</th>
                                    <th>Service IDS</th>
                                    <th>Payment IDS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($vouchers['current_page']-1)*$vouchers['limit']);
                                ?>
                                @foreach($vouchers['voucher'] as $voucher)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $voucher['id'] }}</td>
                                        <td class="text-center">{{ $voucher['shop_id'] }}</td>
                                        <td class="text-center">{{ $voucher['shop']['name'] }}</td>
                                        <td class="text-center  {{ $voucher['status'] == 'completed' ? "success" : "warning" }}">{{ ($voucher['status']) }}</td>
                                        <td class="text-center">{{ ($voucher['is_use']) ? str_replace("T"," ",substr($voucher['updated_at'], 0, 19)) : "-" }}</td>
                                        <td class="text-center">{{ $voucher['voucher_title'] }}</td>
                                        <td class="text-center">{{ $voucher['code'] }}</td>
                                        <td class="text-center">{{ $voucher['value'] }}</td>
                                        <td class="text-center">{{ $voucher['unit'] }}</td>
                                        <td class="text-center">{{ $voucher['max_value'] }}</td>
                                        <td class="text-center">{{ $voucher['voucher_type'] }}</td>
                                        <td class="text-center">{{ $voucher['min_shipping_price'] }}</td>
                                        <td class="text-center">{{ $voucher['max_shipping_price'] }}</td>
                                        <td class="text-center">{{ str_replace("T"," ",substr($voucher['start_date'], 0, 19)) }}</td>
                                        <td class="text-center">{{ str_replace("T"," ",substr($voucher['expired_date'], 0, 19)) }}</td>
                                        <td class="text-center">{{ $voucher['expedition_ids'] }}</td>
                                        <td class="text-center">{{ $voucher['service_codes'] }}</td>
                                        <td class="text-center">{{ $voucher['payment_method'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $vouchers['current_page'] + 2; 
                                    $start_page = $vouchers['current_page'] - 2;
                                ?>
                                
                                @if ($vouchers['current_page'] > 1)
                                    <?php $before_page = $vouchers['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/1?filter='.$filter) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/'.$before_page.'?filter='.$filter) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $vouchers['total_pages'])
                                        <li class="page-item @if($vouchers['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/'.$i.'?filter='.$filter) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($vouchers['current_page'] < $vouchers['total_pages'])
                                    <?php $next_page = $vouchers['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/'.$next_page.'?filter='.$filter) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher_title.'/'.$vouchers['total_pages'].'?filter='.$filter) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection