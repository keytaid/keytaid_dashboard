@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-barcode"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total voucher name</span>
                                    <span class="info-box-number">{{ $vouchers['total'] }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Details</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                ?>
                                @foreach($vouchers['voucher'] as $voucher)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $voucher['field'] }}</td>
                                        <td class="text-center">
                                            <a href="{{ URL::to('/admin/'.$user_id.'/voucher/list/'.$voucher['field'].'/1' ) }}">
                                                <i class="btn btn-info">Details</i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ URL::to('/admin/'.$user_id.'/voucher/update/'.$voucher['field'] ) }}">
                                                <i class="btn btn-warning">Update</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection