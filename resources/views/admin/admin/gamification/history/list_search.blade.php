@extends('admin/admin')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>

                <div class="card-tools">

                </div>
            </div>
            <div class="card-body">
                @if (Session::has('message'))
                <div id="alert-msg" class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}
                </div>
                @endif

                <div class="row">
                    <form class="col-6" id="form_tanggal" method="GET"
                        action="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?range='.$date_range) }}">
                        <div class="form-group">
                            <label>Pilih Tanggal:</label>

                            <div class="input-group">
                                <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                    <i class="fa fa-calendar"></i> {{$date_range}}
                                    <i class="fa fa-caret-down"></i>
                                    <input type="hidden" name="voucherReward" value="{{$voucherReward}}">
                                    <input type="hidden" name="userName" value="{{$userName}}">
                                    <input type="hidden" name="day" value="{{$day}}">
                                    <input type="hidden" name="range" id="tanggal" value="{{$date_range}}" />
                                </button>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Claimed</span>
                                <span class="info-box-number">{{$points['data']['count']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-warning"><i class="fa fa-gift"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Voucher Claimed</span>
                                <span class="info-box-number">{{$points['data']['voucher_claimed']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Keyta Point Claimed</span>
                                <span class="info-box-number">@currency($points['data']['total_point'])</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 text-center">
                        <span></span>
                    </div>
                    <div class="col-5">
                        <form method="GET"
                            action="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?range='.$date_range) }}"
                            enctype="multipart/form-data">
                            <input type="hidden" name="voucherReward" value="{{$voucherReward}}">
                            <input type="hidden" name="range" value="{{$date_range}}">
                            <input type="hidden" name="day" value="{{$day}}">
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <input class="form-control" type="text" placeholder="Search by shop id or name" aria-label="Search"
                                    name="userName" value="{{$userName}}">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 text-center">
                        <span></span>
                    </div>
                    <div class="col-2 dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if($day == null)
                            Select Day
                            @else
                            {{$day}}
                            @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?userName='.$userName.'&voucherReward='.$voucherReward.'&range='.$date_range) }}">Select
                                Day</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=1&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">1</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=2&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">2</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=3&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">3</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=4&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">4</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=5&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">5</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=6&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">6</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?day=7&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">7</a>
                        </div>
                    </div>
                    <div class="col-2 dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if($voucherReward == null)
                            Select Voucher
                            @else
                            {{$voucherReward}}
                            @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?&day='.$day) }}">Select
                                Voucher</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=GrabExpress&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">GrabExpress</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=GoSend&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">GoSend</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=J%26T&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">J&T</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=Paxel&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">Paxel</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=SiCepat&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">SiCepat</a>
                            <a class="dropdown-item"
                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1/?voucherReward=AnterAja&day='.$day.'&userName='.$userName.'&range='.$date_range) }}">AnterAja</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="scrolling-wrapper">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Date Created</th>
                                <th>Shop Id</th>
                                <th>Username</th>
                                <th>Day</th>
                                <th>Point</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                    $no = 1 + (($points['data']['current_page']-1)*$points['data']['limit']);
                                    // echo $points
                                ?>

                            @foreach($points['data']['rows'] as $point)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ date("d/m/Y H:i A",strtotime($point['createdAt'])+25200)}}</td>
                                <td class="text-center">{{ $point['shopId'] }}</td>
                                <td class="text-center">{{ $point['userName'] }}</td>
                                <td class="text-center">{{ $point['day'] }}</td>
                                <td class="text-center">{{ $point['value'] }}</td>
                                <td class="text-center">{{ $point['description'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mt-2 d-flex justify-content-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <?php 
                                    $show_page = $points['data']['current_page'] + 2; 
                                    $start_page = $points['data']['current_page'] - 2;
                                ?>

                            @if ($points['data']['current_page'] > 1)
                            <?php $before_page = $points['data']['current_page'] - 1 ?>
                            <li class="page-item"><a class="page-link"
                                    href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/1?day='.$day.'&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">First</a>
                            </li>
                            <li class="page-item"><a class="page-link"
                                    href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/'.$before_page.'?day='.$day.'&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">Previous</a>
                            </li>
                            @endif
                            @for ($i = $start_page; $i <= $show_page; $i++) @if ($i> 0 and $i <=
                                    $points['data']['total_page']) <li
                                    class="page-item @if($points['data']['current_page'] == $i) active @endif"><a
                                        class="page-link"
                                        href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/'.$i.'?day='.$day.'&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">{{$i}}</a>
                                    </li>
                                    @endif
                                    @endfor
                                    @if ($points['data']['current_page'] < $points['data']['total_page'])
                                        <?php $next_page = $points['data']['current_page'] + 1 ?> <li class="page-item">
                                        <a class="page-link"
                                            href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/'.$next_page.'?day='.$day.'&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range) }}">Next</a>
                                        </li>
                                        <li class="page-item"><a class="page-link"
                                                href="{{ URL::to('/admin/'.$user_id.'/gamification/history/filter/'.$points['data']['total_page']).'?day='.$day.'&voucherReward='.$voucherReward.'&userName='.$userName.'&range='.$date_range }}">Last</a>
                                        </li>
                                        @endif

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".check_all").on("click", function () {
        $(".shop_id").each(function () {
            $(this).attr("checked", true);
            if $(this).attr("checked", true) {
                $(this).attr("checked", false)
            }
            else {
                $(this).attr("checked", true)
            }
        });
    });

</script>
@endsection
