@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">

  <div class="col-md-12">
  <form method="POST" action="{{ URL::to('admin/'.$user_id.'/gamification/reward/'.$rewards['data']['id'].'/edit') }}" enctype="multipart/form-data">
      @method('PATCH')
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{$title}}</h3>
          
          <div class="card-tools">
              
          </div>
        </div>
        <div class="card-body">
            @if (Session::has('message'))
              <div id="alert-msg" class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ Session::get('message') }}
              </div>
            @endif
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="row align-items-end">
            <div class="col-md-12">
              <div class="text-center">
                <img src="{{$rewards['data']['imageUrl']}}" alt="" width="400px">
              </div>
              <div class="form-group has-feedback">
                  <div class="form-group">
                      {{ Form::label('imageUrl', 'Upload Image') }}
                      {{ Form::file('imageUrl', ['class'=>'form-control']) }}  
                  </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <!-- <div class="col-md-1"></div> -->
            <div class="col-md-6">
                <div class="form-group has-feedback">
                    <div class="form-group has-feedback">
                        {{ Form::label('day', 'Day') }}
                        <input type="text" class="form-control" placeholder="Expedition Id" name="day" value="{{$rewards['data']['day']}}">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group has-feedback">
                    <div class="form-group has-feedback">
                        {{ Form::label('value', 'Value') }}
                        <input type="text" class="form-control" placeholder="Expedition Id" name="value" value="{{$rewards['data']['value']}}">
                    </div>
                </div>
            </div>
          
            
          </div> 
            

          <div class="row">
            <!-- <div class="col-md-1"></div> -->
            <div class="col-md-6">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('title', 'Title') }}
                      <input type="text" class="form-control" placeholder="Expedition Name" name="title" value="{{$rewards['data']['title']}}">
                  </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('description', 'Description') }}
                      <input type="text" class="form-control" placeholder="Expedition Name" name="description" value="{{$rewards['data']['description']}}">
                  </div>
              </div>
            </div>
          </div>

       

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Update {{$title}}</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection