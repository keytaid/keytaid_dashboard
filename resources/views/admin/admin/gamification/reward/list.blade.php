@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Popup</th>
                                    <th>Day</th>
                                    <th>Value</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <?php 
                                    $no = 1
                                ?> -->

                                @foreach($rewards['data'] as $reward)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center"><img src="{{ $reward['imageUrl'] }}" width="50px"></td>
                                    <td class="text-center">{{ $reward['day'] }}</td>
                                    <td class="text-center">{{ $reward['value'] }}</td>
                                    <td class="text-center">{{ $reward['title'] }}</td>
                                    <td class="text-center">{{ $reward['description'] }}</td>
                                    <td class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/gamification/reward/'.$reward['id']) }}" class="btn btn-success">Edit</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = 1;
                                    $start_page = 1;
                                ?>
                                
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection