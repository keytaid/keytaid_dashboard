@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-2 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/expeditions/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/expeditions/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($filter == null)
                                    Select Filter
                                @else
                                    {{$filter}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=grab_express') }}">Grab Express</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=gojek') }}">Go-Send</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=anteraja') }}">Anter Aja</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=paxel') }}">Paxel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=sicepat') }}">Sicepat</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=jne') }}">JNE</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=jnt') }}">J&T</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=pos') }}">POS Indonesia</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=deliveree') }}">Deliveree</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=ninja') }}">Ninja Xpress</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=tiki') }}">Tiki</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=wahana') }}">Wahana</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=lion') }}">Lion Parcel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=mrspeedy') }}">Mr Speedy</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=rpx') }}">RPX</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/expeditions/1?filter=jet') }}">JET</a>
                            </div>
                        </div>
                    </div> 

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Shop Id</th>
                                <th>Nama Toko</th>
                                <th>Nomor Telepon Toko</th>
                                <th>ID Ekspedisi</th>
                                <th>Nama Ekspedition</th>
                                <!-- <th>Action</th>  -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1 + (($expeditions['current_page']-1)*$expeditions['limit']);
                                // echo $expeditions
                            ?>

                            @foreach($expeditions['results'] as $expedition)
                            <tr>
                            
                                    <td class="text-center">{{ $no++ }}</td>
                                    @if(isset($expedition['shops']))
                                        <td class="text-center">{{ $expedition['shops']['id'] }}</td>
                                        <td class="text-center">{{ $expedition['shops']['name'] }}</td>
                                        <td class="text-center">{{ $expedition['shops']['phone'] }}</td>
                                    @else
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">{{ $expedition['expeditions']['id'] }}</td>
                                    <td class="text-center">{{ $expedition['expeditions']['name'] }}</td>
                                    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $expeditions['current_page'] + 2; 
                                    $start_page = $expeditions['current_page'] - 2;
                                ?>
                                
                                @if ($expeditions['current_page'] > 1)
                                    <?php $before_page = $expeditions['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/expeditions/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/expeditions/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $expeditions['total_pages'])
                                        <li class="page-item @if($expeditions['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/expeditions/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($expeditions['current_page'] < $expeditions['total_pages'])
                                    <?php $next_page = $expeditions['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/expeditions/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/expeditions/search/'.$expeditions['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection