@extends('admin/admin')
@section('content')
    <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="uploadImageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uploadImageLabel">Create new Config</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('admin/'.$user_id.'/remote-config/create') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    {{ Form::label('name', 'Name') }}
                                    <div class="form-group"> 
                                        <input type="text" class="form-control" placeholder="Name" name="name" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    {{ Form::label('condition', 'Condition') }}
                                    <div class="form-group"> 
                                        <input type="text" class="form-control" placeholder="Condition" name="condition" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    {{ Form::label('type', 'Type') }}
                                    <div class="form-group"> 
                                        <select name="type" class="form-control">
                                            <option>String</option>
                                            <option>Integer</option>
                                            <option>Float</option>
                                            <option>JSON</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    {{ Form::label('value', 'Value') }}
                                    <div class="form-group"> 
                                        <textarea style="height:250px" class="form-control" name="value"></textarea>

                                        <!-- <input type="text-area" class="form-control" placeholder="value" name="value" value=""> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="md-form active-cyan active-cyan-2 mb-3">
                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">
                                <i class="spinner fa fa-spinner fa-spin"></i>
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        <button class="btn btn-primary fa fa-plus" data-toggle="modal" data-target="#uploadImage" ></button>
                    </div>

                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <div class="scrolling-wrapper">
                    @if($access_level['role_id']==2)
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Edit</th>
                                    <th>Name</th>
                                    <th>Condition</th>
                                    <th>Value</th>
                                    <th>Config Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 
                                ?>

                                @foreach($remote_configs as $remote_config)
                                <tr>
                                    <td style="vertical-align : middle" class="text-center">{{ $no++ }}</td>
                                    <td style="vertical-align : middle" class="text-center">{{ $remote_config['id'] }}</td>
                                    <td style="vertical-align : middle" class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/remote-config/name/'.$remote_config['name'].'/'.$remote_config['id'] ) }}" class="btn btn-warning">Edit</a></td>
                                    <td style="vertical-align : middle" class="text-center">{{ $remote_config['name'] }}</td>
                                    <td style="vertical-align : middle" class="text-center">{{ $remote_config['condition'] }}</td>
                                    <td style="vertical-align : middle" class="text-left">{{ $remote_config['value'] }}</td>
                                    <td style="vertical-align : middle" class="text-center">{{ $remote_config['config_type'] }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                    </div>  
                </div>
            </div>
        </div>
    </div>
@endsection