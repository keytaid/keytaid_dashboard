@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                        <form method="POST" action="{{ URL::to('admin/'.$user_id.'/remote-config/update/'.$remote_id) }}" enctype="multipart/form-data"> 
                            <input type="hidden" name="_token" value="{{csrf_token()}}">        
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('name', 'Name') }}
                                                <input type="text" class="form-control" placeholder="name" name="name" value="{{$remote_config['name']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('condition', 'Condition') }}
                                                <input type="text" class="form-control" placeholder="condition" name="condition" value="{{$remote_config['condition']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('type', 'Type') }}
                                            <div class="form-group"> 
                                                <select name="type" class="form-control">
                                                    <option>Please select</option>
                                                    <option>String</option>
                                                    <option>Integer</option>
                                                    <option>Float</option>
                                                    <option>JSON</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('value', 'Value') }}
                                                <textarea style="height:250px" class="form-control" name="value" value=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">
                                                Update remote config
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
