@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-2 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/bankaccounts/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/bankaccounts/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="">
                                </div>
                            </form>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($filter == null)
                                    Select Filter
                                @else
                                    {{$filter}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=BCA') }}">BCA</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=Mandiri') }}">Mandiri</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=BNI') }}">BNI</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=BRI') }}">BRI</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=BTPN') }}">BTPN</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=CIMB') }}">CIMB</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=Danamon') }}">Danamon</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=HSBC') }}">HSBC</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=Panin') }}">Panin</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=DBS') }}">DBS</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter=Mega') }}">Mega</a>
                            </div>
                        </div>
                    </div> 
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Created At</th>
                                <th>Nama Toko</th>
                                <th>ID Toko</th>
                                <th>Nama Bank</th>
                                <th>Nama Pemilik Rekening</th>
                                <th>Nomor Rekening</th>
                                <!-- <th>Action</th>  -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1 + (($banks['current_page']-1)*$banks['limit']);
                                // echo $banks
                            ?>

                            @foreach($banks['results'] as $bank)
                            <tr>
                            
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ $bank['created_at'] }}</td>
                                    @if(isset($bank['shop']))
                                        <td class="text-center">{{ $bank['shop']['name'] }}</td>
                                        <td class="text-center">{{ $bank['shop']['id'] }}</td>
                                    @else
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">{{ $bank['name'] }}</td>
                                    <td class="text-center">{{ $bank['account_name'] }}</td>
                                    <td class="text-center">{{ $bank['account_number'] }}</td>
                                    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $banks['current_page'] + 2; 
                                    $start_page = $banks['current_page'] - 2;
                                ?>
                                
                                @if ($banks['current_page'] > 1)
                                    <?php $before_page = $banks['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/1?filter='.$filter) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/'.$before_page.'?filter='.$filter) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $banks['total_pages'])
                                        <li class="page-item @if($banks['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/'.$i.'?filter='.$filter) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($banks['current_page'] < $banks['total_pages'])
                                    <?php $next_page = $banks['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/'.$next_page.'?filter='.$filter) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/bankaccounts/'.$banks['total_pages'].'?filter='.$filter) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection