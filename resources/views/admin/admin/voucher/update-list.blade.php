@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    @if (Session::has('error'))
                    <div id="alert-msg" class="alert alert-error alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('error') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/app-management/voucher/update/'.$voucher_title) }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('password', 'Password') }}
                                                <input type="text" class="form-control" placeholder="Password to update voucher" name="password" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_type', 'Voucher Type') }}
                                            <div class="form-group"> 
                                                <select name="voucher_type" class="form-control">
                                                    @if($vouchers['voucher_type'] == "cashback")
                                                        <option selected="true" value="cashback">Cashback</option>
                                                    @else
                                                        <option value="cashback">Cashback</option>
                                                    @endif

                                                    @if($vouchers['voucher_type'] == "discount")
                                                        <option selected="true" value="discount">Discount</option>
                                                    @else
                                                        <option value="discount">Discount</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('code', 'Voucher Code') }}
                                                <input type="text" class="form-control" placeholder="Voucher Code" name="code" value="{{ $vouchers['code'] }}"  >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('expedition_ids', 'Expedition Ids') }}
                                                <div class="test2 form-control">
                                                @foreach($expeditions['results'] as $expedition)
                                                    @if(in_array($expedition['id'], $newExpeditions))
                                                        <div on class="test form-check">
                                                            <input checked="true" class="form-check-input" type="checkbox" name="expeditions[]" value="{{$expedition['id']}}" id="expedition-{{$expedition['id']}}">
                                                            <label class="form-check-label" for="expedition-{{$expedition['id']}}">
                                                                <img class="image-exp" src="{{$expedition['logo_url']}}" alt="{{$expedition['name']}} "/>
                                                            </label>
                                                        </div>
                                                    @else 
                                                        <div on class="test form-check">
                                                            <input class="form-check-input" type="checkbox" name="expeditions[]" value="{{$expedition['id']}}" id="expedition-{{$expedition['id']}}">
                                                            <label class="form-check-label" for="expedition-{{$expedition['id']}}">
                                                                <img class="image-exp" src="{{$expedition['logo_url']}}" alt="{{$expedition['name']}} "/>
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('payment_method', 'Payment Method') }}
                                            <div class="test3 form-control">
                                                @if(in_array("OVO", $newPaymentMethods))
                                                    <div class="test form-check">
                                                        <input checked="true" class="form-check-input" type="checkbox" name="payment_method[]" value="OVO" id="OVO">
                                                        <label class="form-check-label" for="OVO">
                                                            <img style="height: 25px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/ovo.png"/>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="payment_method[]" value="OVO" id="OVO">
                                                        <label class="form-check-label" for="OVO">
                                                            <img style="height: 25px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/ovo.png"/>
                                                        </label>
                                                    </div>
                                                @endif
                                                @if(in_array("DANA", $newPaymentMethods))
                                                    <div class="test form-check">
                                                        <input checked="true" class="form-check-input" type="checkbox" name="payment_method[]" value="DANA" id="DANA">
                                                        <label class="form-check-label" for="DANA">
                                                            <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/dana.png"/>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="payment_method[]" value="DANA" id="DANA">
                                                        <label class="form-check-label" for="DANA">
                                                            <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/dana.png"/>
                                                        </label>
                                                    </div>
                                                @endif
                                                @if(in_array("LINKAJA", $newPaymentMethods))
                                                    <div class="test form-check">
                                                        <input checked="true" class="form-check-input" type="checkbox" name="payment_method[]" value="LINKAJA" id="LINKAJA">
                                                        <label class="form-check-label" for="LINKAJA">
                                                            <img style="height : 45px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/linkaja.png"/>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="payment_method[]" value="LINKAJA" id="LINKAJA">
                                                        <label class="form-check-label" for="LINKAJA">
                                                            <img style="height : 45px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/linkaja.png"/>
                                                        </label>
                                                    </div>
                                                @endif
                                                @if(in_array("GOPAY", $newPaymentMethods))
                                                    <div class="test form-check">
                                                        <input checked="true" class="form-check-input" type="checkbox" name="payment_method[]" value="GOPAY" id="GOPAY">
                                                        <label class="form-check-label" for="GOPAY">
                                                            <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/gopay.png"/>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="payment_method[]" value="GOPAY" id="GOPAY">
                                                        <label class="form-check-label" for="GOPAY">
                                                            <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/gopay.png"/>
                                                        </label>
                                                    </div>
                                                @endif
                                                @if(in_array("SHOPEEPAY", $newPaymentMethods))
                                                    <div class="test form-check">
                                                        <input checked="true" class="form-check-input" type="checkbox" name="payment_method[]" value="SHOPEEPAY" id="SHOPEEPAY">
                                                        <label class="form-check-label" for="SHOPEEPAY">
                                                            <img style="height: 80px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/shppepay.png"/>
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="payment_method[]" value="SHOPEEPAY" id="SHOPEEPAY">
                                                        <label class="form-check-label" for="SHOPEEPAY">
                                                            <img style="height: 80px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/shppepay.png"/>
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_title', 'Voucher Title') }}
                                            <input type="text" class="form-control" placeholder="Voucher Title" name="voucher_title" value="{{ $vouchers['voucher_title'] }}">
                                        </div>
                                    </div>
                                    <div class=col-md-2>
                                        <div class="form-group has-feedback">
                                            {{ Form::label('order_type', 'Order type') }}
                                            <div class="form-group"> 
                                                <select name="order_type" class="form-control">
                                                    @if($vouchers['order_type'] == "pf")
                                                        <option selected="true" value="pf">PF</option>
                                                    @else
                                                        <option value="pf">PF</option>
                                                    @endif
                                                    @if($vouchers['order_type'] == "do")
                                                        <option selected="true" value="do">DO</option>
                                                    @else
                                                        <option value="do">DO</option>
                                                    @endif
                                                    @if($vouchers['order_type'] == "pl")
                                                        <option selected="true" value="pl">PL</option>
                                                    @else
                                                        <option value="pl">PL</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=col-md-2>
                                        <div id="thisWrapper" class="form-group has-feedback">
                                            {{ Form::label('service_type', 'Service type') }}
                                            <div id="service_types" class="test4 form-control">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('value', 'Voucher Value') }}
                                            <input type="text" class="form-control" placeholder="Voucher Value" name="voucher_value" value="{{ $vouchers['value'] }}">
                                        </div>
                                    </div>
                                    <div class=col-md-3>
                                        <div class="form-group has-feedback">
                                            {{ Form::label('unit', 'Unit') }}
                                            <div class="form-group"> 
                                                <select name="unit" class="form-control">
                                                    @if($vouchers['unit'] == "percent")
                                                        <option selected="true" value="percent">Percent</option>
                                                    @else 
                                                        <option value="percent">Percent</option>
                                                    @endif
                                                    @if($vouchers['unit'] == "price")
                                                        <option selected="true" value="price">Price</option>
                                                    @else 
                                                        <option value="price">Price</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('max_value', 'Maximum Value') }}
                                            <input type="text" class="form-control" placeholder="Max Value" name="max_value" value="{{ $vouchers['max_value'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_by_user_qty', 'Quantity') }}
                                            <input type="text" class="form-control" placeholder="Quantity" name="voucher_by_user_qty" value="{{ $vouchers['quantity'] }}" >
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <label>Date Range</label>
                    
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control float-right" name="date_range" id="reservation" value="{{ $newDateRange }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('date_range_start_time', 'Date Range Start Time') }}
                                            <input type="text" class="form-control" placeholder="start time 00:00:00" name="date_range_start_time" value="{{$newStartTime}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('date_range_expired_time', 'Date Range Expired Time') }}
                                            <input type="text" class="form-control" placeholder="expired time 23:59:59" name="date_range_expired_time" value="{{$newEndTime}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('min_shipping_price', 'Min Price') }}
                                            <input type="text" class="form-control" placeholder="Min shipping price" name="min_shipping_price" value="{{ $vouchers['min_shipping_price'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('max_shipping_price', 'Max Price') }}
                                            <input type="text" class="form-control" placeholder="Max shipping price" name="max_shipping_price" value="{{ $vouchers['max_shipping_price'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('promo_template', 'Promo Template') }}
                                            <input type="text" class="form-control" placeholder="Template #promo_price, #promo_percent, #logo" name="promo_template" value="{{ $vouchers['promo_template'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('description', 'Voucher Description') }}
                                            <textarea class="col-md-12" name="description" id="" rows="3"  placeholder="Deskripsi" >{{ $vouchers['description'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_image', 'Voucher Image') }}
                                                {{ Form::file('voucher_image', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_logo', 'Voucher promo Image') }}
                                                {{ Form::file('voucher_logo', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">
                                                Update Voucher
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
    let isServiceType = {!! json_encode($vouchers['service_codes']) !!};
    if (isServiceType == null) {
        $("#thisWrapper").hide();
    } else {
        isServiceType = isServiceType.replace("[", "")
        isServiceType = isServiceType.replace("]", "")
        var arrayService = isServiceType.split(",")

        let selected = {!! json_encode($vouchers['expedition_ids']) !!};
        selected = selected.replace("[", "")
        selected = selected.replace("]", "")
        var arraySelected = selected.split(",")

        if (arraySelected.length == 1) {

            var mapper = {!! json_encode($expeditions) !!};
            var service_types = mapper['mapper'][arraySelected[0]]
            let select = []
            for (let I = 0; I < service_types.length; I++) {
                if (arrayService.includes(service_types[I]['code'])) {
                    select.push(`<div on class="test form-check">
                        <input checked="true" class="form-check-input" type="checkbox" name="service_types[]" value="${service_types[I]['code']}" id="service_type-${service_types[I]['id']}">
                        <label class="form-check-label" for="service_type-${service_types[I]['id']}" >
                            ${service_types[I]['service_type']}
                        </label>
                    </div>`) 
                } else {
                    select.push(`<div on class="test form-check">
                        <input class="form-check-input" type="checkbox" name="service_types[]" value="${service_types[I]['code']}" id="service_type-${service_types[I]['id']}">
                        <label class="form-check-label" for="service_type-${service_types[I]['id']}" >
                            ${service_types[I]['service_type']}
                        </label>
                    </div>`)
                }
            }
            $("#service_types").empty()
            $("#service_types").append(select)
            $("#thisWrapper").show();
        } else {
            $("#service_types").empty()
            $("#thisWrapper").hide();
        }

    }
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })

        $('.form-check-input').change(function() {
            var checkedNum = $('input[name="expeditions[]"]:checked').length;
            if (checkedNum == 1) {
                var selected_expedition;
                var ayam = $('input[name="expeditions[]"]:checked').each(function() {
                    selected_expedition = $(this).val()
                })

                var mapper = {!! json_encode($expeditions) !!};
                var service_types = mapper['mapper'][selected_expedition]

                let select = []
                for (let I = 0; I < service_types.length; I++) {
                    select.push(`<div on class="test form-check">
                        <input class="form-check-input" type="checkbox" name="service_types[]" value="${service_types[I]['code']}" id="service_type-${service_types[I]['id']}">
                        <label class="form-check-label" for="service_type-${service_types[I]['id']}" >
                            ${service_types[I]['service_type']}
                        </label>
                    </div>`)
                }
                $("#service_types").empty()
                $("#service_types").append(select)
                $("#thisWrapper").show();
            } else {
                let select = []
                $("#service_types").empty()
                $("#thisWrapper").hide();
            }
        })
    })

   

</script>
@endsection