@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    @if (Session::has('error'))
                    <div id="alert-msg" class="alert alert-error alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('error') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/app-management/voucher/shop') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_type', 'Voucher Type') }}
                                            <div class="form-group"> 
                                                <select name="voucher_type" class="form-control">
                                                    <option value="cashback">Cashback</option>
                                                    <option value="discount">Discount</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('shop_id', 'Shop ID') }}
                                                <input type="text" class="form-control" placeholder="Shop ID" name="shop_id" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('code', 'Voucher Code') }}
                                                <input type="text" class="form-control" placeholder="Voucher Code" name="code" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('expedition_ids', 'Expedition Ids') }}
                                                <div class="test2 form-control">
                                                @foreach($expeditions['results'] as $expedition)
                                                    <div on class="test form-check">
                                                        <input class="form-check-input" type="checkbox" name="expeditions[]" value="{{$expedition['id']}}" id="expedition-{{$expedition['id']}}">
                                                        <label class="form-check-label" for="expedition-{{$expedition['id']}}">
                                                            <img class="image-exp" src="{{$expedition['logo_url']}}" alt="{{$expedition['name']}} "/>
                                                        </label>
                                                    </div>
                                                @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('payment_method', 'Payment Method') }}
                                            <div class="test3 form-control">
                                                <div class="test form-check">
                                                    <input class="form-check-input" type="checkbox" name="payment_method[]" value="OVO" id="OVO">
                                                    <label class="form-check-label" for="OVO">
                                                        <img style="height: 25px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/ovo.png"/>
                                                    </label>
                                                </div>
                                                <div class="test form-check">
                                                    <input class="form-check-input" type="checkbox" name="payment_method[]" value="DANA" id="DANA">
                                                    <label class="form-check-label" for="DANA">
                                                        <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/dana.png"/>
                                                    </label>
                                                </div>
                                                <div class="test form-check">
                                                    <input class="form-check-input" type="checkbox" name="payment_method[]" value="LINKAJA" id="LINKAJA">
                                                    <label class="form-check-label" for="LINKAJA">
                                                        <img style="height : 45px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/linkaja.png"/>
                                                    </label>
                                                </div>
                                                <div class="test form-check">
                                                    <input class="form-check-input" type="checkbox" name="payment_method[]" value="GOPAY" id="GOPAY">
                                                    <label class="form-check-label" for="GOPAY">
                                                        <img class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/gopay.png"/>
                                                    </label>
                                                </div>
                                                <div class="test form-check">
                                                    <input class="form-check-input" type="checkbox" name="payment_method[]" value="SHOPEEPAY" id="SHOPEEPAY">
                                                    <label class="form-check-label" for="SHOPEEPAY">
                                                        <img style="height: 80px" class="payment-logo" src="https://keyta-storage.s3.ap-southeast-1.amazonaws.com/prd/gamification/rewards/shppepay.png"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_title', 'Voucher Title') }}
                                            <input type="text" class="form-control" placeholder="Voucher Title" name="voucher_title" value="">
                                        </div>
                                    </div>
                                    <div class=col-md-2>
                                        <div class="form-group has-feedback">
                                            {{ Form::label('order_type', 'Order type') }}
                                            <div class="form-group"> 
                                                <select name="order_type" class="form-control">
                                                    <option value="pf">PF</option>
                                                    <option value="do">DO</option>
                                                    <option value="pl">PL</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=col-md-2>
                                        <div id="thisWrapper" class="form-group has-feedback">
                                            {{ Form::label('service_type', 'Service type') }}
                                            <div id="service_types" class="test4 form-control">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('value', 'Voucher Value') }}
                                            <input type="text" class="form-control" placeholder="Voucher Value" name="voucher_value" value="">
                                        </div>
                                    </div>
                                    <div class=col-md-3>
                                        <div class="form-group has-feedback">
                                            {{ Form::label('unit', 'Unit') }}
                                            <div class="form-group"> 
                                                <select name="unit" class="form-control">
                                                    <option value="percent">Percent</option>
                                                    <option value="price">Price</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('max_value', 'Maximum Value') }}
                                            <input type="text" class="form-control" placeholder="Max Value" name="max_value" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_by_user_qty', 'Quantity') }}
                                            <input type="text" class="form-control" placeholder="Quantity" name="voucher_by_user_qty" value="">
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <label>Date Range</label>
                    
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('date_range_start_time', 'Date Range Start Time') }}
                                            <input type="text" class="form-control" placeholder="start time 00:00:00" name="date_range_start_time" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('date_range_expired_time', 'Date Range Expired Time') }}
                                            <input type="text" class="form-control" placeholder="expired time 23:59:59" name="date_range_expired_time" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('min_shipping_price', 'Min Price') }}
                                            <input type="text" class="form-control" placeholder="Min shipping price" name="min_shipping_price" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('max_shipping_price', 'Max Price') }}
                                            <input type="text" class="form-control" placeholder="Max shipping price" name="max_shipping_price" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('promo_template', 'Promo Template') }}
                                            <input type="text" class="form-control" placeholder="Template #promo_price, #promo_percent, #logo" name="promo_template" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('description', 'Voucher Description') }}
                                            <textarea class="col-md-12" name="description" id="" rows="3"  placeholder="Deskripsi"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_image', 'Voucher Image') }}
                                                {{ Form::file('voucher_image', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_logo', 'Voucher promo Image') }}
                                                {{ Form::file('voucher_logo', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">
                                                Give Voucher
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
    $("#thisWrapper").hide();
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })

        $('.form-check-input').change(function() {
            var checkedNum = $('input[name="expeditions[]"]:checked').length;
            if (checkedNum == 1) {
                var selected_expedition;
                var ayam = $('input[name="expeditions[]"]:checked').each(function() {
                    selected_expedition = $(this).val()
                })

                var mapper = {!! json_encode($expeditions) !!};
                var service_types = mapper['mapper'][selected_expedition]

                let select = []
                for (let I = 0; I < service_types.length; I++) {
                    select.push(`<div on class="test form-check">
                        <input class="form-check-input" type="checkbox" name="service_types[]" value="${service_types[I]['code']}" id="service_type-${service_types[I]['id']}">
                        <label class="form-check-label" for="service_type-${service_types[I]['id']}" >
                            ${service_types[I]['service_type']}
                        </label>
                    </div>`)
                }
                $("#service_types").empty()
                $("#service_types").append(select)
                $("#thisWrapper").show();
            } else {
                let select = []
                $("#service_types").empty()
                $("#thisWrapper").hide();
            }
        })
    })

   

</script>
@endsection