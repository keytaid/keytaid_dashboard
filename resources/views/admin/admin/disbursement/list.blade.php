@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                </div>

                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Xendit Cash</span>
                                    <span class="info-box-number">{{ number_format($balance_cash['balance'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Keytasaldo</span>
                                    <span class="info-box-number">{{ number_format($balance_saldo, 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Xendit - Keytasaldo</span>
                                    <span class="info-box-number">{{ number_format($balance_cash['balance'] - $balance_saldo, 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Complete disbursement</span>
                                    <span class="info-box-number">{{ number_format($disbursements['amount_completed_disburse'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Fee Xendit + VAT </span>
                                    <span class="info-box-number">{{ number_format($disbursements['amount_need_to_pay'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Fee from user</span>
                                    <span class="info-box-number">{{ number_format($disbursements['amount_fee_paid'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Keyta Subsidy</span>
                                    <span class="info-box-number">{{ number_format($disbursements['amount_need_to_pay'] - $disbursements['amount_fee_paid'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/disbursement/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>                                  
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Tanggal</th>
                                        <th>Shop id</th>
                                        <th>Shop Name</th>
                                        <th>Jumlah</th>
                                        <th>Fee before disc</th>
                                        <th>Fee after disc</th>
                                        <th>Fee Xendit</th>
                                        <th>VAT</th>
                                        <th>Net Payment</th>
                                        <th>Status</th>
                                        <th>Disbursment method</th>
                                        <th>Nama pemilik</th>
                                        <th>Nomor akun</th>
                                        <th>Reference id</th>
                                        <th>Failure code</th>
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $no = 1 + (($disbursements['current_page']-1)*$disbursements['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>

                            @foreach($disbursements['results'] as $disbursement)
                                <tr>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $no++ }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ str_replace("T"," ",substr($disbursement['created_at'], 0, 19))}}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $disbursement['shop_detail']['id'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $disbursement['shop_detail']['name'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['amount'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['service_fee_before_discount'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['service_fee'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['xendit_fee'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['vat'], 0, ',', '.') }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($disbursement['net_payment'], 0, ',', '.') }}</td>
                                    @if($disbursement['status'] == 'cancelled')
                                        <td style="vertical-align : middle;text-align:center;" class="text-center
                                        @if($disbursement['status'] == 'cancelled')
                                            {{'error'}}
                                        @endif
                                        ">failed</td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center
                                        @if($disbursement['status'] == 'completed')
                                            {{'success'}}
                                        @else
                                            {{'error'}}
                                        @endif
                                        ">{{ $disbursement['status'] }}</td>
                                    @endif
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $disbursement['disbursement_method'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $disbursement['account_name'] }}</td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $disbursement['account_number'] }}</td>
                                    @if(count($disbursement['payments']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($disbursement['payments'] as $payment_detail)
                                                <li>{{ strtoupper($payment_detail['external_id']) }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif

                                    @if(count($disbursement['payments']) >= 1)
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                            @foreach($disbursement['payments'] as $payment_detail)
                                                <li>{{ strtoupper($payment_detail['failure_code']) }}</li>
                                            @endforeach
                                        </td>
                                    @else
                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>

                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $disbursements['current_page'] + 2; 
                                    $start_page = $disbursements['current_page'] - 2;
                                ?>
                                @if($access_level['role_id']==2)
                                    @if ($disbursements['current_page'] > 1)
                                        <?php $before_page = $disbursements['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/disbursement/1?date_range='.$date_range) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/disbursement/'.$before_page.'?date_range='.$date_range) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $disbursements['total_pages'])
                                            <li class="page-item @if($disbursements['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/disbursement/'.$i.'?date_range='.$date_range) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($disbursements['current_page'] < $disbursements['total_pages'])
                                        <?php $next_page = $disbursements['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/disbursement/'.$next_page.'?date_range='.$date_range) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/disbursement/'.$disbursements['total_pages'].'?date_range='.$date_range) }}">Last</a></li>
                                    @endif
                                @endif
                            </ul>
                        </nav>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection