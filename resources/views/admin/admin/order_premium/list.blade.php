@extends('admin/admin')
@section('content')
    <div class="container-fluid">
        <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exportModalLabel">Order Premium Export</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/premium/exportnew') }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                <!-- <div class="col-md-1"></div> -->
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label>Date Range</label>
                
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label>Whatsapp number</label>
                
                                        <div class="input-group">
                                            <input type="text" class="form-control float-right" name="whatsapp" id="reservation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$title}}</h3>
                        
                        <div class="card-tools">
                            
                        </div>
                    </div>
                    <div class="card-body">
                        @if (Session::has('message'))
                            <div id="alert-msg" class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Activate (user)</span>
                                    <span class="info-box-number">{{ number_format($orders['total'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gross Revenue Katalog Produk</span>
                                    <span class="info-box-number">{{"Rp. ".number_format($orders['order_value'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        <style>
                            a.disabled {
                                pointer-events: none;
                                color: #ccc;
                            }
                        </style>

                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/orders-premium/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>                                  
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-1 text-center">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </button>
                                {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a> --}}
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/orders-premium/search/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                    </div>
                                </form>
                            </div>
                        </div> 

                        <br> 
                        <div class="scrolling-wrapper">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th>No</th>
                                        <th>Tanggal Order</th>
                                        <th>Shop Id</th>
                                        <th>Nama Toko</th>
                                        <th>Feature</th>
                                        <th>Duration</th>
                                        <th>Quantity</th>
                                        <th>Start</th>
                                        <th>Expired</th>
                                        <th>Status</th>
                                        <th>Voucher Value</th>
                                        <th>Voucher Title</th>
                                        <th>Pricing</th>
                                        <th>Total Amount (after disc)</th>
                                        <th>Payment KP</th>
                                        <th>Payment KS</th>
                                        <th>Payment Ewallet</th>
                                        <th>Payment Method</th>
                                        <th>Reference Id Wallet</th>
                                        <th>Reference Id Keytapoint</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                        $GLOBAL['order_id'] = 0;
                                        $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                        $last_id = 0;
                                        $i = 0;
                                        // echo $orders
                                    ?>

                                    @foreach($orders['results'] as $order)  
                                        <tr>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $no++ }}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ str_replace("T"," ",substr($order['created_at'], 0, 19))}}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['shops']['id']}}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['shops']['name']}}</td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['premium_feature'] as $premium_name)
                                                    <li>{{ $premium_name }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['premium_duration'] as $premium_duration)
                                                    <li>{{ $premium_duration }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['premium_quantity'] as $premium_quantity)
                                                    <li>{{ $premium_quantity }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['start_date'] as $premium_start_date)
                                                    <li>{{ $premium_start_date }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['expired_date'] as $premium_expired_date)
                                                    <li>{{ $premium_expired_date }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['status'] }}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($order['voucher_value'], 0, ',', '.') }}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['voucher_title'] }}</td>
                                            <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                @foreach($order['details']['premium_price'] as $premium_price)
                                                    <li>{{ number_format($premium_price, 0, ',', '.') }}</li>
                                                @endforeach
                                            </td>

                                            <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($order['amount'], 0, ',', '.') }}</td>

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @foreach($order['payments'] as $payment_detail)
                                                        @if( $payment_detail['payment_method'] == "KEYTAPOINT")
                                                            <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($order['keytapoint_payment'], 0, ',', '.') }}</td>
                                            @endif

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @foreach($order['payments'] as $payment_detail)
                                                        @if( strtoupper($payment_detail['payment_method']) == "KEYTASALDO")
                                                            <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                            @endif

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @foreach($order['payments'] as $payment_detail)
                                                        @if( $payment_detail['payment_method'] != "KEYTAPOINT" and strtoupper($payment_detail['payment_method']) != "KEYTASALDO")
                                                            <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($order['ewallet_payment'], 0, ',', '.') }}</td>
                                            @endif

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @foreach($order['payments'] as $payment_detail)
                                                        <li>{{ strtoupper($payment_detail['payment_method'])}}</li>
                                                    @endforeach
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['premium_payments']['ewallet_type'] }}</td>
                                            @endif

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @if( $payment_detail['payment_method'] != "KEYTAPOINT")
                                                        <li>{{ $payment_detail['external_id'] }}</li>
                                                    @endif
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['premium_payments']['external_id'] }}</td>
                                            @endif

                                            @if(count($order['payments']) >= 1)
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                    @if( $payment_detail['payment_method'] == "KEYTAPOINT")
                                                        <li>{{ $payment_detail['external_id'] }}</li>
                                                    @endif
                                                </td>
                                            @else
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $order['premium_payments']['external_id'] }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-2 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php 
                                        $show_page = $orders['current_page'] + 2; 
                                        $start_page = $orders['current_page'] - 2;
                                    ?>
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/1?date_range='.$date_range) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$before_page.'?date_range='.$date_range) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item 
                                                @if($orders['current_page'] == $i) active @endif">
                                                <a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$i.'?date_range='.$date_range) }}">{{$i}}</a>
                                            </li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$next_page.'?date_range='.$date_range) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$orders['total_pages'].'?date_range='.$date_range) }}">Last</a></li>
                                    @endif
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
    </script>
@endsection