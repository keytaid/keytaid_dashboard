@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">
  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('admin/'.$user_id.'/pricing-premium/store') }}" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-header">
          <div class="">
            <h3>Add {{$title}}</h2>
          </div>
        </div>
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif

          <div class="row">
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('premium_feature_id', 'Premium Feature') }}
                <div class="form-group"> 
                  <select name="premium_feature_id" class="form-control">
                    <option>Select Feature</option>
                    <option value="1">E-Invoice</option>
                    <option value="2">Katalog Produk</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('is_trial', 'Trial') }}
                <div class="form-group"> 
                  <select name="is_trial" class="form-control">
                    <option>Select Type</option>
                    <option value="0">Paid</option>
                    <option value="1">Trial</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('duration', 'Duration') }}
                      <input type="text" class="form-control" placeholder="Duration" name="duration" value="">
                  </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-feedback">
                {{ Form::label('duration_type', 'Type') }}
                <div class="form-group"> 
                  <select name="duration_type" class="form-control">
                    <option>Duration Type</option>
                    <option value="minggu">Minggu</option>
                    <option value="bulan">Bulan</option>
                    <option value="tahun">Tahun</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('quantity', 'Quantity') }}
                      <input type="text" class="form-control" placeholder="Quantity" name="quantity" value="">
                  </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group has-feedback">
                {{ Form::label('quantity_type', 'Type') }}
                <div class="form-group"> 
                  <select name="quantity_type" class="form-control">
                    <option>Quantity Type</option>
                    <option value="pcs">PCS</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('price', 'Price') }}
                      <input type="text" class="form-control" placeholder="Price" name="price" value="">
                  </div>
              </div>
            </div>
          </div>


        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Add {{$title}}</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection