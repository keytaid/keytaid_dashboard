@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    @if($access_level['role_id']==1)
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Creation</span>
                                    <span class="info-box-number">{{$orders['total']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Orders Creation</span>
                                    <span class="info-box-number">{{$orders['total_users']}}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Creation</span>
                                    <span class="info-box-number">{{$orders['total']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Orders Creation</span>
                                    <span class="info-box-number">{{$orders['total_users']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gross Revenue</span>
                                    <span class="info-box-number">{{"Rp. ".number_format($orders['order_value'], 0)}}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-1 text-center">
                                <a href="{{URL::to('/admin/'.$user_id.'/orders/export')}}">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/orders/search/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="">
                                    </div>
                                </form>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-4 text-center">
                                <span></span>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($expedition_name == null)
                                        Select Expedition
                                    @else
                                        {{$expedition_name}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=grab_express&payment_method='.$payment_method) }}">Grab Express</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=gojek&payment_method='.$payment_method) }}">Go-Send</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=anteraja&payment_method='.$payment_method) }}">Anter Aja</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=paxel&payment_method='.$payment_method) }}">Paxel</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=sicepat&payment_method='.$payment_method) }}">Sicepat</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jne&payment_method='.$payment_method) }}">JNE</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jnt&payment_method='.$payment_method) }}">J&T</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=pos&payment_method='.$payment_method) }}">POS Indonesia</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=deliveree&payment_method='.$payment_method) }}">Deliveree</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=ninja&payment_method='.$payment_method) }}">Ninja Xpress</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=tiki&payment_method='.$payment_method) }}">Tiki</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=wahana&payment_method='.$payment_method) }}">Wahana</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=lion&payment_method='.$payment_method) }}">Lion Parcel</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=mrspeedy&payment_method='.$payment_method) }}">Mr Speedy</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=rpx&payment_method='.$payment_method) }}">RPX</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jet&payment_method='.$payment_method) }}">JET</a>
                                </div>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($payment_method == null)
                                        Select Payment
                                    @else
                                        {{$payment_method}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=OVO') }}">OVO</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=DANA') }}">DANA</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=LINKAJA') }}">LINKAJA</a>
                                </div>
                            </div>
                        </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Tanggal Order</th>
                                        <th>Order Id</th>
                                        <th>Payment Id</th>
                                        <th>Shop Id</th>
                                        <th>Nama Toko</th>
                                        <th>Kode Invoice</th>
                                        <th>Status</th>
                                        <th>Expedition</th>
                                        <th>Layanan</th>
                                        <th>Berat</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Expedition External Id</th>
                                        <th>Shipping Price</th>
                                        <th>Payment Method</th>
                                        <th>Reference Id</th>
                                        <th>Tanggal Invoice</th>
                                        <th>Action</th>
                                    @elseif($access_level['role_id']==1)
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Kode Invoice</th>
                                        <th>Expedition</th>
                                        <th>Layanan</th>
                                        <th>Ongkos Kirim</th>
                                        <th>Status</th>
                                        
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                                $GLOBAL['order_id'] = 0;
                                $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>

                                @foreach($orders['results'] as $transaction)
                                        <tr>
                                            
                                                <td class="text-center">{{ $no++ }}</td>
                                                @if($access_level['role_id']==2)
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['created_at']}}</td>
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['id']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    <?php 
                                                        if(count($transaction['payment2']) > 1){
                                                            if($last_id == $transaction['id']){
                                                                $i++;
                                                            }
                                                            else{
                                                                $i=0;
                                                            }
                                                        }
                                                        else{
                                                            $i=0;
                                                        }
                                                    ?>
                                                    <td class="text-center">{{ $transaction['payment2'][$i]['id'] }}</td>
                                                    <td class="text-center">{{ $transaction['shop']['id'] }}</td>
                                                    <td class="text-center">{{ $transaction['shop']['name'] }}</td>
                                                    <td class="text-center">{{ $transaction['number'] }}</td>

                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center
                                                        @if($transaction['payment2'][$i]['order2']['status'] == 'COMPLETED' or $transaction['payment2'][$i]['order2']['status'] == 'delivered' or $transaction['payment2'][$i]['order2']['status'] == 'completed')
                                                            {{'success'}}
                                                        @elseif($transaction['payment2'][$i]['order2']['status'] == 'FAILED' or $transaction['payment2'][$i]['order2']['status'] == 'cancelled' or $transaction['payment2'][$i]['order2']['status'] == 'cancelled_by_user' or $transaction['payment2'][$i]['order2']['status'] == 'failed')   
                                                            {{'error'}}
                                                        @else
                                                            {{'info'}}
                                                        @endif
                                                        ">{{ $transaction['payment2'][$i]['order2']['status']}}</td>
                                                    @else
                                                        <td class="text-center error">Order not created</td>
                                                    @endif

                                                    <td class="text-center">{{ $transaction['expedition']['name']}}</td>
                                                    <td class="text-center">{{ $transaction['expedition']['expedition_service_type']}}</td>
                                                    
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['weight']}}</td>
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['sender_name']}}</td>
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['receipent_name']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['expedition_external_number']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    <td class="text-center">{{ $transaction['payment2'][$i]['amount']}}</td>
                                                    <td class="text-center">{{ $transaction['payment2'][$i]['ewallet_type']}}</td>
                                                    <td class="text-center">{{ $transaction['payment2'][$i]['external_id']}}</td>
                                                    
                                                    <td class="text-center">{{ $transaction['created_at']}}</td>
                                                    <td>
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        
                                                        @if($transaction['payment2'][$i]['order2']['status'] == "confirmed" || $transaction['payment2'][$i]['order2']['status'] == "allocating" || $transaction['payment2'][$i]['order2']['status'] == "picking_up" || $transaction['payment2'][$i]['order2']['status'] == "allocated")
                                                            <a class="btn btn-primary btn-md disabled" href="{{URL::to('/admin/'.$user_id.'/orders/cancel/'.$transaction['payment2'][$i]['order2']['id'])}}">Cancel Order</a>
                                                        @else
                                                            <a class="btn btn-secondary btn-md disabled" href="{{URL::to('/admin/'.$user_id.'/orders/cancel/'.$transaction['payment2'][$i]['order2']['id'])}}">Cancel Order</a>
                                                        @endif
                                                    @endif
                                                    </td>
                                                @elseif($access_level['role_id']==1)
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center">{{ $transaction['payment2'][$i]['order2']['created_at']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    <td class="text-center">{{ $transaction['shop']['name'] }}</td>
                                                    <td class="text-center">{{ $transaction['number'] }}</td>
                                                    <td class="text-center">{{ $transaction['expedition']['name']}}</td>
                                                    <td class="text-center">{{ $transaction['expedition']['expedition_service_type']}}</td>
                                                    <td class="text-center">{{ $transaction['payment2'][$i]['amount']}}</td>
                                                    @if(isset($transaction['payment2'][$i]['order2']))
                                                        <td class="text-center
                                                        @if($transaction['payment2'][$i]['order2']['status'] == 'COMPLETED' or $transaction['payment2'][$i]['order2']['status'] == 'delivered' or $transaction['payment2'][$i]['order2']['status'] == 'completed')
                                                            {{'success'}}
                                                        @elseif($transaction['payment2'][$i]['order2']['status'] == 'FAILED' or $transaction['payment2'][$i]['order2']['status'] == 'cancelled' or $transaction['payment2'][$i]['order2']['status'] == 'cancelled_by_user' or $transaction['payment2'][$i]['order2']['status'] == 'failed')   
                                                            {{'error'}}
                                                        @else
                                                            {{'info'}}
                                                        @endif
                                                        ">{{ $transaction['payment2'][$i]['order2']['status']}}</td>
                                                    @else
                                                        <td class="text-center error">Order not created</td>
                                                    @endif
                                                @endif
                                                <?php 
                                                    $last_id = $transaction['id'];
                                                ?>
                                        </tr>
                                        
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $orders['current_page'] + 2; 
                                    $start_page = $orders['current_page'] - 2;
                                ?>
                                @if($access_level['role_id']==2)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$before_page.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$i.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$next_page.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$orders['total_pages'].'?expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Last</a></li>
                                    @endif
                                @elseif($access_level['role_id']==1)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$i) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$orders['total_pages']) }}">Last</a></li>
                                    @endif
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <form method="POST" action="{{ URL::to('/admin/'.$user_id.'/orders/cancel/'.$GLOBAL['order_id']) }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <button type="submit" class="btn btn-danger">Cancel Order</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection