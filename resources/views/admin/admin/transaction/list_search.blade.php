@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/transactions/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/transactions/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-2 text-center">
                            <span></span>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($channel == null)
                                    Select Channel
                                @else
                                    {{$channel}}
                                @endif
                            </button>
                            <?php
                                $channels = array("instagram","whatsapp","whatsapp_bussiness","facebook_messenger","line","line_official","tiktok","wechat","telegram","tokopedia","shopee","bukalapak")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($channels as $channel_list)
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel_list.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status) }}">{{$channel_list}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($expedition_name == null)
                                    Select Expedition
                                @else
                                    {{$expedition_name}}
                                @endif
                            </button>
                            <?php
                                    $expeditions = array("grab_express","gojek","anteraja","paxel","sicepat","jne","jnt","pos","deliveree","ninja","tiki","wahana","lion","mrspeedy","rpx","jet")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($expeditions as $expedition_list)
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_list.'&payment_method='.$payment_method.'&status='.$status) }}">{{$expedition_list}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($payment_method == null)
                                    Select Payment
                                @else
                                    {{$payment_method}}
                                @endif
                            </button>
                            <?php
                                $payments = array("BCA","Mandiri","BNI","BRI","BTPN","CIMB","Danamon","HSBC","Panin","DBS","Mega")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($payments as $payment)
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment.'&status='.$status) }}">{{$payment}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($status == null)
                                    Select Status
                                @else
                                    {{$status}}
                                @endif
                            </button>
                            <?php
                                $statuses = array("Pending","Terbayar","Dikirim","Selesai","Batal")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($statuses as $status_list)
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status_list) }}">{{$status_list}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Tanggal Invoice</th>
                                    <th>Shop Id</th>
                                    <th>Nama Toko</th>
                                    <th>Kode Invoice</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Channel</th>
                                    <th>Channel Phone/ID</th>
                                    <th>Expedition</th>
                                    <th>Layanan</th>
                                    <th>Shipping Price</th>
                                    <th>Total Price</th>
                                    <th>Payment Method</th>
                                    <th>Status</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($transactions['current_page']-1)*$transactions['limit']);
                                    // echo $transactions
                                ?>

                                @foreach($transactions['result'] as $transaction)
                                <tr>
                                
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $transaction['created_at'] }}</td>
                                        @if(isset($transaction['shop']))
                                            <td class="text-center">{{ $transaction['shop']['id'] }}</td>
                                            <td class="text-center">{{ $transaction['shop']['name'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        <td class="text-center">{{ $transaction['number'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['customer_name'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['channel'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['channel_account'] }}</td>
                                        @if(isset($transaction['expedition']))
                                            <td class="text-center">{{ $transaction['expedition']['name'] }}</td>
                                            <td class="text-center">{{ $transaction['expedition']['expedition_service_type'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        <td class="text-center">{{ $transaction['shipping_price'] }}</td>
                                        <td class="text-center">{{ $transaction['total_price'] }}</td>
                                        @if(isset($transaction['bank']))
                                            <td class="text-center">{{ $transaction['bank']['name'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                        
                                        @if($transaction["payment_status"] == "pending" && $transaction["shipment_status"] == "pending")
                                            <td class="text-center">Pending</td>
                                        @elseif($transaction["payment_status"] == "paid" && $transaction["shipment_status"] == "pending")
                                            <td class="text-center">Terbayar</td>
                                        @elseif($transaction["payment_status"] != "cancelled" && $transaction["shipment_status"] == "shipped")
                                            <td class="text-center">Dikirim</td>
                                        @elseif($transaction["payment_status"] != "cancelled" && $transaction["shipment_status"] == "completed")
                                            <td class="text-center">Selesai</td>
                                        @elseif($transaction["payment_status"] == "cancelled")
                                            <td class="text-center">Batal</td>
                                        @endif
                                        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $transactions['current_page'] + 2; 
                                    $start_page = $transactions['current_page'] - 2;
                                ?>
                                
                                @if ($transactions['current_page'] > 1)
                                    <?php $before_page = $transactions['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/transactions/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/transactions/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $transactions['total_pages'])
                                        <li class="page-item @if($transactions['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/transactions/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($transactions['current_page'] < $transactions['total_pages'])
                                    <?php $next_page = $transactions['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/transactions/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/transactions/search/'.$transactions['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection