@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exportModalLabel">Transaction Export</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/transactions/export') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <!-- <div class="col-md-1"></div> -->
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label>Date Range</label>
            
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label>Whatsapp number</label>
            
                                    <div class="input-group">
                                        <input type="text" class="form-control float-right" name="whatsapp" id="reservation">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="md-form active-cyan active-cyan-2 mb-3">
                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">

                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <div class="row">
                        <form class="col-6" id="form_tanggal" method="GET"
                            action="{{ URL::to('/admin/'.$user_id.'/transactions/1?date_range='.$date_range) }}">
                            <div class="form-group">
                                <label>Pilih Tanggal:</label>

                                <div class="input-group">
                                    <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                        <i class="fa fa-calendar"></i> {{$date_range}}
                                        <i class="fa fa-caret-down"></i>
                                        <input type="hidden" name="channel" value="{{$channel}}">
                                        <input type="hidden" name="expedition_name" value="{{$expedition_name}}">
                                        <input type="hidden" name="payment_method" value="{{$payment_method}}">
                                        <input type="hidden" name="status" value="{{$status}}">
                                        <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Invoice Creation</span>
                                    <span class="info-box-number">{{ number_format($transactions['total'], 0, ',', '.') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Invoice Creation</span>
                                    <span class="info-box-number">{{ number_format($transactions['total_users'], 0, ',', '.') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Invoice Value</span>
                                    <span
                                        class="info-box-number">{{"Rp. ".number_format($transactions['invoice_value'], 0, ',', '.') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </button>
                            {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a> --}}
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/transactions/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="">
                                    
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-2 text-center">
                            <span></span>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($channel == null)
                                Select Channel
                                @else
                                {{$channel}}
                                @endif
                            </button>
                            <?php
                                $channels = array("instagram","whatsapp","whatsapp_bussiness","facebook_messenger","line","line_official","tiktok","wechat","telegram","tokopedia","shopee","bukalapak")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($channels as $channel_list)
                                <a class="dropdown-item"
                                    href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel_list.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">{{$channel_list}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($expedition_name == null)
                                Select Expedition
                                @else
                                {{$expedition_name}}
                                @endif
                            </button>
                            <?php
                                    $expeditions = array("grab_express","gojek","anteraja","paxel","sicepat","jne","jnt","pos","deliveree","ninja","tiki","wahana","lion","mrspeedy","rpx","jet")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($expeditions as $expedition_list)
                                <a class="dropdown-item"
                                    href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_list.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">{{$expedition_list}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($payment_method == null)
                                Select Payment
                                @else
                                {{$payment_method}}
                                @endif
                            </button>
                            <?php
                                $payments = array("BCA","Mandiri","BNI","BRI","BTPN","CIMB","Danamon","HSBC","Panin","DBS","Mega")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($payments as $payment)
                                <a class="dropdown-item"
                                    href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment.'&status='.$status.'&date_range='.$date_range) }}">{{$payment}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($status == null)
                                Select Status
                                @else
                                {{$status}}
                                @endif
                            </button>
                            <?php
                                $statuses = array("Pending","Terbayar","Dikirim","Selesai","Batal")
                            ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($statuses as $status_list)
                                <a class="dropdown-item"
                                    href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status_list.'&date_range='.$date_range) }}">{{$status_list}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Tanggal Invoice</th>
                                    <th>Shop Id</th>
                                    <th>Nama Toko</th>
                                    <th>Kode Invoice</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Channel</th>
                                    <th>Channel Phone/ID</th>
                                    <th>Expedition</th>
                                    <th>Layanan</th>
                                    <th>Shipping Price</th>
                                    <th>Total Price</th>
                                    <th>Payment Method</th>
                                    <th>Status</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($transactions['current_page']-1)*$transactions['limit']);
                                    // echo $transactions
                                ?>

                                @foreach($transactions['results'] as $transaction)
                                <tr>

                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ $transaction['created_at'] }}</td>
                                    @if(isset($transaction['shop']))
                                    <td class="text-center">{{ $transaction['shop']['id'] }}</td>
                                    <td class="text-center">{{ $transaction['shop']['name'] }}</td>
                                    @else
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">{{ $transaction['number'] }}</td>
                                    <td class="text-center">{{ $transaction['customer']['customer_name'] }}</td>
                                    <td class="text-center">{{ $transaction['customer']['channel'] }}</td>
                                    <td class="text-center">{{ $transaction['customer']['channel_account'] }}</td>
                                    @if(isset($transaction['expedition']['name']))
                                    <td class="text-center">{{ $transaction['expedition']['name'] }}</td>
                                    <td class="text-center">{{ $transaction['expedition']['expedition_service_type'] }}
                                    </td>
                                    @else
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    @endif

                                    <td class="text-center">{{ number_format($transaction['shipping_price'], 0, ',', '.') }}</td>
                                    <td class="text-center">{{ number_format($transaction['total_price'], 0, ',', '.') }}</td>
                                    @if(isset($transaction['bank']))
                                        <td class="text-center">{{ $transaction['bank']['name'] }}</td>
                                    @else
                                        <td class="text-center"></td>
                                    @endif

                                    @if($transaction["payment_status"] == "pending" && $transaction["shipment_status"] == "pending")
                                    <td class="text-center">Pending</td>
                                    @elseif($transaction["payment_status"] == "paid" && $transaction["shipment_status"] == "pending")
                                    <td class="text-center">Terbayar</td>
                                    @elseif($transaction["payment_status"] != "cancelled" &&
                                    $transaction["shipment_status"] == "shipped")
                                    <td class="text-center">Dikirim</td>
                                    @elseif($transaction["payment_status"] != "cancelled" &&
                                    $transaction["shipment_status"] == "completed")
                                    <td class="text-center">Selesai</td>
                                    @elseif($transaction["payment_status"] == "cancelled")
                                    <td class="text-center">Batal</td>
                                    @endif


                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $transactions['current_page'] + 2; 
                                    $start_page = $transactions['current_page'] - 2;
                                ?>

                                @if ($transactions['current_page'] > 1)
                                <?php $before_page = $transactions['current_page'] - 1 ?>
                                <li class="page-item"><a class="page-link"
                                        href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">First</a>
                                </li>
                                <li class="page-item"><a class="page-link"
                                        href="{{ URL::to('/admin/'.$user_id.'/transactions/'.$before_page.'?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">Previous</a>
                                </li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++) @if ($i> 0 and $i <=
                                        $transactions['total_pages']) <li
                                        class="page-item @if($transactions['current_page'] == $i) active @endif"><a
                                            class="page-link"
                                            href="{{ URL::to('/admin/'.$user_id.'/transactions/'.$i.'?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">{{$i}}</a>
                                        </li>
                                        @endif
                                        @endfor
                                        @if ($transactions['current_page'] < $transactions['total_pages'])
                                            <?php $next_page = $transactions['current_page'] + 1 ?> <li
                                            class="page-item"><a class="page-link"
                                                href="{{ URL::to('/admin/'.$user_id.'/transactions/'.$next_page.'?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">Next</a>
                                            </li>
                                            <li class="page-item"><a class="page-link"
                                                    href="{{ URL::to('/admin/'.$user_id.'/transactions/'.$transactions['total_pages'].'?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status.'&date_range='.$date_range) }}">Last</a>
                                            </li>
                                            @endif

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection
