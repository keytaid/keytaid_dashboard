@extends('admin/admin')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>

                <div class="card-tools">

                </div>
            </div>
            <div class="card-body">
                @if (Session::has('message'))
                <div id="alert-msg" class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}
                </div>
                @endif

                <div class="row">
                    <!-- <div class="col-md-1"></div> -->
                    <div class="col-md-6">
                        <div class="text-left">
                            <img src="{{$expeditions['markup'][0]['expedition']['logo_url']}}" alt="">
                        </div>
                    </div>
                </div>
                <br>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>Min Cost</th>
                            <th>Max Cost</th>
                            <th>Value</th>
                            <th>Edit Markup</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                                $no = 1 ;
                            ?>

                        @foreach($expeditions['markup'] as $expedition)
                        <tr>

                            <td class="text-center">{{ $no++ }}</td>
                            <td class="text-center">@currency($expedition['min_cost'])</td>
                            <td class="text-center">@currency($expedition['max_cost'])</td>
                            <td class="text-center">@currency($expedition['value'])</td>
                            <td class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/markup/detail/price/'.$expedition['id']) }}" class="btn btn-warning">Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
