@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div class="text-left">
                            <img src="{{$expeditions['markup']['expedition']['logo_url']}}" alt="">
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12">
                        <form method="POST" action="{{ URL::to('admin/'.$user_id.'/markup/detail/price/'.$expeditions['markup']['id'].'/edit') }}" enctype="multipart/form-data"> 
                        @method('PUT')
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('min_cost', 'Minimum cost') }}
                                                <input type="text" class="form-control" placeholder="min_cost" name="min_cost" value="{{$expeditions['markup']['min_cost']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('max_cost', 'Minimum cost') }}
                                                <input type="text" class="form-control" placeholder="max_cost" name="max_cost" value="{{$expeditions['markup']['max_cost']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('value', 'Value') }}
                                                <input type="text" class="form-control" placeholder="value" name="value" value="{{$expeditions['markup']['value']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">
                                                Update markup detail
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
