@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/keytauser/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/keytauser/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>User ID</th>
                                    <th>Shop Id</th>
                                    <th>Tanggal Registrasi</th>
                                    <th>Nomor HP</th>
                                    <th>Nama</th>
                                    <th>Nama Toko</th>
                                    <th>Alamat Toko</th>
                                    <th>Referrer</th>
                                    <th>Community</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($users['current_page']-1)*$users['limit']);
                                    // echo $users
                                ?>

                                @foreach($users['results'] as $user)
                                <tr>
                                
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $user['id'] }}</td>
                                        <td class="text-center">{{ $user['shop_id'] }}</td>
                                        <td class="text-center">{{ $user['created_at'] }}</td>
                                        <td class="text-center">{{ $user['phone_with_code'] }}</td>
                                        <td class="text-center">{{ $user['name'] }}</td>
                                        @if(isset($user['shop']))
                                            <td class="text-center">{{ $user['shop']['name'] }}</td>
                                            <td class="text-center">{{ $user['shop']['address'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset($user['referrer']))
                                            <td class="text-center">{{ $user['referrer'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset( $user['shop']['community']))
                                            <td class="text-center">{{ $user['shop']['community']['name'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $users['current_page'] + 2; 
                                    $start_page = $users['current_page'] - 2;
                                ?>
                                
                                @if ($users['current_page'] > 1)
                                    <?php $before_page = $users['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $users['total_pages'])
                                        <li class="page-item @if($users['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($users['current_page'] < $users['total_pages'])
                                    <?php $next_page = $users['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/search/'.$users['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection