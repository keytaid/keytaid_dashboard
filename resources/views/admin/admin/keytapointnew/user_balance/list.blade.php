@extends('admin/admin')
@section('content')
    <div class="modal fade" id="exportModala" tabindex="-1" role="dialog" aria-labelledby="exportModalLabela" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exportModalLabel">User Balance Export</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/keytasaldo/user-balance/export') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <!-- <div class="col-md-1"></div> -->
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    {{ Form::label('shop_id_range', 'Shop Id range') }}
                                    <div class="form-group"> 
                                        <input type="text" class="form-control" placeholder="1-100" name="shop_id_range" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="md-form active-cyan active-cyan-2 mb-3">
                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var path = "{{ route('autocomplete') }}";

        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    </script>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif

                @if($access_level['role_id']==2)
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Active Keyta point</span>
                                <span class="info-box-number">{{ number_format($points['total'], 0, ',', '.') }}</span>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-2 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </button>
                            {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a> --}}
                        </div>
                        <div class="col-6">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search Shop ID and Shop Name" aria-label="Search" name="keyword" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <br>
                    <br>

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Point ID</th>
                                <th>Shop Id</th>
                                <th>Shop Name</th>
                                <th>Last Action</th>
                                <th>Amount</th>
                                <th>Balance</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1 + (($points['current_page']-1)*$points['limit']);
                                // echo $points
                            ?>

                            @foreach($points['results'] as $point)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ $point['_id'] }}</td>
                                <td class="text-center">{{ $point['shop_id'] }}</td>
                                <td class="text-center">{{ $point['shop_name'] }}</td>
                                @if(isset($point["histories"][0]))
                                    <td class="text-center">{{ $point['histories'][0]['payment_type'] }}</td>
                                    <td class="text-center">{{ number_format($point['histories'][0]['amount'], 0, ',', '.') }}</td>
                                @else
                                    <td  class="text-center">-</td>
                                    <td  class="text-center">-</td>
                                @endif
                                <td class="text-center">{{ number_format($point['balance'], 0, ',', '.') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $points['current_page'] + 2; 
                                    $start_page = $points['current_page'] - 2;
                                ?>
                                
                                @if ($points['current_page'] > 1)
                                    <?php $before_page = $points['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/1') }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/'.$before_page) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $points['total_pages'])
                                        <li class="page-item @if($points['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/'.$i) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($points['current_page'] < $points['total_pages'])
                                    <?php $next_page = $points['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/'.$next_page) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapointnew/user-balance/'.$points['total_pages']) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection