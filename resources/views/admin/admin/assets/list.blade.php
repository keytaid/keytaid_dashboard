@extends('admin/admin')
@section('content')
    <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="uploadImageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uploadImageLabel">Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('assets/'.$user_id.'/upload') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <div class="form-group">
                                        {{ Form::label('image', 'image') }}
                                        {{ Form::file('image', ['class'=>'form-control']) }}  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    {{ Form::label('image_name', 'Image Name') }}
                                    <div class="form-group"> 
                                        <input type="text" class="form-control" placeholder="Image name" name="image_name" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="md-form active-cyan active-cyan-2 mb-3">
                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">
                                <i class="spinner fa fa-spinner fa-spin"></i>
                                Upload
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        <button class="btn btn-primary fa fa-plus" data-toggle="modal" data-target="#uploadImage" ></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-6">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/assets/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="{{ $search }}" aria-label="Search" name="search" value="">
                                </div>
                            </form>
                        </div>
                    </div>

                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    {{-- <div class="scrolling-wrapper"> --}}
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Preview</th>
                                    <th>Name</th>
                                    <th>Link</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                ?>

                                @foreach($assets['url'] as $asset)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">
                                            <img width='250px' object-fit='contain' src="{{ $asset['url'] }}" alt="{{ $no }}"></img>
                                        </td>
                                        <td class="text-center">{{ $asset['name'] }}</td>
                                        <td class="text-center"><a href="{{ $asset['url'] }}" download="{{ $asset['name'] }}">{{ $asset['url'] }}</a></td>
                                        <td class="text-center">
                                            <a href="{{URL::to('/admin/'.$user_id.'/assets/'.substr($asset['object_key'], 11).'/delete')}}">
                                                <i class="fa fa-trash nav-icon fa-2x"></i>
                                            </a>
                                       </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    {{-- </div> --}}
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $assets['current_page'] + 2; 
                                    $start_page = $assets['current_page'] - 2;
                                ?>

                                @if ($assets['current_page'] > 1)
                                <?php $before_page = $assets['current_page'] - 1 ?>
                                <li class="page-item"><a class="page-link"
                                        href="{{ URL::to('/admin/'.$user_id.'/assets/search/1?search='.$search) }}">First</a>
                                </li>
                                <li class="page-item"><a class="page-link"
                                        href="{{ URL::to('/admin/'.$user_id.'/assets/search/'.$before_page.'?search='.$search) }}">Previous</a>
                                </li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++) @if ($i> 0 and $i <=
                                        $assets['total_pages']) <li
                                        class="page-item @if($assets['current_page'] == $i) active @endif"><a
                                            class="page-link"
                                            href="{{ URL::to('/admin/'.$user_id.'/assets/search/'.$i.'?search='.$search) }}">{{$i}}</a>
                                        </li>
                                        @endif
                                        @endfor
                                        @if ($assets['current_page'] < $assets['total_pages'])
                                            <?php $next_page = $assets['current_page'] + 1 ?> <li
                                            class="page-item"><a class="page-link"
                                                href="{{ URL::to('/admin/'.$user_id.'/assets/search/'.$next_page.'?search='.$search) }}">Next</a>
                                            </li>
                                            <li class="page-item"><a class="page-link"
                                                    href="{{ URL::to('/admin/'.$user_id.'/assets/search/'.$assets['total_pages'].'?search='.$search) }}">Last</a>
                                            </li>
                                            @endif

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
            $('.spinner').show();
        })
    })
</script>
@endsection