@extends('admin/admin')
@section('content')
    <div class="container-fluid">
        <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exportModalLabel">Orders Export</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/orders/exportnew') }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                <!-- <div class="col-md-1"></div> -->
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label>Date Range</label>
                
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label>Whatsapp number</label>
                
                                        <div class="input-group">
                                            <input type="text" class="form-control float-right" name="whatsapp" id="reservation">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        <a class="btn btn-warning" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/1') }}"><i class="fa" aria-hidden="true">   Order Difference</i></a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/orders/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>                                  
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                            <input type="hidden" name="expedition_name" value="{{$expedition_name}}">
                                            <input type="hidden" name="expedition_name" value="{{$expedition_name}}">
                                            <input type="hidden" name="voucher_title" value="{{$voucher_title}}">
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                
                    @if($access_level['role_id']==1)
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Creation</span>
                                    <span class="info-box-number">{{ number_format($orders['total'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Orders Creation</span>
                                    <span class="info-box-number">{{ number_format($orders['total_users'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Creation</span>
                                    <span class="info-box-number">{{ number_format($orders['total'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Orders Creation</span>
                                    <span class="info-box-number">{{ number_format($orders['total_users'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gross Revenue</span>
                                    <span class="info-box-number">{{"Rp. ".number_format($orders['order_value'], 0, ',', '.') }}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <!-- <div class="col-1 text-center">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </button>
                                {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a> --}}
                            </div> -->
                            <div class="col-1 text-center">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </button>
                                {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a> --}}
                            </div>
                            <div class="col-4">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/orders/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="expedition_name" value="{{$expedition_name}}">
                                    <input type="hidden" name="voucher_title" value="{{$voucher_title}}">
                                    <input type="hidden" name="payment_method" value="{{$payment_method}}">
                                    <input type="hidden" name="date_range" value="{{$date_range}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search by shop name or shop id" aria-label="Search" name="keyword" value="">
                                    </div>
                                </form>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($expedition_name == null)
                                        Select Expedition
                                    @else
                                        {{$expedition_name}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=grab_express&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Grab Express</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=gojek&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Go-Send</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=anteraja&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Anter Aja</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=paxel&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Paxel</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=sicepat&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Sicepat</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jne&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">JNE</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jnt&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">J&T</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=pos&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">POS Indonesia</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=deliveree&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Deliveree</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=ninja&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Ninja Xpress</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=tiki&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Tiki</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=wahana&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Wahana</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=lion&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Lion Parcel</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=mrspeedy&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">Mr Speedy</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=rpx&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">RPX</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name=jet&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range.'&keyword='.$keyword) }}">JET</a>
                                </div>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($payment_method == null)
                                        Select Payment
                                    @else
                                        {{$payment_method}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=OVO&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">OVO</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=DANA&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">DANA</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=LINKAJA&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">LINKAJA</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=GOPAY&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">GOPAY</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method=SHOPEEPAY&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">SHOPEEPAY</a>   
                                </div>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($voucher_title == null)
                                        Select Voucher
                                    @else
                                        {{$voucher_title}}
                                    @endif
                                </button>
                                <div class="dropdown-menu scrollabe" aria-labelledby="dropdownMenuButton">
                                    @foreach ($voucher_lists as $voucher)
                                        <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher['field'].'&date_range='.$date_range) }}">{{$voucher['field']}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Action</th>
                                        <th>Lacak</th>
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Expedition</th>
                                        <th>Status</th>
                                        <th>Cancellation_reason</th>
                                        <th>Shipping Price</th>
                                        <th>KP</th>
                                        <th>KS</th>
                                        <th>Ewallet</th>
                                        <th>Voucher</th>
                                        <th>Voucher Title</th>
                                        <th>Payment Method</th>
                                        <th>Expedition External Id</th>
                                        <th>Layanan</th>
                                        <th>Nomor Resi</th>
                                        <th>Berat</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Kode Invoice</th>
                                        <th>Payment Ref Wallet</th>
                                        <th>Payment Ref Keytapoint</th>
                                        <th>Order Id</th>
                                        <th>Payment Id</th>
                                        <th>Shop Id</th>
                                        <th>Tanggal Invoice</th>
                                        <th>Description</th>
                                    @elseif($access_level['role_id']==1)
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Kode Invoice</th>
                                        <th>Expedition</th>
                                        <th>Layanan</th>
                                        <th>Ongkos Kirim</th>
                                        <th>Status</th>
                                        
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                                $GLOBAL['order_id'] = 0;
                                $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>

                                @foreach($orders['results'] as $payment)
                                        <tr>
                                                <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $no++ }}</td>
                                                <td style="vertical-align : middle;text-align:center;" class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/orders/detail/'.$payment['id']) }}" class="btn btn-info">Detail</a></td>
                                                
                                                @if($payment['status'] == 'failed')
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center"> - </td>
                                                @else
                                                    @if(isset($payment['tracking_url']))
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center"><a href="{{ $payment['tracking_url'] }}" class="btn btn-info">Lacak</a></td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/orders/tracking/'.$payment['id']) }}" class="btn btn-info">Lacak</a></td>
                                                    @endif
                                                @endif

                                                @if($access_level['role_id']==2)
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ str_replace("T"," ",substr($payment['created_at'], 0, 19))}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['trx']['shop']['name'] }}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['expedition']['name'] }}</td>

                                                    <td style="vertical-align : middle;text-align:center;" class="text-center
                                                        @if($payment['status'] == 'COMPLETED' or $payment['status'] == 'delivered' or $payment['status'] == 'completed')
                                                            {{'success'}}
                                                        @elseif($payment['status'] == 'FAILED' or $payment['status'] == 'cancelled' or $payment['status'] == 'cancelled_by_user' or $payment['status'] == 'failed' or $payment['status'] == 'courier_not_found')   
                                                            {{'error'}}
                                                        @else
                                                            {{'info'}}
                                                        @endif
                                                        ">{{ $payment['status']}}
                                                    </td>
                                                
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['cancellation_reason']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($payment['shipment_price'], 0, ',', '.') }}</td>                                             

                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                @if( $payment_detail['payment_method'] == "KEYTAPOINT")
                                                                    <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($payment['keytapoint_payment'], 0, ',', '.') }}</td>
                                                    @endif

                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                @if(strtoupper($payment_detail['payment_method']) == "KEYTASALDO")
                                                                    <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                                    @endif

                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                @if( $payment_detail['payment_method'] != "KEYTAPOINT" and strtoupper($payment_detail['payment_method']) != "KEYTASALDO")
                                                                    <li>{{ number_format($payment_detail['amount'], 0, ',', '.') }}</li>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($payment['ewallet_payment'], 0, ',', '.') }}</td>
                                                    @endif

                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ number_format($payment['voucher_value'], 0, ',', '.') }}</td>
                                                    @if(isset($payment['voucher']))
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['voucher']['voucher_title']}}</td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">-</td>
                                                    @endif
                                                    
                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                <li>{{ strtoupper($payment_detail['payment_method']) }}</li>
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['payment']['ewallet_type'] }}</td>
                                                    @endif
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['expedition_external_number']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['expedition_service_type']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['resi_number']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['weight']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['sender_name']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['receipent_name']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['trx']['number']}}</td>
                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                @if( $payment_detail['payment_method'] != "KEYTAPOINT")
                                                                    <li>{{ $payment_detail['external_id'] }}</li>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['payment']['external_id'] }}</td>
                                                    @endif

                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                @if( $payment_detail['payment_method'] == "KEYTAPOINT")
                                                                    <li>{{ $payment_detail['external_id'] }}</li>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['payment']['external_id'] }}</td>
                                                    @endif
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['id']}}</td>

                                                    @if(count($payment['payments']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">
                                                            @foreach($payment['payments'] as $payment_detail)
                                                                <li>{{ $payment_detail['id'] }}</li>
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['payment']['id'] }}</td>
                                                    @endif
                                                    
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['trx']['shop_id']}}</td>
                                                    <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['trx']['created_at']}}</td>
                                                    @if(count($payment['delivery_histories']) >= 1)
                                                        <td style="vertical-align : middle;text-align:center;" class="text-center">{{ $payment['delivery_histories'][count($payment['delivery_histories']) - 1]['description']}}</td>
                                                    @endif
                                                @endif
                                        </tr>
                                        
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $orders['current_page'] + 2; 
                                    $start_page = $orders['current_page'] - 2;
                                ?>
                                @if($access_level['role_id']==2)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/1?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$before_page.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$i.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$next_page.'?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/'.$orders['total_pages'].'?expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&date_range='.$date_range) }}">Last</a></li>
                                    @endif
                                @elseif($access_level['role_id']==1)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$i) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/orders/'.$orders['total_pages']) }}">Last</a></li>
                                    @endif
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <form method="POST" action="{{ URL::to('/admin/'.$user_id.'/orders/cancel/'.$GLOBAL['order_id']) }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <button type="submit" class="btn btn-danger">Cancel Order</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection