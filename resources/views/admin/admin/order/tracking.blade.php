@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detail order: {{ $order['order']['id'] }}</h3>

                    <div class="card-tools">
                        <p class="btn btn-warning  
                        @if($order['order']['status'] == 'COMPLETED' or $order['order']['status'] == 'delivered' or $order['order']['status'] == 'completed')
                            {{'btn btn-success'}}
                        @elseif($order['order']['status'] == 'FAILED' or $order['order']['status'] == 'cancelled' or $order['order']['status'] == 'cancelled_by_user' or $order['order']['status'] == 'failed' or $order['order']['status'] == 'courier_not_found')   
                            {{'btn btn-danger'}}
                        @else
                            {{'btn btn-warning'}}
                        @endif" 
                        href="{{ URL::to('/admin/'.$user_id.'/orders/difference/1') }}"><i class="fa" aria-hidden="true">{{ $order['order']['status'] }}</i></p>
                    </div>
                </div>

                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                            <ul class="list-group">
                                <li class="list-group-item active">Lacak Paket</li>
                                @foreach($order['history'] as $histories)
                                    <li class="list-group-item">
                                        <div class="col">
                                            <div class="row">
                                                <p class="card-title">{{ str_replace("T"," ",substr($histories['timestamp'], 0, 19)) }}</p>
                                            </div>
                                            <div class="row">
                                                <p class="card-text">{{ $histories['note'] }}</p>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card">
                                <h5 class="card-header">ORDER REF: {{ $order['order']['expedition_external_number'] }}</h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm">
                                            <h5 class="card-text">Resi pengiriman:</h5>
                                            @if(isset($order['order']['resi_number']))
                                                <h6 class="card-title">{{ $order['order']['resi_number'] }}</h5>
                                            @else
                                                <td class="text-center">-</td>
                                            @endif
                                        </div>
                                    </div>
                                    <br />
                                    <div class="card">
                                        <h5 class="card-header">Alamat Asal</h5>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm">
                                                    <h5 class="card-title">{{ $order['order']['sender_name'] }}</h5>
                                                    <p class="card-text">{{ $order['order']['sender_address'] }}</p>
                                                </div>
                                                <div class="col-sm">
                                                    <div class="row">
                                                        <h5 class="card-title">{{ $order['order']['sender_phone'] }}</h5>
                                                    </div>
                                                    <div class="row">
                                                        <p class="card-text">Latitude: {{ $order['order']['sender_latitude'] }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="card-text">Longitude: {{ $order['order']['sender_longitude'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <h5 class="card-header">Alamat Tujuan</h5>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm">
                                                    <h5 class="card-title">{{ $order['order']['receipent_name'] }}</h5>
                                                    <p class="card-text">{{ $order['order']['destination_address'] }}</p>
                                                </div>
                                                <div class="col-sm">
                                                    <div class="row">
                                                        <h5 class="card-title">{{ $order['order']['receipent_phone'] }}</h5>
                                                    </div>
                                                    <div class="row">
                                                        <p class="card-text">Latitude: {{ $order['order']['destination_latitude'] }}</p>
                                                    </div>
                                                    <div class="row">
                                                        <p class="card-text">Longitude: {{ $order['order']['destination_longitude'] }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection