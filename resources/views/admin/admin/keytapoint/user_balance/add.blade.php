@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">


  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('admin/benefit/'.$user_id.'/store') }}" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="">
            <h3>Edit Slider</h2>
          </div>
          <br>
          <div class="form-group has-feedback">
            {{ Form::label('icon', 'Icon') }}
            <div class="form-group"> 
              <select name="icon" class="form-control">
                <option value="fa-car"><i class="fa fa-user nav-icon"></i> Car</option>
                <option value="fa-bell"><i class="fa fa-user nav-icon"></i> Bell</option>
                <option value="fa-bolt"><i class="fa fa-user nav-icon"></i> Bolt</option>
                <option value="fa-cloud"><i class="fa fa-user nav-icon"></i> Cloud</option>
                <option value="fa-code-fork"><i class="fa fa-user nav-icon"></i> Code Fork</option>
                <option value="fa-crosshairs"><i class="fa fa-user nav-icon"></i> Crosshair</option>
                <option value="fa-comments-o"><i class="fa fa-user nav-icon"></i> Comments-o</option>
                <option value="fa-industry"><i class="fa fa-user nav-icon"></i> Industry</option>
                <option value="fa-microchip"><i class="fa fa-user nav-icon"></i> Microchip</option>
                <option value="fa-recycle"><i class="fa fa-user nav-icon"></i> Recycle</option>
                <option value="fa-camera-retro"><i class="fa fa-user nav-icon"></i> Camera Retro</option>
              </select>
            </div>
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('subject', 'Judul') }}
            <input type="text" class="form-control" placeholder="Subject" name="subject" value="">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('description', 'Deskripsi') }}
            <textarea class="col-md-12" name="description" id="" rows="3"  placeholder="Deskripsi"></textarea>
          </div>

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Tambah Benefit</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection