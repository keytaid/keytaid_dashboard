@extends('admin/admin')
@section('content')
<div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exportModalLabel">Orders Export</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-prevent-multiple-submits" method="GET" action="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/export') }}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <!-- <div class="col-md-1"></div> -->
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback">
                                    <label>Date Range</label>
            
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control float-right" name="date_range" id="reservation">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="md-form active-cyan active-cyan-2 mb-3">
                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Keyta Balance</span>
                                <span class="info-box-number">{{$points['total_balance']}}</span>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-3 text-center">
                        <span></span>
                    </div>
                    <div class="col-1 text-center">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                            <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                        </button>
                        {{-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exportModal">
                            <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                        </a> --}}
                    </div>
                    <div class="col-4">
                        <form method="GET"
                            action="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/1') }}"
                            enctype="multipart/form-data">
                            
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <input class="form-control" type="text" placeholder="Search by shop id"
                                    aria-label="Search" name="keyword" value="">
                            </div>
                        </form>
                    </div>
                    </div>
                                        
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                    <th>Shop Id</th>
                                    <th>Shop Name</th>
                                    <th>Transaction Number</th>
                                    <th>Transaction Type</th>
                                    <th>Reference</th>
                                    <th>Amount</th>
                                    <th>Balance</th>    
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($points['current_page']-1)*$points['limit']);
                                    // echo $points
                                ?>

                                @foreach($points['results'] as $point)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">Completed</td>
                                    <td class="text-center">{{ $point['created_at'] }}</td>
                                    <td class="text-center">{{ $point['shop']['id'] }}</td>
                                    <td class="text-center">{{ $point['shop']['name'] }}</td>
                                    <td class="text-center">{{ $point['number'] }}</td>
                                    <td class="text-center">{{ $point['payment_type'] }}</td>
                                    <td class="text-center">{{ $point['id'] }}</td>
                                    <td class="text-center">{{ $point['amount'] }}</td>
                                    <td class="text-center">{{ $point['keyta_balance'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $points['current_page'] + 2; 
                                    $start_page = $points['current_page'] - 2;
                                ?>
                                
                                @if ($points['current_page'] > 1)
                                    <?php $before_page = $points['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/1') }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/'.$before_page) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $points['total_pages'])
                                        <li class="page-item @if($points['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/'.$i) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($points['current_page'] < $points['total_pages'])
                                    <?php $next_page = $points['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/'.$next_page) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/'.$points['total_pages']) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection