@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User List</h3>

                    <!-- <div class="card-tools">
                        <a href="{//{URL::to('admin/category/create')}}" class="btn btn-toolbar">
                            <i class="fa fa-plus"></i> &nbsp; Add
                        </a>
                    </div> -->
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Akses Level</th>
                                <th>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1 ?>
                            @foreach($users as $user)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ $user['id'] }}</td>
                                <td class="text-center">{{ ucfirst($user['name']) }}</td>
                                <td class="text-center">{{ ucfirst($user['email']) }}</td>
                                <td class="text-center">{{ $user['role_id'] }}</td>
                                <td class="text-center">
                                    <form method="POST" action="{//{ URL::to('/admin/category/'.$category['id']) }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="{//{ URL::to('/admin/category/'.$category['id']) }}"><i class="fa fa-eye"></i></a>
                                            <a class="btn btn-success" href="{//{ URL::to('/admin/category/'.$category['id'].'/edit') }}"><i class="fa fa-pencil"></i></a>
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection