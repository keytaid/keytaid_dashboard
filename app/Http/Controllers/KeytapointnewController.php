<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Keytapointnew;
use App\Exports\UsersExport;
use App\Exports\UsersBalanceExport;
use App\Exports\KeytaPointHistoryExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class KeytapointnewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:user');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request, $id, $page)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $keyword = $request->post('keyword'); 
        if (!$keyword){
            $keyword = null;
        }
        $points = Keytapointnew::pointsList($token, $page, $keyword);
        $data=[
            'title' => 'Keyta Point',
            'user_id'=> $id,
            'access_level' => $access_level,
            'points' => $points
        ];
    	return view('admin/admin/keytapointnew/user_balance/list')->with($data);
    }

    public function history(Request $request, $id, $page)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        // $keyword = $request->post('keyword'); 
        $keyword = null;
        
        $histories = Keytapointnew::pointHistory($token, $page, $keyword);
        $data=[
            'title' => 'History Point',
            'user_id'=> $id,
            'access_level' => $access_level,
            'points' => $histories
        ];
    	return view('admin/admin/keytapointnew/history/list')->with($data);
    }

    public function history_search(Request $request, $id, $page)
	{
        $keyword = $request->get('keyword');
        
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $histories = Keytapoint::pointHistorySearch($token, $page, $keyword);
        $data=[
            'title' => 'Keyta Point History',
            'user_id'=> $id,
            'access_level' => $access_level,
            'points' => $histories,
            'keyword' => $keyword
        ];
    	return view('admin/admin/keytapointnew/history/list_search')->with($data);
    }

    public function history_export(Request $request, $id)
    {
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }

        return Excel::download(new KeytaPointHistoryExport($date_range), 'KeytaPointHistory.xlsx');
    }

    public function pointTopup(Request $request, $id)
    {
        $shop_id = $request->post('shop_id');
        $value = $request->post('topup_value');

        $token = Helper::getToken();
        $checksum = Keytapoint::checksum($token, $shop_id);
        $shops = Keytapoint::kpTopup($token, $shop_id, $value, $checksum);

        return redirect('/admin/'.$id.'/keytapoint/user-balance/1')->with(['message' => 'Topup Berhasil']);
    }

    public function pointReward(Request $request, $id)
    {
        $shop_id = $request->post('shop_id');
        $value = $request->post('topup_value');
        $description = $request->post('reward_description');

        $token = Helper::getToken();
        $checksum = Keytapoint::checksum($token, $shop_id);
        $shops = Keytapoint::kpReward($token, $shop_id, $value, $checksum, $description);

        return redirect('/admin/'.$id.'/keytapoint/user-balance/1')->with(['message' => 'Reward Berhasil']);
    }

    public function pointReduce(Request $request, $id)
    {
        $shop_id = $request->post('shop_id');
        $value = $request->post('reduce_value');

        $token = Helper::getToken();
        $checksum = Keytapoint::checksum($token, $shop_id);
        $shops = Keytapoint::kpReduce($token, $shop_id, $value, $checksum);

        return redirect('/admin/'.$id.'/keytapoint/user-balance/1')->with(['message' => 'Reduce Berhasil']);
    }
}

