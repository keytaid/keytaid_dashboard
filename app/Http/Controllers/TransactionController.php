<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Transaction;
use App\Exports\TransactionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page) 
	{
        $channel = $request->get('channel');
        $expedition_name = $request->get('expedition_name');
        $payment_method = $request->get('payment_method');
        $status = $request->get('status');
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $token = Helper::getToken(); 
        $access_level = \App\User::find($id);
        $transactions = Transaction::transactionsList($token, $page, $channel, $expedition_name, $payment_method, $status, $date_range);
        // dd($transactions);
        $data=[
            'title' => 'Invoices',
            'user_id'=> $id,
            'access_level' => $access_level,
            'transactions' => $transactions,
            'channel' => $channel,
            'expedition_name' => $expedition_name,
            'payment_method' => $payment_method,
            'status' => $status,
            'date_range' => $date_range
        ];
    	return view('admin/admin/transaction/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $channel = $request->get('channel');
        $expedition_name = $request->get('expedition_name');
        $payment_method = $request->get('payment_method');
        $status = $request->get('status');
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $transactions = Transaction::transactionsSearch($token, $keyword, $page);
        $data=[
            'title' => 'Invoices',
            'user_id'=> $id,
            'access_level' => $access_level,
            'transactions' => $transactions,
            'keyword' => $keyword,
            'channel' => $channel,
            'expedition_name' => $expedition_name,
            'payment_method' => $payment_method,
            'status' => $status
        ];
    	return view('admin/admin/transaction/list_search')->with($data);
    }

    public function export(Request $request, $id)
    { 
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $whatsapp = $request->post('whatsapp');
        $transactions = Transaction::requestDownload($date_range, $whatsapp);

        // echo $date_range;
        return back()->with(['message' => 'Download request sent']);
    }
}
