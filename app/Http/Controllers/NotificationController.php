<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Notification;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id, $page)
	{
        $community_name = $request->get('community_name');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $notifications = Notification::index($token, $page);
        $data=[
            'title' => 'Notification',
            'user_id'=> $id,
            'access_level' => $access_level,
            'notifications' => $notifications,
        ];

    	return view('admin/admin/notification/list')->with($data);
    }

    public function detail(Request $request, $id, $notif_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $notifications = Notification::detail($token, $notif_id);
        $data=[
            'title' => 'Notification',
            'user_id'=> $id,
            'access_level' => $access_level,
            'notifications' => $notifications,
        ];

    	return view('admin/admin/notification/edit')->with($data);
    }

    public function update(Request $request, $id, $notif_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $topic = $request->post('topic');
        $title = $request->post('title');
        $event_name = $request->post('event_name');
        $description = $request->post('description');
        $show_time = $request->post('show_time');
        $show_day = $request->post('show_day');
        $start_date = $request->post('start_date');
        $end_date = $request->post('end_date');
        $notification_type = $request->post('notification_type');
        $image_url = $request->post('image_url');
        $redirect_url = $request->post('redirect_url');

        $data = [
            'topic' => $topic,
            'title' => $title,
            'event_name' => $event_name,
            'description' => $description,
            'show_time' => $show_time,
            'show_day' => $show_day,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'notification_type' => $notification_type,
            'image_url' => $image_url,
            'redirect_url' => $redirect_url,  
        ];

        $notifications = Notification::update($token, $notif_id, $data);
        $data=[
            'title' => 'Notification',
            'user_id'=> $id,
            'access_level' => $access_level,
            'notifications' => $notifications,
        ];

    	return redirect()->route('notification', ['id' => $id, 'page' => 1])->with(['message' => 'Update notification success']);
    }

    public function createPage(Request $request, $id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $data=[
            'title' => 'Notification',
            'user_id'=> $id,
            'access_level' => $access_level,
        ];

    	return view('admin/admin/notification/add')->with($data);
    }

    public function create(Request $request, $id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $topic = $request->post('topic');
        $title = $request->post('title');
        $event_name = $request->post('event_name');
        $description = $request->post('description');
        $show_time = $request->post('show_time');
        $show_day = $request->post('show_day');
        $start_date = $request->post('start_date');
        $end_date = $request->post('end_date');
        $notification_type = $request->post('notification_type');
        $image_url = $request->file('image_url');
        $imageContent = $image_url ? file_get_contents($image_url) : null;
        $redirect_url = $request->post('redirect_url');

        $data = [
            [
                'name' => 'topic',
                'contents' => $topic
            ],
            [
                'name' => 'title',
                'contents' => $title
            ],
            [
                'name' => 'event_name',
                'contents' => $event_name
            ],
            [
                'name' => 'description',
                'contents' => $description
            ],
            [
                'name' => 'show_time',
                'contents' => $show_time
            ],
            [
                'name' => 'show_day',
                'contents' => $show_day
            ],
            [
                'name' => 'start_date',
                'contents' => $start_date
            ],
            [
                'name' => 'end_date',
                'contents' => $end_date
            ],
            [
                'name' => 'notification_type',
                'contents' => $notification_type
            ],
            [
                'name' => 'redirect_url',
                'contents' => $redirect_url
            ],
        ];
        
        if ($imageContent) {
            array_push($data, [
                'name'     => 'image',
                'contents' => $imageContent,
                'filename' => 'image'
            ]);
        }

        $notifications = Notification::create($token, $data);
        $data=[
            'title' => 'Notification',
            'user_id'=> $id,
            'access_level' => $access_level,
            'notifications' => $notifications,
        ];

    	return redirect()->route('notification', ['id' => $id, 'page' => 1])->with(['message' => 'Create notification success']);
    }
    
}
