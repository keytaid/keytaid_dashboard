<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Order;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Voucher;
use App\Exports\OrderExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $expedition_name = $request->get('expedition_name');
        $payment_method = $request->get('payment_method');
        $voucher_title = $request->get('voucher_title');
        $date_range= $request->get('date_range');
        $keyword= $request->get('keyword');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $orders = Order::ordersList($token, $page, $expedition_name, $payment_method, $voucher_title, $date_range, $keyword);
        $vouchers = Voucher::voucherTitleList($token);
        $data=[
            'title' => 'Orders Courier',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
            'expedition_name' => $expedition_name,
            'payment_method' => $payment_method,
            'voucher_title' => $voucher_title,
            'voucher_lists' => $vouchers['voucher'],
            'date_range' => $date_range,
            'keyword' => $keyword
        ];
    	return view('admin/admin/order/list')->with($data);
    }

    public function get_order_detail(Request $request,$id, $order_id)
	{
       
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $order = Order::orderDetails($token, $order_id);
        $vouchers = Voucher::voucherTitleLists($token);
        // dd($order);
        $data=[
            'title' => "Orders Details",
            'user_id'=> $id,
            'access_level' => $access_level,
            'order' => $order,
        ];
    	return view('admin/admin/order/detail')->with($data);
    }

    public function tracking_order(Request $request,$id, $order_id)
	{
       
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $order = Order::tracking_order($token, $order_id);
        // dd($order);
        $data=[
            'title' => "Lacak Order",
            'user_id'=> $id,
            'access_level' => $access_level,
            'order' => $order,
        ];
    	return view('admin/admin/order/tracking')->with($data);
    }

    public function canvaser_user_order_list(Request $request,$id, $page)
	{
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $orders = Canvaser::ordersList($token, $page, $access_level["username"], $access_level["shop_id"]);
        $data=[
            'title' => 'Orders Courier',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders
        ];
    	return view('admin/admin/order/list')->with($data); 
    }
    
    public function search(Request $request, $id, $page)
    {
        $expedition_name = $request->get('expedition_name');
        $payment_method = $request->get('payment_method');
        $keyword = $request->get('keyword');
        $voucher_title = $request->get('voucher_title');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $vouchers = Voucher::voucherTitleLists($token);
        $orders = Order::ordersSearch($token, $keyword, $page, $expedition_name, $payment_method, $voucher_title);
        $data=[
            'title' => 'Orders Courier',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
            'keyword' => $keyword,
            'voucher_title' => $voucher_title,
            'voucher_lists' => $vouchers,
            'expedition_name' => $expedition_name,
            'payment_method' => $payment_method
        ];
    	return view('admin/admin/order/list_search')->with($data);
    }

    public function export(Request $request, $id)
    {
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }

        return Excel::download(new OrderExport($date_range), 'Orders.xlsx');
    }

    public function cancelOrder($id, $order_id)
    {
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $orders = Order::ordersCancel($token, $order_id);
        $data=[
            'title' => 'Orders Courier',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders
        ];
    	return redirect("/admin/$id/orders/1");
    }

    public function indexDifference(Request $request,$id, $page)
	{
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $orders = Order::ordersListDifference($token, $page);
        $data=[
            'title' => 'Orders Difference',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
        ];
    	return view('admin/admin/order/list_order_difference')->with($data);
    }

    public function createDifference(Request $request,$id)
	{
        $order_id = $request->get('order_id');
        $difference_amount = $request->get('difference_amount');
        $evidence_url = $request->get('evidence_url');
        $token = Helper::getToken();
        $data = [
            'order_id' => $order_id,
            'difference_amount' => $difference_amount,
            'evidence_url' => $evidence_url,
        ];

        $access_level = \App\User::find($id);
        $orders = Order::createDifference($token, $data);
        $data=[
            'title' => 'Orders Difference',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
        ];
        return redirect()->route('order_difference', ['id' => $id, 'page' => 1])->with(['message' => 'Create new order difference success']);
    }

    public function export_new(Request $request, $id)
    { 
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $whatsapp = $request->post('whatsapp');
        $transactions = Order::requestDownload($date_range, $whatsapp);

        // echo $date_range;
        return back()->with(['message' => 'Download request sent']);
    }
}
