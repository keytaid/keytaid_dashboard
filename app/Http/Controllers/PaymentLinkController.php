<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Payment;
use Facades\App\Helper\Order;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Voucher;
use App\Exports\OrderExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class PaymentLinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $status= $request->get('status');
        $token = Helper::getToken();
        $keyword= $request->get('keyword');
        $access_level = \App\User::find($id);
        $paymentlinks = Payment::paymentlink_list($token, $page, $date_range, $status, $keyword);
        $data=[
            'title' => 'Payment Link',
            'user_id'=> $id,
            'access_level' => $access_level,
            'paymentlinks' => $paymentlinks,
            'date_range' => $date_range,
            'status' => $status,
            'keyword' => $keyword
        ];
    	return view('admin/admin/paymentlink/list')->with($data);
    }

    public function listfee(Request $request,$id )
	{
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $paymentlinks = Payment::listfee($token);
        $data=[
            'title' => 'Payment Link',
            'user_id'=> $id,
            'access_level' => $access_level,
            'paymentlinks' => $paymentlinks
        ];
    	return view('admin/admin/paymentlink/list_fee')->with($data);
    }

    public function resolve(Request $request,$id, $trx_id )
	{
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $paymentlinks = Payment::resolve($token, $trx_id);
        $data=[
            'title' => 'Payment Link',
            'user_id'=> $id,
            'access_level' => $access_level,
            'paymentlinks' => $paymentlinks
        ];
        return redirect('/admin/'.$id.'/paymentlink/1')->with(['message' => 'Resolve berhasil']);

    }
}
