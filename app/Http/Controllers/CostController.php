<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Cost;
use App\Exports\CostsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class CostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $filter = $request->get('filter');
        $date_range = $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $costs = Cost::costsList($token, $page, $date_range);
        $data=[
            'title' => 'Cek Ongkir',
            'user_id'=> $id,
            'access_level' => $access_level,
            'costs' => $costs,
            'filter' => $filter,
            'date_range' => $date_range
        ];
    	return view('admin/admin/cost/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $filter = $request->get('filter');
        $keyword = $request->get('keyword');
        $date_range = $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $costs = Cost::costsSearch($token, $keyword, $page, $date_range);
        $data=[
            'title' => 'Cek Ongkir',
            'user_id'=> $id,
            'access_level' => $access_level,
            'costs' => $costs,
            'keyword' => $keyword,
            'filter' => $filter,
            'date_range' => $date_range
        ];
    	return view('admin/admin/cost/list_search')->with($data);
    }

    public function export()
    {
        return Excel::download(new costsExport(), 'costs.xlsx');
    }
}
