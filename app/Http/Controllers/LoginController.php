<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Hash; 

class LoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest');
	}

	public function getLogin()
	{
		 $data=[
            'title' => 'Login'
        ];
		return view('login/loginForm')->with($data);	
	}

	public function postLogin(Request $request)		
	{
		if(Auth::attempt([
			'username' => $request->inputemailuser,
			'password' => $request->password
		])){
			$id = Auth::user()->id;
			if (auth()->user()->roleCheck('admin')) {
				$request->session()->put('key', 'value');
				return Redirect::to('admin/'.$id);//betul
			}elseif (auth()->user()->roleCheck('user')) {
				$request->session()->put('key', 'value');
				return Redirect::to('canvaser/'.$id.'/keytauser/1');//betul
			}elseif (auth()->user()->roleCheck('superuser')) {
				$request->session()->put('key', 'value');
				return Redirect::to('superuser/'.$id);//betul
			}
			
		}elseif(Auth::attempt([
			'email' => $request->inputemailuser,
			'password' => $request->password
		])){
			$id = Auth::user()->id;
			if (auth()->user()->roleCheck('admin')) {
				return Redirect::to('admin/'.$id);//betul
			}elseif (auth()->user()->roleCheck('user')) {
				return Redirect::to('canvaser/'.$id.'/keytauser/1');//betul
			}elseif (auth()->user()->roleCheck('superuser')) {
				return Redirect::to('superuser/'.$id);//betul
			}
		}
		else{
			return Redirect::to('login');
		
		}
	}

	public function postLogout(Request $request)
	{
		Auth::logout();
		Session::flush();
		return Redirect::to('login');
	}
}
