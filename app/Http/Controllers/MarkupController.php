<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Markup;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class MarkupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:user');
        $this->middleware('role_access:admin');
    }

    public function indexMarkup($id, $page)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $expeditions = Markup::indexMarkup($token, $id);
        $data=[
            'title' => 'Markup Cost',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
        
    	return view('admin/admin/markup/list')->with($data);
    }

    public function detailMarkup($id, $expedition_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $expeditions = Markup::detailMarkup($token, $id, $expedition_id);
        $data=[
            'title' => 'Markup Cost',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
        
    	return view('admin/admin/markup/list2')->with($data);
    }

    public function eachMarkup($id, $markup_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $expeditions = Markup::eachMarkup($token, $id, $markup_id);
        $data=[
            'title' => 'Markup Cost',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
        // dd($expeditions);
        
    	return view('admin/admin/markup/edit')->with($data);
    }

    public function updateMarkup(Request $request, $id, $markup_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $min_cost = $request->post('min_cost');
        $max_cost = $request->post('max_cost');
        $value = $request->post('value');
        $body = [
            'min_cost' => $min_cost,
            'max_cost' => $max_cost,
            'value' => $value
        ];

        $expeditions = Markup::updateMarkup($token, $id, $markup_id, $body);
        $data=[
            'title' => 'Markup Cost',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
        // dd($expeditions);
        
    	return redirect('/admin/'.$id.'/markup/detail/'.$expeditions['markup']['expedition']['id'])->with(['message' => 'Edit Markup Berhasil']);
    }
}
