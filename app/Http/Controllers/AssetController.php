<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Assets;
use Redirect;
use Session;

class AssetController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role_access:admin');
	}

    public function index(Request $request, $id, $page)
	{
        $token = Helper::getToken();
        $search = $request->get('search');
        if (!$search) {
            $search = '';
        }
        $assets = Assets::assetsList($token, $search, $page);
		$access_level = \App\User::find($id);
        $data=[
            'title' => 'Assets gallery',
            'user_id'=> $id,
            'access_level' => $access_level,
            'assets' => $assets,
            'search' => $search,
        ];
    	return view('admin/admin/assets/list')->with($data);
	}

    public function upload(Request $request, $id)
	{
        $token = Helper::getToken();
        $image = $request->file('image');
        $image_name = $request->get('image_name');

        $data = [
            [
                'name' => 'location',
                'contents' => 'assets',
            ],
            [
                'name'     => 'image',
                'contents' => file_get_contents($image),
                'filename' => 'image'
            ],
            [
                'name' => 'image_name',
                'contents'=> $image_name
            ],
        ];
        $blast = Assets::uploadImage($token, $data);

        return redirect()->route('assets_gallery', ['id' => $id, 'page' => 1])->with(['message' => 'Success upload image']);

	}

    public function delete($id, $asset_id)
	{
        $token = Helper::getToken();
        $data = [
            [
                'name' => 'location',
                'contents' => 'assets',
            ],
            [
                'name'     => 'image_key',
                'contents' => $asset_id
            ]
        ];
        $blast = Assets::deleteImage($token, $data);

        return redirect()->route('assets_gallery', ['id' => $id, 'page' => 1])->with(['message' => 'Success delete image']);

	}

}
