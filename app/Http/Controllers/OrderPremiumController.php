<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Premium;
use App\Exports\OrderPremiumExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class OrderPremiumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $keyword = $request->get('keyword');
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $orders = Premium::premiumOrdersList($token, $page, $date_range);
        $data=[
            'title' => 'Orders Premium',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
            'date_range' => $date_range,
            'keyword' => $keyword
        ];
    	return view('admin/admin/order_premium/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $orders = Premium::premiumOrdersSearch($token, $keyword, $page);
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $data=[
            'title' => 'Orders Premium',
            'user_id'=> $id,
            'access_level' => $access_level,
            'orders' => $orders,
            'date_range' => $date_range,
            'keyword' => $keyword
        ];
    	return view('admin/admin/order_premium/list')->with($data);
    }

    public function export(Request $request, $id)
    {
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        return Excel::download(new OrderPremiumExport($date_range), 'OrdersPremium.xlsx');
    }

    public function priceIndex($id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $pricing = Premium::premiumPriceList($token);
        $data=[
            'title' => 'Premium Pricing',
            'user_id'=> $id,
            'access_level' => $access_level,
            'pricings' => $pricing
        ];
    	return view('admin/admin/order_premium/price_list')->with($data);
    }

    public function priceAdd($id)
    {
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Premium Pricing',
            'user_id'=> $id,
            'access_level' => $access_level
        ];
    	return view('admin/admin/order_premium/price_add')->with($data);
    }

    public function priceStore(Request $request, $id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $payload = [
            'premium_feature_id' => $request->post("premium_feature_id"),
            'duration' => $request->post("duration"),
            'duration_type' => $request->post("duration_type"),
            'quantity' => $request->post("quantity"),
            'quantity_type' => $request->post("quantity_type"),
            'price' => $request->post("price"),
            'is_trial' => $request->post("is_trial")
        ];
        $pricing = Premium::premiumPriceStore($token, $payload);
    	return redirect('/admin/'.$id.'/pricing-premium')->with(['message' => 'Tambah Data Berhasil']);
    }

    public function priceEdit($id, $pricing_id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $pricing = Premium::premiumPriceListId($token, $pricing_id);
        $data=[
            'title' => 'Premium Pricing',
            'user_id'=> $id,
            'access_level' => $access_level,
            'pricings' => $pricing['results']
        ];
    	return view('admin/admin/order_premium/price_edit')->with($data);
    }

    public function priceUpdate(Request $request, $id, $pricing_id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $payload = [
            'premium_feature_id' => $request->post("premium_feature_id"),
            'duration' => $request->post("duration"),
            'duration_type' => $request->post("duration_type"),
            'quantity' => $request->post("quantity"),
            'quantity_type' => $request->post("quantity_type"),
            'price' => $request->post("price"),
            'is_trial' => $request->post("is_trial")
        ];
        $pricing = Premium::premiumPriceEdit($token, $pricing_id, $payload);
    	return redirect('/admin/'.$id.'/pricing-premium')->with(['message' => 'Edit Berhasil']);
    }

    public function priceDelete(Request $request, $id, $pricing_id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $pricing = Premium::premiumPriceDelete($token, $pricing_id);
    	return redirect('/admin/'.$id.'/pricing-premium')->with(['message' => 'Data Berhasil Dihapus']);
    }

    public function premiumShopIndex($id, $page)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $premium = Premium::premiumShopIndex($token, $page);
        $data=[
            'title' => 'Shop Premium',
            'user_id'=> $id,
            'access_level' => $access_level,
            'premiums' => $premium
        ];
    	return view('admin/admin/order_premium/shop_list')->with($data);
    }

    public function premiumShopSearch(Request $request, $id, $page)
    {
        $keyword = $request->post('keyword');   
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $premium = Premium::premiumShopSearch($token, $page, $keyword);
        $data=[
            'title' => 'Shop Premium',
            'user_id'=> $id,
            'access_level' => $access_level,
            'premiums' => $premium
        ];
    	return view('admin/admin/order_premium/shop_list')->with($data);
    }

    public function export_new(Request $request, $id)
    { 
        $date_range = $request->post('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $whatsapp = $request->post('whatsapp');
        $transactions = Premium::requestDownload($date_range, $whatsapp);

        // echo $date_range;
        return back()->with(['message' => 'Download request sent']);
    }
}
