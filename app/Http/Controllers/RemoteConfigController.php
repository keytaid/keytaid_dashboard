<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Remoteconfig;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class RemoteConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id) 
	{
        $token = Helper::getToken(); 
        $access_level = \App\User::find($id);
        $remote_config = Remoteconfig::index($token);
        // dd($remote_config);
        $data=[
            'title' => 'Remote Config',
            'user_id'=> $id,
            'access_level' => $access_level,
            'remote_configs' => $remote_config
        ];
    	return view('admin/admin/remoteconfig/list')->with($data);
    }

    public function create(Request $request,$id) 
	{
        $token = Helper::getToken(); 
        $access_level = \App\User::find($id);
        $body = [];
        $name = $request->get('name');
        if(isset($name)){
            $adder = ['name' => $name];
            $body = $body + $adder;
        }
        $condition = $request->get('condition');
        if(isset($condition)){
            $adder = ['condition' => $condition];
            $body = $body + $adder;
        }
        $type = $request->get('type');

        $value = $request->get('value');
        if(isset($value)){
            if ($type == 'JSON') {
                $edited_value = json_decode($value, true);
            }

            if ($type == 'String') {
                $edited_value = $value;
            }

            if ($type == 'Integer') {
                $edited_value = (int)$value;
            }

            if ($type == 'Float') {
                $edited_value = (float)$value;
            }

            $adder = ['value' => $edited_value];
            $body = $body + $adder;
        }
        $remote_config = Remoteconfig::create($token, $body);

        // dd($remote_config);
        $data=[
            'title' => 'Remote Config',
            'user_id'=> $id,
            'access_level' => $access_level,
        ];

        return redirect()->route('remoteconfig', ['id' => $id])->with(['message' => 'success create new config']);
    }

    public function getname(Request $request,$id, $name, $remote_id) 
	{
        $token = Helper::getToken(); 
        $access_level = \App\User::find($id);
        $remote_config = Remoteconfig::getByName($token, $name);

        $data=[
            'title' => 'Update remote config',
            'user_id'=> $id,
            'access_level' => $access_level,
            'remote_config' => $remote_config,
            'remote_id' => (int)$remote_id
        ];
        
    	return view('admin/admin/remoteconfig/edit')->with($data);
    }

    public function update(Request $request,$id, $remote_id) 
	{
        $token = Helper::getToken(); 
        $access_level = \App\User::find($id);
        $body = [];
        $name = $request->get('name');
        if(isset($name)){
            $adder = ['name' => $name];
            $body = $body + $adder;
        }
        $condition = $request->get('condition');
        if(isset($condition)){
            $adder = ['condition' => $condition];
            $body = $body + $adder;
        }
        $type = $request->get('type');

        $value = $request->get('value');
        if(isset($value)){
            if ($type == 'JSON') {
                $edited_value = json_decode($value, true);
            }

            if ($type == 'String') {
                $edited_value = $value;
            }

            if ($type == 'Integer') {
                $edited_value = (int)$value;
            }

            if ($type == 'Float') {
                $edited_value = (float)$value;
            }

            $adder = ['value' => $edited_value];
            $body = $body + $adder;
        }
        // dd($body);
        $remote_config = Remoteconfig::update($token, $body, $remote_id);

        // dd($remote_id);
        $data=[
            'title' => 'Update remote config',
            'user_id'=> $id,
            'access_level' => $access_level,
            'remote_config' => $remote_config,
            'remote_id' => $remote_id
        ];
        return redirect()->route('remoteconfig', ['id' => $id])->with(['message' => 'success update new config id '.$remote_id]);
    }
}
