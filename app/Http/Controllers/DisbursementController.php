<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Payment;
use Facades\App\Helper\Order;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Voucher;
use App\Exports\OrderExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class DisbursementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $disbursement = Payment::disbursement_list($token, $page, $date_range);
        $balanceCash = Payment::balanceXendit("CASH");
        $balanceSaldo = Payment::balancesaldokeyta();
        $data=[
            'title' => 'Disbursement',
            'user_id'=> $id,
            'access_level' => $access_level,
            'disbursements' => $disbursement,
            'date_range' => $date_range,
            'balance_cash' => $balanceCash,
            'balance_saldo' => $balanceSaldo
        ];
    	return view('admin/admin/disbursement/list')->with($data);
    }
}
