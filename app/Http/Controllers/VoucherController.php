<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Voucher;
use Facades\App\Helper\Expedition;
use App\Exports\TransactionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class VoucherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher Blast',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/voucher/list')->with($data);
    }

    public function shop_index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list_with_service($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher by Shop',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions,
            'selected_expedition' => null
        ];
    	return view('admin/admin/voucher/shop-list')->with($data);
    }

    public function queue_index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list_with_service($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher Queues',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/voucher/queue-list')->with($data);
    }

    public function blast_voucher(Request $request, $id)
    {
        $voucher_type = $request->post('voucher_type');

        $checkbox_expd = $request->post('expeditions');
        $expedition_id = implode(",",$checkbox_expd);
        $expedition_ids = "[".$expedition_id."]";

        $checkbox_pay = $request->post('payment_method');
        $payments = implode(",",$checkbox_pay);
        $payment_method = "[".$payments."]";

        $voucher_title = $request->post('voucher_title');
        $voucher_value = $request->post('voucher_value');
        $code = $request->post('code');
        $unit = $request->post('unit');
        $max_value = $request->post('max_value');
        $date_range = str_replace(' ', '', $request->post('date_range'));
        $promo_template = $request->post('promo_template');
        $description = $request->post('description');
        $voucher_image = $request->file('voucher_image');
        $voucher_logo = $request->file('voucher_logo');
        $quantity = $request->post('voucher_by_user_qty');
        $max_shipping_price = $request->post('max_shipping_price');
        $min_shipping_price = $request->post('min_shipping_price');

        $dates = $pieces = explode("-", $date_range);
        $start_date = date("Y-m-d", strtotime($dates[0]."-".$dates[1]."-".$dates[2]));
        $expired_date = date("Y-m-d", strtotime($dates[3]."-".$dates[4]."-".$dates[5]));

        if($payment_method == ""){
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        } else {
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'payment_method',
                    'contents' => $payment_method,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        }

        set_time_limit(1200);
        
        $token = Helper::getToken();
        $blast = Voucher::blastVoucher($token, $data);

    	return redirect()->route('voucher_blast', ['id' => $id])->with(['message' => 'Blast Voucher Success']);
    }

    public function shop_voucher(Request $request, $id)
    {
        $data = [];
        $voucher_type = $request->post('voucher_type');
        array_push($data, [
            'name' => 'voucher_type',
            'contents' => strtolower($voucher_type),
        ]);

        $shop_id = $request->post('shop_id');
        if ($shop_id == null) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'shop_id is required!']);
        } else {
            array_push($data, [
                'name' => 'shop_id',
                'contents' => strtolower($shop_id),
            ]);
        }

        $code = $request->post('code');
        if (!isset($code)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Voucher code is required']);
        } else {
            array_push($data, [
                'name' => 'code',
                'contents' => $code,
            ]);
        }

        $checkbox_expd = $request->post('expeditions');
        if (isset($checkbox_expd)) {
            $expedition_id = implode(",",$checkbox_expd);
            $expedition_ids = "[".$expedition_id."]";
            array_push($data, [
                'name' => 'expedition_ids',
                'contents' => $expedition_ids,
            ]);
        }

        $checkbox_pay = $request->post('payment_method');
        if (isset($checkbox_pay)) {
            $payments = implode(",",$checkbox_pay);
            $payment_method = "[".$payments."]";
            array_push($data, [
                'name' => 'payment_method',
                'contents' => $payment_method,
            ]);
        }

        $voucher_title = $request->post('voucher_title');
        if (!isset($voucher_title)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Voucher title is required']);
        }
        array_push($data, [
            'name' => 'voucher_title',
            'contents' => $voucher_title,
        ]);

        $order_type = $request->post('order_type');
        if (!isset($order_type)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'order type is required']);
        }  
        array_push($data, [
            'name' => 'order_type',
            'contents' => $order_type,
        ]);

        $checkbox_service_type = $request->post('service_types');
        if (isset($checkbox_service_type)) {
            $service_types = implode(",",$checkbox_service_type);
            $service_type = "[".$service_types."]";
            array_push($data, [
                'name' => 'service_codes',
                'contents' => $service_type,
            ]);
        }

        $voucher_value = $request->post('voucher_value');
        if (!isset($voucher_value)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Voucher value is required']);
        }
        array_push($data, [
            'name' => 'value',
            'contents' => $voucher_value,
        ]);

        $unit = $request->post('unit');
        if (!isset($unit)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Unit is required']);
        }
        array_push($data, [
            'name' => 'unit',
            'contents' => strtolower($unit),
        ]);

        $max_value = $request->post('max_value');
        if (!isset($max_value)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Max Value is required']);
        }
        array_push($data, [
            'name' => 'max_value',
            'contents' => $max_value,
        ]);

        $quantity = $request->post('voucher_by_user_qty');
        if (!isset($quantity)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Quantity is required']);
        }
        array_push($data, [
            'name' => 'voucher_by_user_qty',
            'contents' => $quantity,
        ]);

        $date_range = str_replace(' ', '', $request->post('date_range'));
        if ($date_range == "") {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Date Range is required']);
        } else {
            $voucher_dates = $pieces = explode("-", $date_range);
            $voucher_start_date = $voucher_dates[0]."-".$voucher_dates[1]."-".$voucher_dates[2];
            $voucher_expired_date = $voucher_dates[3]."-".$voucher_dates[4]."-".$voucher_dates[5];
            $start_time = $request->post('date_range_start_time');
            $expired_time = $request->post('date_range_expired_time');
            if (isset($start_time)) {
                $voucher_start_date = $voucher_start_date." ".$start_time;
            } 
            if (isset($expired_time)) {
                $voucher_expired_date = $voucher_expired_date." ".$expired_time;
            } 
            array_push($data, [
                'name' => 'start_date',
                'contents' => $voucher_start_date,
            ]);
            array_push($data, [
                'name' => 'expired_date',
                'contents' => $voucher_expired_date,
            ]);
        }

        $min_shipping_price = $request->post('min_shipping_price');
        if (isset($min_shipping_price)) {
            array_push($data, [
                'name' => 'min_shipping_price',
                'contents' => $min_shipping_price
            ]);
        }

        $max_shipping_price = $request->post('max_shipping_price');
        if (isset($max_shipping_price)) {
            array_push($data, [
                'name' => 'max_shipping_price',
                'contents' => $max_shipping_price
            ]);
        }

        $promo_template = $request->post('promo_template');
        if (!isset($promo_template)) {
            return redirect()->route('voucher_shop', ['id' => $id])->with(['error' => 'Promo template is required']);
        }
        array_push($data, [
            'name' => 'promo_template',
            'contents' => $promo_template,
        ]);

        $description = $request->post('description');
        if (!isset($description)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Description is required']);
        }
        array_push($data, [
            'name' => 'description',
            'contents' => $description,
        ]);

        $voucher_image = $request->file('voucher_image');
        if (isset($voucher_image)) {
            array_push($data, [
                'name'     => 'voucher_image',
                'contents' => file_get_contents($voucher_image),
                'filename' => 'voucher_image'
            ]);
        }
    
        $voucher_logo = $request->file('voucher_logo');
        if (isset($voucher_logo)) {
            array_push($data, [
                'name'     => 'voucher_promo_image',
                'contents' => file_get_contents($voucher_logo),
                'filename' => 'voucher_logo'
            ]);
        }

        // dd($data);
        $token = Helper::getToken();
        $blast = Voucher::shopVoucher($token, $data);

    	return redirect()->route('voucher_shop', ['id' => $id])->with(['message' => 'Give Voucher Success']);
    }

    public function queue_voucher(Request $request, $id)
    {
        $data = [];
        $voucher_type = $request->post('voucher_type');
        array_push($data, [
            'name' => 'voucher_type',
            'contents' => strtolower($voucher_type),
        ]);

        $code = $request->post('code');
        if (!isset($code)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Voucher code is required']);
        } else {
            array_push($data, [
                'name' => 'code',
                'contents' => $code,
            ]);
        }

        $checkbox_expd = $request->post('expeditions');
        if (isset($checkbox_expd)) {
            $expedition_id = implode(",",$checkbox_expd);
            $expedition_ids = "[".$expedition_id."]";
            array_push($data, [
                'name' => 'expedition_ids',
                'contents' => $expedition_ids,
            ]);
        }

        $checkbox_pay = $request->post('payment_method');
        if (isset($checkbox_pay)) {
            $payments = implode(",",$checkbox_pay);
            $payment_method = "[".$payments."]";
            array_push($data, [
                'name' => 'payment_method',
                'contents' => $payment_method,
            ]);
        }
        
        $voucher_title = $request->post('voucher_title');
        if (!isset($voucher_title)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Voucher title is required']);
        }
        array_push($data, [
            'name' => 'voucher_title',
            'contents' => $voucher_title,
        ]);

        $checkbox_service_type = $request->post('service_types');
        if (isset($checkbox_service_type)) {
            $service_types = implode(",",$checkbox_service_type);
            $service_type = "[".$service_types."]";
            array_push($data, [
                'name' => 'service_codes',
                'contents' => $service_type,
            ]);
        }

        $voucher_value = $request->post('voucher_value');
        if (!isset($voucher_value)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Voucher value is required']);
        }
        array_push($data, [
            'name' => 'value',
            'contents' => $voucher_value,
        ]);

        $unit = $request->post('unit');
        if (!isset($unit)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Unit is required']);
        }
        array_push($data, [
            'name' => 'unit',
            'contents' => strtolower($unit),
        ]);

        $max_value = $request->post('max_value');
        if (!isset($max_value)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Max Value is required']);
        }
        array_push($data, [
            'name' => 'max_value',
            'contents' => $max_value,
        ]);

        $quantity = $request->post('quantity');
        if (!isset($quantity)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Quantity is required']);
        }
        array_push($data, [
            'name' => 'quantity',
            'contents' => $quantity,
        ]);

        $date_range = str_replace(' ', '', $request->post('date_range'));
        if ($date_range == "") {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Queue Date Range is required']);
        } else {
            $dates = $pieces = explode("-", $date_range);
            $start_date = $dates[0]."-".$dates[1]."-".$dates[2];
            $expired_date = $dates[3]."-".$dates[4]."-".$dates[5];
            $start_time = $request->post('queue_range_start_time');
            $expired_time = $request->post('queue_range_expired_time');
            if (isset($start_time)) {
                $start_date = $start_date." ".$start_time;
            } 
            if (isset($expired_time)) {
                $expired_date = $expired_date." ".$expired_time;
            } 
            
            array_push($data, [
                'name' => 'queue_start_date',
                'contents' => $start_date,
            ]);
            array_push($data, [
                'name' => 'queue_expired_date',
                'contents' => $expired_date,
            ]);
        }

        $voucher_date_range = str_replace(' ', '', $request->post('voucher_date_range'));
        if ($voucher_date_range == "") {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Voucher Date Range is required']);
        } else {
            $voucher_dates = $pieces = explode("-", $date_range);
            $voucher_start_date = date("Y-m-d", strtotime($voucher_dates[0]."-".$voucher_dates[1]."-".$voucher_dates[2]));
            $voucher_expired_date = date("Y-m-d", strtotime($voucher_dates[3]."-".$voucher_dates[4]."-".$voucher_dates[5]));
            $start_time = $request->post('date_range_start_time');
            $expired_time = $request->post('date_range_expired_time');
            if (isset($start_time)) {
                $voucher_start_date = $voucher_start_date." ".$start_time;
            } 
            if (isset($expired_time)) {
                $voucher_expired_date = $voucher_expired_date." ".$expired_time;
            }

            array_push($data, [
                'name' => 'voucher_start_date',
                'contents' => $voucher_start_date,
            ]);
            array_push($data, [
                'name' => 'voucher_expired_date',
                'contents' => $voucher_expired_date,
            ]);
        }
        $max_shipping_price = $request->post('max_shipping_price');
        if (isset($max_shipping_price)) {
            array_push($data, [
                'name' => 'max_shipping_price',
                'contents' => $max_shipping_price
            ]);
        }

        $min_shipping_price = $request->post('min_shipping_price');
        if (isset($min_shipping_price)) {
            array_push($data, [
                'name' => 'min_shipping_price',
                'contents' => $min_shipping_price
            ]);
        }

        $promo_template = $request->post('promo_template');
        if (!isset($promo_template)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Promo template is required']);
        }
        array_push($data, [
            'name' => 'promo_template',
            'contents' => $promo_template,
        ]);

        $queue_type = $request->post('queue_type');
        if (!isset($queue_type)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'queue type is required']);
        } 
        array_push($data, [
            'name' => 'queue_type',
            'contents' => $queue_type,
        ]);

        $order_type = $request->post('order_type');
        if (!isset($order_type)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'order type is required']);
        }  
        array_push($data, [
            'name' => 'order_type',
            'contents' => $order_type,
        ]);
        
        $description = $request->post('description');
        if (!isset($description)) {
            return redirect()->route('voucher_queue', ['id' => $id])->with(['error' => 'Description is required']);
        }
        array_push($data, [
            'name' => 'description',
            'contents' => $description,
        ]);

        $voucher_image = $request->file('voucher_image');
        if (isset($voucher_image)) {
            array_push($data, [
                'name'     => 'voucher_image',
                'contents' => file_get_contents($voucher_image),
                'filename' => 'voucher_image'
            ]);
        }
    
        $voucher_logo = $request->file('voucher_logo');
        if (isset($voucher_logo)) {
            array_push($data, [
                'name'     => 'voucher_promo_image',
                'contents' => file_get_contents($voucher_logo),
                'filename' => 'voucher_logo'
            ]);
        }

        $voucher_limit = $request->post('voucher_limit');
        if (isset($voucher_limit)) {
            array_push($data, [
                'name' => 'voucher_limit',
                'contents' => $voucher_limit,
            ]);
        }
        
        $token = Helper::getToken();
        $blast = Voucher::queueVoucher($token, $data);

    	return redirect()->route('voucher_queue', ['id' => $id])->with(['message' => 'Give Voucher Success']);
    }

    public function list($id) 
	{
        $token = Helper::getToken();
        $vouchers = Voucher::voucherTitleLists($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'vouchers' => $vouchers
        ];
    	return view('admin/admin/voucher_list/list')->with($data);
    }

    public function list_by_title(Request $request, $id, $voucher_title, $page) 
	{
        $filter = $request->post('filter');
        $token = Helper::getToken();
        $vouchers = Voucher::voucherByTitle($token, $voucher_title, $page, $filter);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'vouchers' => $vouchers,
            'voucher_title' => $voucher_title,
            'filter' => $filter
        ];
    	return view('admin/admin/voucher_list/details')->with($data);
    }

    public function update(Request $request, $id, $voucher_title) 
	{
        $filter = $request->post('filter');
        $token = Helper::getToken();
        $vouchers = Voucher::voucherByTitle($token, $voucher_title, 1, $filter);
        $expeditions = Expedition::list_with_service($token);
        $access_level = \App\User::find($id);

        $newExpedition = trim($vouchers['voucher'][0]['expedition_ids'], '[]');
        if (Str::contains($newExpedition, ",")) {
            $newExpedition = explode(",",$newExpedition);
        } else {
            $newExpedition = [$newExpedition];
        }

        $newPaymentMethod = trim($vouchers['voucher'][0]['payment_method'], '[]');
        if (Str::contains($newPaymentMethod, ",")) {
            $newPaymentMethod = explode(",",$newPaymentMethod);
        } else {
            $newPaymentMethod = [$newPaymentMethod];
        }
        $newStartTime = substr($vouchers['voucher'][0]['start_date'], 11, 11);
        $newEndTime = substr($vouchers['voucher'][0]['expired_date'], 11, 11);

        $newDateRange = substr($vouchers['voucher'][0]['start_date'], 0, 10)." - ".substr($vouchers['voucher'][0]['expired_date'], 0, 10);
        $data=[
            'title' => 'Update Voucher List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'vouchers' => $vouchers['voucher'][0],
            'voucher_title' => $voucher_title,
            'expeditions' => $expeditions,
            'newExpeditions' => $newExpedition,
            'newPaymentMethods' => $newPaymentMethod,
            'newDateRange' => $newDateRange,
            'newStartTime' => $newStartTime,
            'newEndTime' => $newEndTime,
            'filter' => $filter
        ];
    	return view('admin/admin/voucher/update-list')->with($data);
    }

    public function update_voucher(Request $request, $id, $voucher_title)
    {
        $data = [];
        $voucher_code = $voucher_title;
        $password = $request->post('password');
        if ($password == null) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'password is required!']);
        } else if ($password != "zakytuolol") {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'password is invalid!']);
        }

        $voucher_type = $request->post('voucher_type');
        array_push($data, [
            'name' => 'voucher_type',
            'contents' => strtolower($voucher_type),
        ]);

        $code = $request->post('code');
        if (!isset($code)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Voucher code is required']);
        } else {
            array_push($data, [
                'name' => 'code',
                'contents' => $code,
            ]);
        }

        $checkbox_expd = $request->post('expeditions');
        if (isset($checkbox_expd)) {
            $expedition_id = implode(",",$checkbox_expd);
            $expedition_ids = "[".$expedition_id."]";
            array_push($data, [
                'name' => 'expedition_ids',
                'contents' => $expedition_ids,
            ]);
        }

        $checkbox_pay = $request->post('payment_method');
        if (isset($checkbox_pay)) {
            $payments = implode(",",$checkbox_pay);
            $payment_method = "[".$payments."]";
            array_push($data, [
                'name' => 'payment_method',
                'contents' => $payment_method,
            ]);
        }

        $voucher_title = $request->post('voucher_title');
        if (!isset($voucher_title)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Voucher title is required']);
        }
        array_push($data, [
            'name' => 'voucher_title',
            'contents' => $voucher_title,
        ]);

        $order_type = $request->post('order_type');
        if (!isset($order_type)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'order type is required']);
        }  
        array_push($data, [
            'name' => 'order_type',
            'contents' => $order_type,
        ]);

        $checkbox_service_type = $request->post('service_types');
        if (isset($checkbox_service_type)) {
            $service_types = implode(",",$checkbox_service_type);
            $service_type = "[".$service_types."]";
            array_push($data, [
                'name' => 'service_codes',
                'contents' => $service_type,
            ]);
        }

        $voucher_value = $request->post('voucher_value');
        if (!isset($voucher_value)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Voucher value is required']);
        }
        array_push($data, [
            'name' => 'value',
            'contents' => $voucher_value,
        ]);

        $unit = $request->post('unit');
        if (!isset($unit)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Unit is required']);
        }
        array_push($data, [
            'name' => 'unit',
            'contents' => strtolower($unit),
        ]);

        $max_value = $request->post('max_value');
        if (!isset($max_value)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Max Value is required']);
        }
        array_push($data, [
            'name' => 'max_value',
            'contents' => $max_value,
        ]);

        $quantity = $request->post('voucher_by_user_qty');
        if (!isset($quantity)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Quantity is required']);
        }
        array_push($data, [
            'name' => 'voucher_by_user_qty',
            'contents' => $quantity,
        ]);

        $date_range = str_replace(' ', '', $request->post('date_range'));
        if ($date_range == "") {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Date Range is required']);
        } else {
            $voucher_dates = $pieces = explode("-", $date_range);
            $voucher_start_date = $voucher_dates[0]."-".$voucher_dates[1]."-".$voucher_dates[2];
            $voucher_expired_date = $voucher_dates[3]."-".$voucher_dates[4]."-".$voucher_dates[5];
            $start_time = $request->post('date_range_start_time');
            $expired_time = $request->post('date_range_expired_time');

            if (isset($start_time)) {
                $voucher_start_date = $voucher_start_date." ".$start_time;
            } 
            if (isset($expired_time)) {
                $voucher_expired_date = $voucher_expired_date." ".$expired_time;
            }
            array_push($data, [
                'name' => 'start_date',
                'contents' => $voucher_start_date,
            ]);
            array_push($data, [
                'name' => 'expired_date',
                'contents' => $voucher_expired_date,
            ]);
        }

        $min_shipping_price = $request->post('min_shipping_price');
        if (isset($min_shipping_price)) {
            array_push($data, [
                'name' => 'min_shipping_price',
                'contents' => $min_shipping_price
            ]);
        }

        $max_shipping_price = $request->post('max_shipping_price');
        if (isset($max_shipping_price)) {
            array_push($data, [
                'name' => 'max_shipping_price',
                'contents' => $max_shipping_price
            ]);
        }

        $promo_template = $request->post('promo_template');
        if (!isset($promo_template)) {
            return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Promo template is required']);
        }
        array_push($data, [
            'name' => 'promo_template',
            'contents' => $promo_template,
        ]);

        $description = $request->post('description');
        if (!isset($description)) {
            return redirect()->route('voucher_queue', ['id' => $id, 'voucher_title' => $voucher_code])->with(['error' => 'Description is required']);
        }
        array_push($data, [
            'name' => 'description',
            'contents' => $description,
        ]);

        $voucher_image = $request->file('voucher_image');
        if (isset($voucher_image)) {
            array_push($data, [
                'name'     => 'voucher_image',
                'contents' => file_get_contents($voucher_image),
                'filename' => 'voucher_image'
            ]);
        }
    
        $voucher_logo = $request->file('voucher_logo');
        if (isset($voucher_logo)) {
            array_push($data, [
                'name'     => 'voucher_promo_image',
                'contents' => file_get_contents($voucher_logo),
                'filename' => 'voucher_logo'
            ]);
        }

        $token = Helper::getToken();
        $blast = Voucher::updateVoucher($token, $data, $voucher_code);
    	return redirect()->route('voucher_update', ['id' => $id, 'voucher_title' => $voucher_code])->with(['message' => 'Update Voucher Success']);
    }
}
