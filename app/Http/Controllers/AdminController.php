<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Dashboard;
use Facades\App\Helper\Order;
use App\Exports\BanksExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role_access:admin');
	}

    public function index($id)
	{
        $token = Helper::getToken();
        $fetchApi = Dashboard::dashboard($token);
        $dashboard = $fetchApi['dashboard_metric'];
        $nsm = $fetchApi['nsm_data'];
        $income = $fetchApi['nsm_income'];
        $date_range = null;
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "2020-11-01-{$end_date}";
        }
        $order = Order::ordersList($token, 1, null, null, null, $date_range, null);
		$access_level = \App\User::find($id);
        $data=[
            'title' => 'Dashboard',
            'user_id'=> $id,
            'access_level' => $access_level,
            'dashboard' => $dashboard,
            'nsm' => $nsm,
            'income' => $income,
            'order' => $order,
        ];
    	return view('admin/admin/dashboard/list')->with($data);
	}

}
