<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Keytasaldo
{
    public function pointsList($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer"." ".env('TOKEN_WALLET','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJrZXl0YXByb2R1Y3Rpb24iLCJpYXQiOjE2NDM5Njk2MDh9.2IlksExSOiaYH1DuMuf_juXHEshukIJe7Yq05fObmdE') ];

        $response = $client->request('GET', config('constants.api_url_wallet').'/saldo-service/users-balance?page_number='.$page.'&limit=25&keyword='.$keyword, [
            'headers' => $headers, 
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp['data'];
    }

    public function pointHistory($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer"." ".env('TOKEN_WALLET','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJrZXl0YXByb2R1Y3Rpb24iLCJpYXQiOjE2NDM5Njk2MDh9.2IlksExSOiaYH1DuMuf_juXHEshukIJe7Yq05fObmdE') ];

        $response = $client->request('GET', config('constants.api_url_wallet').'/saldo-service/users-history?page_number='.$page.'&limit=25&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $histories = json_decode($response->getBody()->getContents(), true);

        return $histories['data'];
    }
}