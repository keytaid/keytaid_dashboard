<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Product
{
    public function productsList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/products?page='.$page.'&per_page=25', [
            'headers' => $headers, 
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }

    public function productsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/products/export' , [
            'headers' => $headers
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }

    public function productsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/products/search?page='.$page.'&per_page=25&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }
}