<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Report
{
    public function reportsList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/report?page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/report/export' , [
            'headers' => $headers
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/report/search?keyword='.$keyword.'&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsResolve($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'id' => $id
        ];
        $response = $client->request('POST', config('constants.api_url')."/idb/report/{$id}/resolve" , [
            'headers' => $headers
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }
}