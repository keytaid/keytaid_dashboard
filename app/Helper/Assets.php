<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Assets
{
  public function assetsList($token, $search, $page)
  {
    $client = new \GuzzleHttp\Client();
    $headers = ['Authorization' => "Bearer {$token}" ];

    $response = $client->request('GET', config('constants.api_url').'/idb/assets?per_page=10&page='.$page.'&search='.$search , [
        'headers' => $headers, 
    ]);
    $assets = json_decode($response->getBody()->getContents(), true);

    return $assets;
  }

  public function uploadImage($token, $data)
  {
    $client = new \GuzzleHttp\Client();
    $headers = ['Authorization' => "Bearer {$token}" ];
    $body = $data;
    $response = $client->request('POST', config('constants.api_url').'/idb/assets' , [
        'headers' => $headers, 
        'multipart' => $body, 
    ]);
    return $asset = json_decode($response->getBody()->getContents(), true);
  }

  public function deleteImage($token, $data)
  {
    $client = new \GuzzleHttp\Client();
    $headers = ['Authorization' => "Bearer {$token}" ];
    $body = $data;
    $response = $client->request('DELETE', config('constants.api_url').'/idb/assets' , [
        'headers' => $headers, 
        'multipart' => $body, 
    ]);
    return $asset = json_decode($response->getBody()->getContents(), true);
  }
}