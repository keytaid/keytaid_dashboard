<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Notification
{
    public function index($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/notification/list?per_page=100' , [
            'headers' => $headers,
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications;
    }

    public function detail($token, $notif_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('PUT', config('constants.api_url').'/idb/notification/update/'.$notif_id , [
            'headers' => $headers, 
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications;
    }

    public function update($token, $notif_id, $body)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('PUT', config('constants.api_url').'/idb/notification/update/'.$notif_id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications;
    }

    public function create($token, $body)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('POST', config('constants.api_url').'/idb/notification/create' , [
            'headers' => $headers, 
            'multipart' => $body,
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications;
    }

}