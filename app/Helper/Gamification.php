<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Gamification
{
    public function indexReward($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('GET', config('constants.api_url').'/node/gamification/reward' , [
            'headers' => $headers, 
        ]);
        $reward = json_decode($response->getBody()->getContents(), true);

        return $reward;
    }

    public function detailReward($token, $id, $day)
    {

        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];;

        $response = $client->request('GET', config('constants.api_url').'/node/gamification/reward/'.$day , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $reward = json_decode($response->getBody()->getContents(), true);
        
        return $reward;
    }

    public function updateReward($token, $reward_id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = [ 'Authorization' => "Bearer {$token}" ];
        $body = $data;

        $response = $client->request('PATCH', config('constants.api_url').'/node/gamification/reward/update/'.$reward_id , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);

        $reward = json_decode($response->getBody()->getContents(), true);
        
        return $reward;
    }

    public function indexWheel($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('GET', config('constants.api_url').'/node/gamification/wheelvoucher' , [
            'headers' => $headers, 
        ]);
        $wheel = json_decode($response->getBody()->getContents(), true);

        return $wheel;
    }

    public function detailWheel($token, $wheel_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('GET', config('constants.api_url').'/node/idb/wheel/'.$wheel_id , [
            'headers' => $headers, 
        ]);
        $wheel = json_decode($response->getBody()->getContents(), true);
        
        return $wheel;
    }

    public function updateWheel($token, $wheel_id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = [ 'Authorization' => "Bearer {$token}" ];
        $body = $data;

        $response = $client->request('PATCH', config('constants.api_url').'/node/gamification/wheelvoucher/update/'.$wheel_id , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);

        $wheel = json_decode($response->getBody()->getContents(), true);
        
        return $wheel;
    }
    
    public function listHistory($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('POST', config('constants.api_url').'/node/idb/claimed' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp;
    }

    public function searchHistory($token, $page, $body)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $response = $client->request('POST', config('constants.api_url').'/node/idb/claimed/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp;
    }
}