<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Premium
{
    public function premiumOrdersList($token, $page, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/order/premiums?page='.$page.'&per_page=25&date_range='.$date_range , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders['data'];
    }

    public function ordersExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/premiums/export?date_range='.$date_range , [
            'headers' => $headers,
            // 'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function premiumOrdersSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/premium/search?page='.$page.'&per_page=25&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersCancel($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('PUT', config('constants.api_url')."/idb/orders/{$order_id}/cancel" , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function premiumPriceList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/premium/price/list' , [
            'headers' => $headers, 
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceListId($token, $pricing_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/premium/price/list/'.$pricing_id , [
            'headers' => $headers, 
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceEdit($token, $pricing_id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;

        $response = $client->request('PUT', config('constants.api_url').'/idb/premium/price/list/'.$pricing_id.'/update' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceStore($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url').'/premiums/pricing/create' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumShopIndex($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/premium/shop/list?page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }

    public function premiumShopSearch($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/premium/shop/search?keyword='.$keyword.'&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }

    public function premiumPriceDelete($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('DELETE', config('constants.api_url').'/idb/premium/price/'.$id.'/delete' , [
            'headers' => $headers, 
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }

    public function requestDownload($date_range, $whatsapp)
    {
        $client = new \GuzzleHttp\Client();
        $body = [
            'date_range' => $date_range, 
            'phone' => $whatsapp,
            'table' => 'premiums',
        ];
        $response = $client->request('POST', 'https://queue.keyta.id/request-export/premium' , [
            'json' => $body,
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }
}