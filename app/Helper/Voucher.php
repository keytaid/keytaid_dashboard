<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Voucher
{   
    public function blastVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/idb/voucher/blast" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function shopVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/idb/voucher/create" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function updateVoucher($token, $data, $voucher_title)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('PUT', config('constants.api_url_v2')."/idb/voucher/update/".$voucher_title , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher;
    }

    public function queueVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/idb/voucher/queue" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function voucherTitleLists($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2')."/idb/voucher/group_by/code" , [
            'headers' => $headers,
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher['data'];
    }

    public function voucherTitleList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2')."/idb/voucher/group_by/voucher_title" , [
            'headers' => $headers,
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher['data'];
    }

    public function voucherByTitle($token, $voucher_title, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('GET', config('constants.api_url_v2').'/idb/voucher/list?voucher_title='.$voucher_title.'&page='.$page.'&per_page=25&filter='.$filter , [
            'headers' => $headers,
            // 'json' => $body,
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher['data'];
    }
}