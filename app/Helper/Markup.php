<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Markup
{
    public function indexMarkup($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions?page=1&per_page=25' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);
        // dd($expeditions);
        return $expeditions;
    }

    public function detailMarkup($token, $id, $expedition_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/markup/show?expedition_id='.$expedition_id.'&page=1&per_page=25' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);
        return $expeditions;
    }

    public function eachMarkup($token, $id, $markup_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/markup/show/'.$markup_id , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);
        return $expeditions;
    }

    public function updateMarkup($token, $id, $markup_id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('PUT', config('constants.api_url').'/idb/markup/update/'.$markup_id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);
        return $expeditions;
    }
}