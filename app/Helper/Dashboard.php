<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Dashboard
{
    public function dashboard($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/dashboard/lists' , [
            'headers' => $headers
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders['data'];
    }
}