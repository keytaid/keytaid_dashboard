<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Keytauser
{
    public function userList($token, $page, $community_name, $sort_by, $sort_table_name, $sort_column_name, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        if(!isset($sort_table_name)){
            $sort_table_name = "users";
        }

        if(!isset($sort_column_name)){
            $sort_column_name = "id";
        }

        if(!isset($sort_by)){
            $sort_by = "DESC";
        }

        $response = $client->request('GET', config('constants.api_url').'/idb/users?page='.$page.'&per_page=25&table_name='.$sort_table_name.'&table_column='.$sort_column_name.'&sort_by='.$sort_by.'&date_range='.$date_range.'&community_name='.$community_name , [
            'headers' => $headers, 
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function communityList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/community/list/name' , [
            'headers' => $headers, 
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function dashboard($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/dashboard' , [
            'headers' => $headers
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function userExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/users/export' , [
            'headers' => $headers
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function userBalanceExport($token, $shop_id_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/keyta_point/user/balance/export?shop_id_range='.$shop_id_range , [
            'headers' => $headers,
        ]);
        $usersBalance = json_decode($response->getBody()->getContents(), true);

        return $usersBalance;
    }

    public function userSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/users/search?keyword='.$keyword.'&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function referralList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/referral?page='.$page.'&per_page=25' , [
            'headers' => $headers,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function userCommunity($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/users/community/canvaser?referrer=4301DZ0D0JQ7,LUNASUMKM&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $users = json_decode($response->getBody()->getContents(), true);
        return $users;
    }

    public function userCommunityInv($token, $shop_id, $page, $referrer)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [
        //     'page' => $page, 
        //     'per_page' => 25,
        //     'shop_id' => $shop_id,
        // ];

        $response = $client->request('GET', config('constants.api_url').'/users/community/canvaser/inv?referrer='.$referrer.'&page='.$page.'&per_page=25&shop_id='.$shop_id , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);
        // dd($users);
        return $users;
    }
}