<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Transaction
{
    public function transactionsList($token, $page, $channel, $expedition_name, $payment_method, $status, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/transactions?page='.$page.'&per_page=25&channel='.$channel.'&date_range='.$date_range.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&status='.$status , [
            'headers' => $headers, 
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);
        return $transactions;
    }

    public function transactionsExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [ 
        //     'date_range' => $date_range,
        // ];
        $response = $client->request('GET', config('constants.api_url').'/idb/transactions/export?date_range='.$date_range , [
            'headers' => $headers,
            // 'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }

    public function transactionsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/transactions/all/search?page='.$page.'&per_page=25&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }

    public function requestDownload($date_range, $whatsapp)
    {
        $client = new \GuzzleHttp\Client();
        $body = [
            'date_range' => $date_range, 
            'phone' => $whatsapp,
            'table' => 'invoices',
        ];
        $response = $client->request('POST', 'https://queue.keyta.id/request-export' , [
            'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }
}