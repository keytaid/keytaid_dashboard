<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Remoteconfig
{
    public function index($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/config' , [
            'headers' => $headers,
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications['configs'];
    }

    public function create($token, $body)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('POST', config('constants.api_url').'/idb/config' , [
            'headers' => $headers,
            'json' => $body, 
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);

        return $notifications;
    }

    public function getByName($token, $name)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('GET', config('constants.api_url').'/idb/config/name?name='.$name , [
            'headers' => $headers,
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);
        // dd($notifications);
        return $notifications;
    }

    public function update($token, $body, $remote_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('PUT', config('constants.api_url').'/idb/config/'.$remote_id , [
            'headers' => $headers,
            'json' => $body, 
        ]);
        $notifications = json_decode($response->getBody()->getContents(), true);
        // dd($notifications);
        return $notifications;
    }
}