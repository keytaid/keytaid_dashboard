<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Helper
{
    public $token = null;

    public function getToken(){
        $client = new \GuzzleHttp\Client();

        if ($this->token === null){
            // $response = $client->request('POST', config('constants.api_url').'/request_otps/verify' , ['json' => [
            //     'authy_id' => "222222222",
            //     'token' => "1234567"
            // ]]);
    
            // $parsed_response = json_decode($response->getBody()->getContents(), true);
            // $this->token = $parsed_response['data']['token'];
            $this->token = env('TOKEN_MAIN','eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo4OSwiZXhwIjoxNjkxODMyNTMwfQ.XzXXm31-fSZIk_OcQOGN1HH6AAyTAe67hBW1i3niDeg');
        }
        
        return $this->token;
    }

    function debug_to_console($data) {
        $output = $data;
        if (is_array($output))
            $output = implode(',', $output);
    
        echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
    }
}