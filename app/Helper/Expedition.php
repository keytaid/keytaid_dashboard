<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Expedition
{
    public function expeditionsList($token, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = ['page' => $page, 'per_page' => 25, 'filter' => $filter];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions/list?page='.$page.'&per_page=25&filter='.$filter , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);

        $expeditions = json_decode($response->getBody()->getContents(), true);
        return $expeditions;
    }

    public function expeditionsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions/export' , [
            'headers' => $headers
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function expeditionsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions/search?keyword='.$keyword.'&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function list($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions?page=1&per_page=25' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function list_with_service($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/order/expeditions' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions['data'];
    }

    public function list_by_id($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/expeditions/'.$id.'?page=1&per_page=25' , [
            'headers' => $headers, 
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function listUpdate($token, $id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('PUT', config('constants.api_url').'/idb/expeditions/'.$id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expedition = json_decode($response->getBody()->getContents(), true);

        return $expedition;
    }
}