<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class BankAccount
{
    public function bankAccountList($token, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/bank_accounts?per_page=25&page='.$page.'&filter='.$filter , [
            'headers' => $headers, 
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }

    public function bankAccountExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/bank_accounts/export' , [
            'headers' => $headers
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }

    public function bankAccountSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/bank_accounts/search?keyword='.$keyword.'&page='.$page.'&per_page=25' , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }
}