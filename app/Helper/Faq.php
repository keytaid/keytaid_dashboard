<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Faq
{
    public function faqList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/faq/show' , [
            'headers' => $headers, 
        ]);
        $faqs = json_decode($response->getBody()->getContents(), true);

        return $faqs;
    }

    public function faqById($token, $faq_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/faq/show/'.$faq_id , [
            'headers' => $headers, 
        ]);
        $faq = json_decode($response->getBody()->getContents(), true);

        return $faq;
    }

    public function faqUpdate($token, $faq_id, $question, $answer, $category, $subcategory, $category_ordering, $subcategory_ordering)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "category" => $category,
            "subcategory" => $subcategory,
            "category_ordering" => $category_ordering,
            "subcategory_ordering" => $subcategory_ordering,
            "question" => $question,
            "answer" => $answer
        ];

        $response = $client->request('PUT', config('constants.api_url').'/idb/faq/update/'.$faq_id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $faq = json_decode($response->getBody()->getContents(), true);

        return $faq;
    }

    public function faqAdd($token, $question, $answer, $category, $subcategory, $category_ordering, $subcategory_ordering)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "category" => $category,
            "subcategory" => $subcategory,
            "category_ordering" => $category_ordering,
            "subcategory_ordering" => $subcategory_ordering,
            "question" => $question,
            "answer" => $answer
        ];

        $response = $client->request('POST', config('constants.api_url').'/idb/faq/create' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $faq = json_decode($response->getBody()->getContents(), true);

        return $faq;
    }
}