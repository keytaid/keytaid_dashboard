<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Payment
{
    public function disbursement_list($token, $page, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/payment/disbursement?page='.$page.'&per_page=25&date_range='.$date_range , [
            'headers' => $headers,
        ]);
        $disbursement = json_decode($response->getBody()->getContents(), true);
        
        // dd($disbursement['data']);
        return $disbursement['data'];
    }

    public function paymentlink_list($token, $page, $date_range, $status, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/payment/paymentlink?page='.$page.'&per_page=25&date_range='.$date_range.'&status='.$status.'&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $disbursement = json_decode($response->getBody()->getContents(), true);
        return $disbursement['data'];
    }

    public function resolve($token, $trx_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
    
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/payment-link/resolve/'.$trx_id, [
            'headers' => $headers
        ]);
        $disbursement = json_decode($response->getBody()->getContents(), true);

        return $disbursement['data'];
    }

    public function listfee($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/payment/paymentlink/fee', [
            'headers' => $headers
        ]);
        $disbursement = json_decode($response->getBody()->getContents(), true);
        return $disbursement['data'];
    }
    
    public function balanceXendit($type)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.xendit.co/balance?account_type='.$type, [
            'auth' => [
                'xnd_production_s9IkZimrWIJgDZcaYls84uyP7vg0RT3S6LlOWB7EDhsXpmC9YyYkdDuj9q77DH',
                null
            ]
        ]);
        $balance = json_decode($response->getBody()->getContents(), true);

        return $balance;
    }

    public function balancesaldokeyta()
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer"." ".env('TOKEN_WALLET','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJrZXl0YXByb2R1Y3Rpb24iLCJpYXQiOjE2NDM5Njk2MDh9.2IlksExSOiaYH1DuMuf_juXHEshukIJe7Yq05fObmdE') ];

        $response = $client->request('GET', config('constants.api_url_wallet').'/saldo-service/balance-keyta' , [
            'headers' => $headers, 
        ]);
        $histories = json_decode($response->getBody()->getContents(), true);

        return $histories['data'];
    }

}