<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Order
{
    public function ordersList($token, $page, $expedition_name, $payment_method, $voucher_title, $date_range, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url_v2').'/idb/order/couriers?page='.$page.'&per_page=25&expedition_name='.$expedition_name.'&date_range='.$date_range.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&keyword='.$keyword  , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        
        return $orders['data'];
    }

    public function orderDetails($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $response = $client->request('GET', config('constants.api_url')."/idb/orders/detail/{$order_id}" , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        
        return $orders;
    }

    public function tracking_order($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $response = $client->request('GET', config('constants.api_url')."/idb/orders/{$order_id}/tracking" , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        
        return $orders;
    }

    public function ordersExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/orders/export?date_range='.$date_range , [
            'headers' => $headers,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersSearch($token, $keyword, $page,$expedition_name, $payment_method, $voucher_title)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/orders/search?page='.$page.'&per_page=25&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher_title.'&keyword='.$keyword  , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersCancel($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('PUT', config('constants.api_url')."/idb/orders/{$order_id}/cancel" , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersListDifference($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/idb/order/difference/list?page='.$page.'&per_page=25' , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function createDifference($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('POST', config('constants.api_url')."/idb/order/difference" , [
            'headers' => $headers, 
            'json' => $data,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function requestDownload($date_range, $whatsapp)
    {
        $client = new \GuzzleHttp\Client();
        $body = [
            'date_range' => $date_range, 
            'phone' => $whatsapp,
            'table' => 'deliveries',
        ];
        $response = $client->request('POST', 'https://queue.keyta.id/request-export/delivery' , [
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

}