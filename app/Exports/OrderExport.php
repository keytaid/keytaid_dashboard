<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Order;

class OrderExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $date_range;

    function __construct(string $date_range) {
        $this->date_range = $date_range;
    }

    public function array(): array
    {
        $token = Helper::getToken();
        $orders = Order::ordersExport($token, $this->date_range);
        $i = 0;
        $x = 0;
        $last_id = 0;
        // print_r($orders);
        // error_log($orders);
        foreach ($orders['results'] as $payment) {
            // error_log($x++);
            
            // $json_string = json_encode($transaction, JSON_PRETTY_PRINT);
            // print_r($json_string);
            // echo '<br/><br/>';
            // if(count($transaction['payment2']) > 1){
                
            //     if($last_id == $transaction['id']){
            //         $i++;
            //     }
            //     else{
            //         $i=0;
            //     }
            // }
            // else{
            //     $i=0;
            // }

            if(isset($payment['order3'])){
                $order_id = $payment['order3']['id'];
                $sender_name = $payment['order3']['sender_name'];
                $sender_address = $payment['order3']['sender_address'];
                $receipent_name = $payment['order3']['receipent_name'];
                $receipent_address = $payment['order3']['destination_address'];
                $weight = $payment['order3']['weight'];
                $status = $payment['order3']['status'];
                $order_time = $payment['order3']['created_at'];
                $expedition_external_id = $payment['order3']['expedition_external_number'];
                $total_price = $payment['order3']['total_price'];
                $keytapoint_payment = $payment['order3']['keytapoint_payment'];
                $ewallet_payment = $payment['order3']['ewallet_payment'];
                $expedition_name = $payment['order3']['expedition']['name'];
                $expedition_service_type = $payment['order3']['expedition_service_type'];

                $resi_number = $payment['order3']['resi_number'];
                if ($resi_number == null){
                    $resi_number = $payment['order3']['expedition_external_number'];
                }
            }
            else{
                $order_id = "-";
                $sender_name = "-";
                $sender_address = "-";
                $receipent_name = "-";
                $receipent_address = "-";
                $weight = "-";
                $status = "-";
                $order_time = "-";
                $expedition_external_id = "-";
                $total_price = "-";
                $expedition_name = "-";
                $expedition_service_type = "-";
                $resi_number = "-";
            }

            if(isset($payment['trx'])){
                $invoice_date = $payment['trx']['created_at'];
                $shop_id = $payment['trx']['shop_id'];
                $number = $payment['trx']['number'];
                $shop_name = $payment['trx']['shop']['name'];
            }
            else{
                $invoice_date = "-";
                $shop_id = "-";
                $number = "-";
                $shop_name = "-";
            }
            
            $result[$x] = [
                'Tanggal Order' => $order_time,
                'Tanggal Invoice' => $invoice_date,
                'Shop Id' => $shop_id,
                'Nomor Invoice' => $number,
                'Nama Toko' => $shop_name,
                'Payment_id' => $payment['id'],
                'Order Id' => $order_id,
                'Nama Pengirim' => $sender_name,
                'Alamat Pengirim' => $sender_address,
                'Nama Penerima' => $receipent_name,
                'Alamat Penerima' => $receipent_address,
                'Berat' => $weight,
                'Ekspedisi' => $expedition_name,
                'Layanan' => $expedition_service_type,
                'Expedition External Id' => $expedition_external_id,
                "Nomor Resi" => $resi_number,
                'Shipping Price' => $total_price,
                'Keytapoint Payment' => $keytapoint_payment,
                'Ewallet Payment' => $ewallet_payment,
                'Voucher Value' => $payment['voucher_value'],
                'Payment Method' => $payment['ewallet_type'],
                'Reference Id' => $payment['external_id'],
                'Order Status' => $status
            ];
            $x++;
            // $last_id = $transaction['id'];
            
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Tanggal Order',
                'Tanggal Invoice',
                'Shop Id',
                'Nomor Invoice',
                'Nama Toko',
                'Payment Id',
                'Order Id',
                'Nama Pengirim',
                'Alamat Pengirim',
                'Nama Penerima',
                'Alamat Penerima',
                'Berat',
                'Ekspedisi',
                'Layanan',
                'Expedition External Id',
                "Nomor Resi",
                'Shipping Price',
                'Keytapoint Payment',
                'Ewallet Payment',
                'Voucher Value',
                'Payment Method',
                'Reference Id',
                'Order Status'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_DATE_DATETIME,
            'B' => NumberFormat::FORMAT_DATE_DATETIME,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_TEXT,
            'L' => NumberFormat::FORMAT_NUMBER,
            'M' => NumberFormat::FORMAT_TEXT,
            'N' => NumberFormat::FORMAT_TEXT,
            'O' => NumberFormat::FORMAT_TEXT,
            'P' => NumberFormat::FORMAT_TEXT,
            'Q' => NumberFormat::FORMAT_NUMBER,
            'R' => NumberFormat::FORMAT_NUMBER,
            'S' => NumberFormat::FORMAT_NUMBER,
            'T' => NumberFormat::FORMAT_NUMBER,
            'U' => NumberFormat::FORMAT_TEXT,
            'V' => NumberFormat::FORMAT_TEXT,
            'W' => NumberFormat::FORMAT_TEXT
        ];
    }
}
