<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Expedition;

class ExpeditionsExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $token = Helper::getToken();
        $expeditions = Expedition::expeditionsExport($token);
        $i = 0;
        foreach ($expeditions['results'] as $expedition) {
            if(isset($expedition['shops'])){
                $result[$i] = [
                    'Nama Toko' => $expedition['shops']['name'],
                    'Nomor Telepon Toko' => $expedition['shops']['phone'],
                    'ID Expedisi' => $expedition['expeditions']['id'],
                    'Nama Ekspedisi' => $expedition['expeditions']['name']
                ];
            }
            else {
                $result[$i] = [
                    'Nama Toko' => "",
                    'Nomor Telepon Toko' => "",
                    'ID Ekspedisi' => $expedition['expeditions']['id'],
                    'Nama Ekspedisi' => $expedition['expeditions']['name']
                ];
            }
            
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Nama Toko',
                'Nomor Telepon Toko',
                'ID Ekspedisi',
                'Nama expedition'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
