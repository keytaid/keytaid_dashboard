<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Transaction;

class TransactionsExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    protected $date_range;

    function __construct(string $date_range) {
        $this->date_range = $date_range;
    }

    public function array(): array
    {
        $token = Helper::getToken();
        $transactions = Transaction::transactionsExport($token, $this->date_range);
        $i = 0;
        foreach ($transactions['results'] as $transaction) {
            if(isset($transaction['shop'])){
                $shop_name = $transaction['shop']['name'];
            }
            else{
                $shop_name = "";
            }

            if(isset($transaction['expedition']['name'])){
                $expedition_name = $transaction['expedition']['name'];
                $expedition_service = $transaction['expedition']['expedition_service_type'];
            }
            else{
                $expedition_name = "";
                $expedition_service = "";
            }

            if(isset($transaction['bank'])){
                $bank = $transaction['bank']['name'];
            } 
            else{
                $bank = "";
            }
            $result[$i] = [
                'Tanggal Transaksi' => $transaction['created_at'],
                'Nomor Invoice' => $transaction['number'],
                'Nama Toko' => $shop_name,
                'Nama Pelanggan' => $transaction['customer']['customer_name'],
                'Channel' => $transaction['customer']['channel'],
                'Channel Phone/ID' => $transaction['customer']['channel_account'],
                'Expedition' => $expedition_name,
                'Layanan' => $expedition_service,
                'Shipping Price' => $transaction['shipping_price'],
                'Total Price' => $transaction['total_payment'],
                'Payment Method' => $bank,
                'Payment Status' => $transaction['payment_status'],
                'Shipment Status' => $transaction['shipment_status'],
                'Status' => $transaction['statusInDevice']
            ];
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Tanggal Transaksi',
                'Nomor Invoice',
                'Nama Toko',
                'Nama Pelanggan',
                'Channel',
                'Channel Phone/ID',
                'Expedition',
                'Layanan',
                'Shipping Price',
                'Total Price',
                'Payment_Method',
                'Payment Status',
                'Shipment Status',
                'Status'

            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_TEXT,
            'L' => NumberFormat::FORMAT_TEXT,
            'M' => NumberFormat::FORMAT_TEXT,
            'N' => NumberFormat::FORMAT_TEXT
        ];
    }
}
