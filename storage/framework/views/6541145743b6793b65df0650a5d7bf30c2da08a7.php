  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo e(URL::to('home')); ?>" class="brand-link" target="blank">
      <img src="<?php echo e(asset('fileUpload/keyta.png')); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light">Keyta Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{//{ asset('storage/'.$tenant['image']) }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{//{ ucfirst($tenant['full_name'])}}</a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
          <?php if($access_level['role_id']==1): ?>
            <li class="nav-item ml-3">
              <a href="<?php echo e(URL::to('canvaser/'.$user_id.'/keytauser/1')); ?>" class="nav-link <?php echo e($title=='Keyta Users'?'active':''); ?>">
                <i class="fa fa-user nav-icon"></i>
                <p>Keyta User</p>
              </a>
            </li>
          <?php elseif($access_level['role_id']==2): ?>
          <li class="nav-item">
            <a href="<?php echo e(URL::to('admin/'.$user_id)); ?>" class="nav-link <?php echo e($title=='Dashboard'?'active':''); ?>">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview <?php echo e(in_array($title,['Remote Config','Paymentlink Fee','Notification','FAQ','Voucher Blast', 'Voucher by Shop','Markup Cost','Premium Pricing','Expedition List', 'Wheel of Fortune', 'Daily Reward', 'Daily Reward History', 'Voucher Queues', 'Assets gallery'])?'menu-open':''); ?>">
            <a href="#" class="nav-link <?php echo e(in_array($title,['Remote Config','Paymentlink Fee','Notification', 'FAQ','Voucher Blast', 'Voucher by Shop','Markup Cost','Premium Pricing','Expedition List', 'Wheel of Fortune', 'Daily Reward', 'Daily Reward History', 'Voucher Queues', 'Assets gallery'])?'active':''); ?>">
              <i class="nav-icon fa fa-server"></i>
              <p>
                App Management 
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3 <?php echo e(in_array($title,['Voucher Blast', 'Voucher by Shop', 'Voucher Queues'])?'menu-open':''); ?>">
                <a href="#" class="nav-link <?php echo e(in_array($title,['Voucher Blast', 'Voucher by Shop', 'Voucher Queues'])?'active':''); ?>">
                  <i class="nav-icon fa fa-home"></i>
                  <p>
                    Voucher
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/app-management/voucher')); ?>" class="nav-link <?php echo e($title=='Voucher Blast'?'active':''); ?>">
                      <i class="fa fa-user nav-icon"></i>
                      <p>Voucher Blast</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/app-management/voucher-shop')); ?>" class="nav-link <?php echo e($title=='Voucher by Shop'?'active':''); ?>">
                      <i class="fa fa-user nav-icon"></i>
                      <p>Voucher by Shop</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/app-management/voucher-queue')); ?>" class="nav-link <?php echo e($title=='Voucher Queues'?'active':''); ?>">
                      <i class="fa fa-user nav-icon"></i>
                      <p>Voucher Queues</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item ml-3 <?php echo e(in_array($title,['Daily Reward', 'Wheel of Fortune','Gamification', 'Daily Reward History'])?'menu-open':''); ?>">
                <a href="#" class="nav-link <?php echo e(in_array($title,['Daily Reward', 'Wheel of Fortune', 'Gamification','Daily Reward History'])?'active':''); ?>">
                  <i class="nav-icon fa fa-gamepad"></i>
                  <p>
                    Gamification
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/gamification/reward')); ?>" class="nav-link <?php echo e($title=='Daily Reward'?'active':''); ?>">
                      <i class="fa fa-gift nav-icon"></i>
                      <p>Daily Reward</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/gamification/wheel')); ?>" class="nav-link <?php echo e($title=='Wheel of Fortune'?'active':''); ?>">
                      <i class="fa fa-spinner nav-icon"></i>
                      <p>Wheel of Fortune</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/gamification/history/1')); ?>" class="nav-link <?php echo e($title=='Daily Reward History'?'active':''); ?>">
                  <i class="fa fa-gamepad nav-icon"></i>
                  <p>History Reward</p>
                </a>
              </li>
                </ul>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/markup/1')); ?>" class="nav-link <?php echo e($title=='Markup Cost'?'active':''); ?>">
                  <i class="fa fa-usd nav-icon"></i>
                  <p>Markup Cost</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/paymentlink_fee/1')); ?>" class="nav-link <?php echo e($title=='Paymentlink Fee'?'active':''); ?>">
                  <i class="fa fa-usd nav-icon"></i>
                  <p>Paymentlink fee</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/pricing-premium')); ?>" class="nav-link <?php echo e($title=='Premium Pricing'?'active':''); ?>">
                  <i class="fa fa-diamond nav-icon"></i>
                  <p>Premium Pricing</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/expeditions/list')); ?>" class="nav-link <?php echo e($title=='Expedition List'?'active':''); ?>">
                  <i class="fa fa-car nav-icon"></i>
                  <p>Expedition List</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/assets/search/1')); ?>" class="nav-link <?php echo e($title=='Assets gallery'?'active':''); ?>">
                  <i class="fa fa-picture-o nav-icon"></i>
                  <p>Assets gallery</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/notification/1')); ?>" class="nav-link <?php echo e($title=='Notification'?'active':''); ?>">
                  <i class="fa fa-bell nav-icon"></i>
                  <p>Notification</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/app-management/faq')); ?>" class="nav-link <?php echo e($title=='FAQ'?'active':''); ?>">
                  <i class="fa fa-question-circle nav-icon"></i>
                  <p>FAQ</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/remote-config')); ?>" class="nav-link <?php echo e($title=='Remote Config'?'active':''); ?>">
                  <i class="fa fa-gears nav-icon"></i>
                  <p>Remote Config</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php echo e(in_array($title,['Keyta Users', 'Voucher List', 'Referral','Bank Account','Expeditions','Invoices','Orders Courier','Products', 'Reports','Cek Ongkir','Orders Premium','Shop Premium','Disbursement','Payment Link'])?'menu-open':''); ?>">
            <a href="#" class="nav-link <?php echo e(in_array($title,['Keyta Users', 'Voucher List', 'Referral', 'Bank Account','Expeditions','Invoices','Orders Courier','Products', 'Reports','Cek Ongkir','Orders Premium','Shop Premium','Disbursement','Payment Link'])?'active':''); ?>">
              <i class="nav-icon fa fa-bar-chart-o"></i>
              <p>
                Analitycs 
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytauser/1')); ?>" class="nav-link <?php echo e($title=='Keyta Users'?'active':''); ?>">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Keyta User</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/disbursement/1')); ?>" class="nav-link <?php echo e($title=='Disbursement'?'active':''); ?>">
                  <i class="fa fa-arrow-circle-down nav-icon"></i>
                  <p>Disbursement</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/paymentlink/1')); ?>" class="nav-link <?php echo e($title=='Payment Link'?'active':''); ?>">
                  <i class="fa fa-money nav-icon"></i>
                  <p>Payment Link</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/voucher/list')); ?>" class="nav-link <?php echo e($title=='Voucher List'?'active':''); ?>">
                  <i class="fa fa-barcode  nav-icon"></i>
                  <p>Voucher List</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/referral/1')); ?>" class="nav-link <?php echo e($title=='Referral'?'active':''); ?>">
                  <i class="fa fa-user nav-icon"></i>
                  <p>Referral</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/bankaccounts/1')); ?>" class="nav-link <?php echo e($title=='Bank Account'?'active':''); ?>">
                  <i class="fa fa-usd nav-icon"></i>
                  <p>Bank Account</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/expeditions/1')); ?>" class="nav-link <?php echo e($title=='Expeditions'?'active':''); ?>">
                  <i class="fa fa-car nav-icon"></i>
                  <p>Expedition</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/transactions/1')); ?>" class="nav-link <?php echo e($title=='Invoices'?'active':''); ?>">
                  <i class="fa fa-folder-open nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/orders/1')); ?>" class="nav-link <?php echo e($title=='Orders Courier'?'active':''); ?>">
                  <i class="fa fa-shopping-cart nav-icon"></i>
                  <p>Order Courier</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/orders-premium/1')); ?>" class="nav-link <?php echo e($title=='Orders Premium'?'active':''); ?>">
                  <i class="fa fa-shopping-cart nav-icon"></i>
                  <p>Order Premium</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/shops-premium/1')); ?>" class="nav-link <?php echo e($title=='Shop Premium'?'active':''); ?>">
                  <i class="fa fa-shopping-cart nav-icon"></i>
                  <p>Shop Premium</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/products/1')); ?>" class="nav-link <?php echo e($title=='Products'?'active':''); ?>">
                  <i class="fa fa-shopping-bag nav-icon"></i>
                  <p>Products</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/reports/1')); ?>" class="nav-link <?php echo e($title=='Reports'?'active':''); ?>">
                  <i class="fa fa-flag nav-icon"></i>
                  <p>Reports</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/costs/1')); ?>" class="nav-link <?php echo e($title=='Cek Ongkir'?'active':''); ?>">
                  <i class="fa fa-flag nav-icon"></i>
                  <p>Cek Ongkir Report</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php echo e(in_array($title,['Keyta Point by User', 'Keyta Point History'])?'menu-open':''); ?>">
            <a href="#" class="nav-link <?php echo e(in_array($title,['Keyta Point by User', 'Keyta Point History'])?'active':''); ?>">
              <i class="nav-icon fa fa-gears"></i>
              <p>
                Keyta Point (Deprecated)
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytapoint/user-balance/1')); ?>" class="nav-link <?php echo e($title=='Keyta Point by User'?'active':''); ?>">
                  <i class="fa fa-user nav-icon"></i>
                  <p>User Balance</p>
                </a>
              </li>
              <li class="nav-item ml-3">
                <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytapoint/history/1')); ?>" class="nav-link <?php echo e($title=='Keyta Point History'?'active':''); ?>">
                  <i class="fa fa-usd nav-icon"></i>
                  <p>Keyta Point History</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php echo e(in_array($title,['Keyta Saldo by User','History Saldo','Keyta Point','History Point'])?'menu-open':''); ?>">
            <a href="#" class="nav-link <?php echo e(in_array($title,['Keyta Saldo by User','History Saldo','Keyta Point','History Point'])?'active':''); ?>">
              <i class="nav-icon fa fa-credit-card-alt"></i>
              <p>
                Keyta Wallet
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3 <?php echo e(in_array($title,['Keyta Saldo by User','History Saldo'])?'menu-open':''); ?>">
                <a href="#" class="nav-link <?php echo e(in_array($title,['Keyta Saldo by User','History Saldo'])?'active':''); ?>">
                  <i class="nav-icon fa fa-money"></i>
                  <p>
                    Keyta Saldo
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytasaldo/user-balance/1')); ?>" class="nav-link <?php echo e($title=='Keyta Saldo by User'?'active':''); ?>">
                      <i class="fa fa-user nav-icon"></i>
                      <p>User Balance</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytasaldo/history/1')); ?>" class="nav-link <?php echo e($title=='History Saldo'?'active':''); ?>">
                      <i class="fa fa-usd nav-icon"></i>
                      <p>History</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item ml-3 <?php echo e(in_array($title,['Keyta Point','History Point'])?'menu-open':''); ?>">
                <a href="#" class="nav-link <?php echo e(in_array($title,['Keyta Pointr','History Point'])?'active':''); ?>">
                  <i class="nav-icon fa fa-money"></i>
                  <p>
                    Keyta Point
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytapointnew/user-balance/1')); ?>" class="nav-link <?php echo e($title=='Keyta Point'?'active':''); ?>">
                      <i class="fa fa-user nav-icon"></i>
                      <p>User Balance</p>
                    </a>
                  </li>
                  <li class="nav-item ml-3">
                    <a href="<?php echo e(URL::to('admin/'.$user_id.'/keytapointnew/history/1')); ?>" class="nav-link <?php echo e($title=='History Point'?'active':''); ?>">
                      <i class="fa fa-usd nav-icon"></i>
                      <p>History</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <?php elseif($access_level['role_id']==3): ?>
           <li class="nav-item">
            <a href="<?php echo e(URL::to('superuser/'.$user_id)); ?>" class="nav-link <?php echo e($title=='User List'?'active':''); ?>">
              <i class="nav-icon fa fa-users"></i>
              <p>
                User List
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo e(URL::to('superuser/tambahadmin/'.$user_id)); ?>" class="nav-link <?php echo e($title=='Tambah Admin'?'active':''); ?>">
              <i class="nav-icon fa fa-plus"></i>
              <p>
                Tambah Admin
                <!--  <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          
          <?php endif; ?>
          
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <?php echo $__env->yieldContent('content'); ?>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>