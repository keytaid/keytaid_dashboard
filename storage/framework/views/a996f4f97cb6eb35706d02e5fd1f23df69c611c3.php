<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php echo e($title); ?> | Keyta Dashboard</title>
  <link href="<?php echo e(asset('fileUpload/logo.ico')); ?>" rel="icon">
  <link href="<?php echo e(asset('fileUpload/logo.ico')); ?>" rel="apple-touch-icon">

  <!-- Font Awesome Icons -->
  <link rel="stylesheet"href="<?php echo e(asset('lte/plugins/font-awesome/css/font-awesome.min.css')); ?>">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet"href="<?php echo e(asset('lte/dist/css/adminlte.min.css')); ?>">
  <link rel="stylesheet"href="<?php echo e(asset('css/style.css')); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
  <script src="<?php echo e(asset('lte/plugins/jquery/jquery.min.js')); ?>"></script>
  <script src="https://code.jquery.com/ui/1.12.0-beta.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css">

  <link rel="stylesheet" href="<?php echo e(asset('lte/plugins/daterangepicker/daterangepicker-bs3.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('lte/plugins/iCheck/all.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('lte/plugins/colorpicker/bootstrap-colorpicker.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('lte/plugins/timepicker/bootstrap-timepicker.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('lte/plugins/select2/select2.min.css')); ?>">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet"href="<?php echo e(asset('lte/dist/css/custom.css')); ?>">

</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Navbar -->
    <?php echo $__env->make('admin/header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Main Sidebar Container -->
    <?php echo $__env->make('admin/sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <!-- Main Footer -->
    <?php echo $__env->make('admin/footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- Bootstrap 4 -->
  
  <script src="<?php echo e(asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/select2/select2.full.min.js')); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo e(asset('lte/dist/js/adminlte.min.js')); ?>"></script>
  <!-- Chart.js -->
  <script src="<?php echo e(asset('lte/plugins/chart.js/Chart.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/input-mask/jquery.inputmask.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/input-mask/jquery.inputmask.date.extensions.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/input-mask/jquery.inputmask.extensions.js')); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src="<?php echo e(asset('lte/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/colorpicker/bootstrap-colorpicker.min.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/plugins/iCheck/icheck.min.js')); ?>"></script>
  <script src="<?php echo e(asset('lte/dist/js/adminlte.min.js')); ?>"></script>

  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservation').daterangepicker({
        format             : 'YYYY-MM-DD'
      })

      $('#reservation_second').daterangepicker({
        format             : 'YYYY-MM-DD'
      })
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker         : true,
        timePickerIncrement: 30,
        format             : 'YYYY-MM-DD'
      })
      //Date range as a button
      var x = document.getElementById("tanggal").value; 
      if(x == null){
        start_date = moment().subtract(1, 'year');
        end_date = moment();
      }
      else{
        // x = x.replace(/-/g,"/");
        s_date = new moment(x.substring(0, 10));
        e_date = new moment(x.substring(11, 21));
        start_date = s_date.format('MM/DD/YYYY');
        end_date = e_date.format('MM/DD/YYYY');
      }
      $('#daterange-btn').daterangepicker(
        {
          ranges   : {
            'All Time'    : [moment().subtract(1, 'year'), moment()],
            'Today'       : [moment(), moment()],
            'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month'  : [moment().startOf('month'), moment().endOf('month')],
            'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: start_date,
          endDate  : end_date
        },
        function (start, end) {
          console.log("Callback has been called!");
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          startDate = start;
          endDate = end;
          var myform = document.getElementById('form_tanggal');
          console.log(startDate.format('YYYY-MM-DD') + '-' + endDate.format('YYYY-MM-DD'));
          tanggal = startDate.format('YYYY-MM-DD') + '-' + endDate.format('YYYY-MM-DD');
          document.getElementById('tanggal').value = tanggal;
          myform.submit();
        }
      )

      //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
      })
      //Red color scheme for iCheck
      $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
      })
      //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
      })

      //Colorpicker
      $('.my-colorpicker1').colorpicker()
      //color picker with addon
      $('.my-colorpicker2').colorpicker()

      //Timepicker
      $('.timepicker').timepicker({
        showInputs: false
      })
    })
  </script>
</body>
</html>
