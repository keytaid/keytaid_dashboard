<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><?php echo e($title); ?></h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    <?php if(Session::has('message')): ?>
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo e(Session::get('message')); ?>

                    </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Users With Shop</span>
                                <span class="info-box-number"><?php echo e($nsm['total_users']); ?></span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-file-text"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Transaction</span>
                                <span class="info-box-number"><?php echo e($dashboard['total_transaction']); ?></span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-warning"><i class="fa fa-shopping-cart"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Order Courier</span>
                                <span class="info-box-number"><?php echo e($order['total']); ?></span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Gross Income Order Courier</span>
                                <span class="info-box-number" id='total-order-income'>0</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Gross Income Premium Features</span>
                                <span class="info-box-number" id="total-premium-income">0</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-warning"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Gross Income</span>
                                <span class="info-box-number" id="total-combine-income">0</span>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header no-border">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title">North Star Metrics</h3>
                                        <a href="javascript:void(0);">View Report</a>
                                    </div>
                                </div>
                            <div class="card-body">
                                <div class="d-flex">
                                <p class="d-flex flex-column">
                                    <span class="text-bold text-lg"><?php echo e($nsm['total']); ?></span>
                                    <span>NSM Over Time</span>
                                </p>
                                <p class="ml-auto d-flex flex-column text-right">
                                    <span class="text-success">
                                    <i class="fa fa-arrow-up" id="percentage"> 12.5%</i>
                                    </span>
                                    <span class="text-muted">Since last month</span>
                                </p>
                                </div>
                                <!-- /.d-flex -->

                                <div class="position-relative mb-4">
                                <canvas id="visitors-chart" height="200"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                <span class="mr-2">
                                    <i class="fa fa-square text-primary"></i> nsm
                                </span>

                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header no-border">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title">Users Growth</h3>
                                        <a href="javascript:void(0);">View Report</a>
                                    </div>
                                </div>
                            <div class="card-body">
                                <div class="d-flex">
                                <p class="d-flex flex-column">
                                    <span class="text-bold text-lg"><?php echo e($nsm['total_users']); ?></span>
                                    <span>Users Over Time</span>
                                </p>
                                <p class="ml-auto d-flex flex-column text-right">
                                    <span class="text-success">
                                    <i class="fa fa-arrow-up" id="percentage-user"> 12.5%</i>
                                    </span>
                                    <span class="text-muted">Since last month</span>
                                </p>
                                </div>
                                <!-- /.d-flex -->

                                <div class="position-relative mb-4">
                                <canvas id="users-chart" height="200"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                <span class="mr-2">
                                    <i class="fa fa-square text-warning"></i> user
                                </span>

                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header no-border">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="card-title">Gross Income Order Courier</h3>
                                        <a href="javascript:void(0);">View Report</a>
                                    </div>
                                </div>
                            <div class="card-body">
                                <div class="d-flex">
                                <p class="d-flex flex-column">
                                    <span class="text-bold text-lg" id="total-gross">0</span>
                                    <span>Gross Income</span>
                                </p>
                                <p class="ml-auto d-flex flex-column text-right">
                                    <span class="text-success">
                                    <i class="fa fa-arrow-up" id="percentage-gross"> 12.5%</i>
                                    </span>
                                    <span class="text-muted">Since last month</span>
                                </p>
                                </div>
                                <!-- /.d-flex -->

                                <div class="position-relative mb-4">
                                <canvas id="gross-chart" height="200"></canvas>
                                </div>

                                <div class="d-flex flex-row justify-content-end">
                                <span class="mr-2">
                                    <i class="fa fa-square text-primary"></i>cumulative
                                    <i class="fa fa-square text-secondary"></i> monthly
                                </span>

                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="card">
                            <div class="card-header no-border">
                              <div class="d-flex justify-content-between">
                                <h3 class="card-title">Gross Income Premium Features</h3>
                                <a href="javascript:void(0);">View Report</a>
                              </div>
                            </div>
                            <div class="card-body">
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <span class="text-bold text-lg" id='total-gross2'>0</span>
                                  <span>Premium Features Over Time</span>
                                </p>
                                <p class="ml-auto d-flex flex-column text-right">
                                  <span class="text-success">
                                    <i class="fa fa-arrow-up" id="percentage-gross2"> 0%</i>
                                  </span>
                                  <span class="text-muted">Since last month</span>
                                </p>
                              </div>
                              <!-- /.d-flex -->

                              <div class="position-relative mb-4">
                                <canvas id="sales-chart" height="200"></canvas>
                              </div>

                              <div class="d-flex flex-row justify-content-end">
                                <span class="mr-2">
                                  <i class="fa fa-square text-primary"></i> cumulative
                                </span>

                                <span>
                                  <i class="fa fa-square text-secondary"></i> monthly
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="<?php echo e(asset('lte/plugins/jquery/jquery.min.js')); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo e(asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- AdminLTE -->
<script src="<?php echo e(asset('lte/dist/js/adminlte.js')); ?>"></script>

<script>
$(function () {
  'use strict'
  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true
  var array = <?php echo json_encode($nsm); ?>;

  const defaultOptions = {
    significantDigits: 2,
    thousandsSeparator: '.',
    decimalSeparator: ',',
    symbol: 'Rp'
  }
  
  const currencyFormatter = (value, options) => {
      if (typeof value !== 'number') value = 0.0

      options = { ...defaultOptions, ...options }
      value = value.toFixed(options.significantDigits)
    
      const [currency] = value.split('.')

      return `${options.symbol} ${currency.replace(
        /\B(?=(\d{3})+(?!\d))/g,
        options.thousandsSeparator
      )}`
  }

  var $visitorsChart = $('#visitors-chart')
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels  : array.nsm_range,
      datasets: [{
        type                : 'line',
        data                : array.nsm_accum,
        backgroundColor     : 'transparent',
        borderColor         : '#007bff',
        pointBorderColor    : '#007bff',
        pointBackgroundColor: '#007bff',
        fill                : false,
        pointHoverBackgroundColor: '#007bff',
        pointHoverBorderColor    : '#007bff'
      }]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: Math.max(...array.nsm_accum)
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
  let percentage = Math.round((array.nsm_accum[array.nsm_accum.length - 1] / array.nsm_accum[array.nsm_accum.length - 2] * 100 - 100) * 10) / 10
  $("#percentage").html(` ${percentage}%`)

  var $usersChart = $('#users-chart')
  var usersChart  = new Chart($usersChart, {
    data   : {
      labels  : array.user_range,
      datasets: [{
        type                : 'line',
        data                : array.user_accum,
        backgroundColor     : 'transparent',
        borderColor         : '#ffc107',
        pointBorderColor    : '#ffc107',
        pointBackgroundColor: '#ffc107',
        fill                : false,
        pointHoverBackgroundColor: '#ffc107',
        pointHoverBorderColor    : '#ffc107'
      }]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: Math.max(...array.user_accum)
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
  let percentageUser = Math.round((array.user_accum[array.user_accum.length - 1] / array.user_accum[array.user_accum.length - 2] * 100 - 100) * 10) / 10
  $("#percentage-user").html(` ${percentageUser}%`)

  var incomes = <?php echo json_encode($income); ?>;
  var $grossChart = $('#gross-chart')
  var grossChart  = new Chart($grossChart, {
    type   : 'bar',
    data   : {
      labels  : incomes.order.range,
      datasets: [
        {
          backgroundColor: '#ced4da',
          borderColor    : '#ced4da',
          data           : incomes.order.income_month
        },
        {
          backgroundColor: '#007bff',
          borderColor    : '#007bff',
          data           : incomes.order.income_accum
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              return currencyFormatter(value, defaultOptions)
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })

  let percentageGross = Math.round((incomes.order.income_accum[incomes.order.income_accum.length - 1] / incomes.order.income_accum[incomes.order.income_accum.length - 2] * 100 - 100) * 10) / 10
  $("#percentage-gross").html(` ${percentageGross}%`)
  
  let totalGross = incomes.order.income_accum[incomes.order.income_accum.length - 1]
  $("#total-gross").html(`${currencyFormatter(totalGross, defaultOptions)}`)
  

  var $salesChart = $('#sales-chart')
  var salesChart  = new Chart($salesChart, {
    type   : 'bar',
    data   : {
      labels  : incomes.premium.range,
      datasets: [
        {
          backgroundColor: '#ced4da',
          borderColor    : '#ced4da',
          data           : incomes.premium.income_month
        },
        {
          backgroundColor: '#007bff',
          borderColor    : '#007bff',
          data           : incomes.premium.income_accum
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              return currencyFormatter(value, defaultOptions)
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })

  let percentageGross2 = Math.round((incomes.premium.income_accum[incomes.premium.income_accum.length - 1] / incomes.premium.income_accum[incomes.premium.income_accum.length - 2] * 100 - 100) * 10) / 10
  $("#percentage-gross2").html(` ${percentageGross2}%`)

  let totalGross2 = incomes.premium.income_accum[incomes.premium.income_accum.length - 1]
  $("#total-gross2").html(`${currencyFormatter(totalGross2, defaultOptions)}`)

  $("#total-order-income").html(`${currencyFormatter(totalGross, defaultOptions)}`)
  $("#total-premium-income").html(`${currencyFormatter(totalGross2, defaultOptions)}`)
  $("#total-combine-income").html(`${currencyFormatter((totalGross + totalGross2), defaultOptions)}`)
})
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>