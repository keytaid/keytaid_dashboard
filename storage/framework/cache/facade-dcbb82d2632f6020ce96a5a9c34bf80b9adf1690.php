<?php

namespace Facades\App\Helper;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Helper\Order
 */
class Order extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Helper\Order';
    }
}
