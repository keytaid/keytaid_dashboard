<?php

namespace Facades\App\Helper;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Helper\Helper
 */
class Helper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Helper\Helper';
    }
}
