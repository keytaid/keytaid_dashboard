<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    // 'api_url' => env('api_url','localhost:3000/api/v1'),
    'api_url' => env('api_url','https://live.keyta.id/api/v1'),
    'api_url_v2' => env('api_url_v2','https://live.keyta.id/api/v2'),
    'api_url_wallet' => env('api_url_wallet','https://live.keyta.id/wallet/v1'),
    'company_email' => env('COMPANY_email','contact@acme.inc'),


];