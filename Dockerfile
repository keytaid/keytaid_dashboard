FROM php:7.4.3-apache

RUN docker-php-ext-install pdo_mysql
RUN a2enmod rewrite

ADD . /var/www
ADD ./public /var/www/html

RUN mkdir -p /var/www/storage/logs
RUN chmod -R ugo+rw /var/www/storage/
RUN mkdir -p /var/www/bootstrap/cache
RUN chmod -R ugo+rw /var/www/bootstrap/